<?php

/**
 *
 * @link              https://profiles.wordpress.org/gutotheme/
 * @since             1.3.0
 * @package           Guto_Toolkit
 *
 * @wordpress-plugin
 * Plugin Name:       Guto Toolkit
 * Plugin URI:        https://wordpress.org/plugins/guto-toolkit/
 * Description:       Elementor Addons WordPress Plugin
 * Version:           1.3.0
 * Author:            GutoTheme
 * Author URI:        https://profiles.wordpress.org/gutotheme/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       guto-toolkit
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Plugin directory path
 */
define('GUTO_ACC_PATH', plugin_dir_path(__FILE__));

/**
 * Plugin image folder path
 */
define('GUTO_TOOLKIT_IMAGES', GUTO_ACC_PATH . '/public/assets/img');

/**
 * Plugin image folder path
 */
define('GUTO_TOOLKIT_SCRIPTS', GUTO_ACC_PATH . '/public/js');

/**
 * Currently plugin version.
 * Start at version 1.3.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'GUTO_TOOLKIT_VERSION', '1.3.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-guto-toolkit-activator.php
 */
function activate_guto_toolkit() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-guto-toolkit-activator.php';
	Guto_Toolkit_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-guto-toolkit-deactivator.php
 */
function deactivate_guto_toolkit() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-guto-toolkit-deactivator.php';
	Guto_Toolkit_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_guto_toolkit' );
register_deactivation_hook( __FILE__, 'deactivate_guto_toolkit' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-guto-toolkit.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.3.0
 */
function run_guto_toolkit() {

	$plugin = new Guto_Toolkit();
	$plugin->run();

}
run_guto_toolkit();
