<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://profiles.wordpress.org/gutotheme/
 * @since      1.3.0
 *
 * @package    Guto_Toolkit
 * @subpackage Guto_Toolkit/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Guto_Toolkit
 * @subpackage Guto_Toolkit/public
 * @author     GutoTheme
 */
class Guto_Toolkit_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.3.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.3.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.3.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.3.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Guto_Toolkit_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Guto_Toolkit_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if (!function_exists('guto_lite_setup')) {

			wp_enqueue_style( 'bootstrap', plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css', array(), $this->version, 'all' );
	        wp_enqueue_style( 'animate', plugin_dir_url( __FILE__ ) . 'css/animate.min.css', array(), $this->version, 'all' );
	        wp_enqueue_style( 'boxicons', plugin_dir_url( __FILE__ ) . 'css/boxicons.min.css', array(), $this->version, 'all' );
	        wp_enqueue_style( 'nice-select', plugin_dir_url( __FILE__ ) . 'css/nice-select.min.css', array(), $this->version, 'all' );
	        wp_enqueue_style( 'owl-carousel', plugin_dir_url( __FILE__ ) . 'css/owl.carousel.min.css', array(), $this->version, 'all' );
	        wp_enqueue_style( 'range-slider', plugin_dir_url( __FILE__ ) . 'css/rangeSlider.min.css', array(), $this->version, 'all' );
	        wp_enqueue_style( 'odometer', plugin_dir_url( __FILE__ ) . 'css/odometer.min.css', array(), $this->version, 'all' );
	        wp_enqueue_style( 'magnific-popup', plugin_dir_url( __FILE__ ) . 'css/magnific-popup.min.css', array(), $this->version, 'all' );
			wp_enqueue_style( 'guto-main', plugin_dir_url( __FILE__ ) . 'css/main-style.css', array(), $this->version, 'all' );
			wp_enqueue_style( 'guto-responsive', plugin_dir_url( __FILE__ ) . 'css/responsive.css', array(), $this->version, 'all' );

	    }
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.3.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Guto_Toolkit_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Guto_Toolkit_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if (!function_exists('guto_lite_setup')) {

			wp_enqueue_script( 'popper', plugin_dir_url( __FILE__ ) . 'js/popper.min.js', array( 'jquery' ), $this->version, true );
	        wp_enqueue_script( 'bootstrap', plugin_dir_url( __FILE__ ) . 'js/bootstrap.min.js', array( 'jquery' ), $this->version, true );
	        wp_enqueue_script( 'nice-select', plugin_dir_url( __FILE__ ) . 'js/nice-select.min.js', array( 'jquery' ), $this->version, true );
	        wp_enqueue_script('magnific-popup', plugin_dir_url( __FILE__ ) . 'js/magnific-popup.min.js', array( 'jquery' ), $this->version, true );
	        wp_enqueue_script( 'owl-carousel', plugin_dir_url( __FILE__ ) . 'js/owl.carousel.min.js', array( 'jquery' ), $this->version, true );
	        wp_enqueue_script( 'parallax', plugin_dir_url( __FILE__ ) . 'js/parallax.min.js', array( 'jquery' ), $this->version, true );
	        wp_enqueue_script( 'range-slider', plugin_dir_url( __FILE__ ) . 'js/rangeSlider.min.js', array( 'jquery' ), $this->version, true );
	        wp_enqueue_script( 'jquery-appear', plugin_dir_url( __FILE__ ) . 'js/jquery.appear.min.js', array( 'jquery' ), $this->version, true );
	        wp_enqueue_script( 'odometer', plugin_dir_url( __FILE__ ) . 'js/odometer.min.js', array( 'jquery' ), $this->version, true );
	        wp_enqueue_script( 'wow', plugin_dir_url( __FILE__ ) . 'js/wow.min.js', array( 'jquery' ), $this->version, true );
	        wp_enqueue_script( 'jquery-ajaxchimp', plugin_dir_url( __FILE__ ) . 'js/jquery.ajaxchimp.min.js', array( 'jquery' ), $this->version, true );
			wp_enqueue_script( 'guto-main', plugin_dir_url( __FILE__ ) . 'js/guto-toolkit-public.js', array( 'jquery' ), $this->version, true );

	    }

	}

}
