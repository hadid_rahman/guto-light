<?php
/**
 * Guto register custom widgets
 */

// Recent post with thumbs
require_once( GUTO_ACC_PATH . 'includes/widgets/guto-toolkit-posts-thumbs.php' );

// Footer info widget
require_once( GUTO_ACC_PATH . 'includes/widgets/guto-toolkit-footer-info.php' );

// Contact info widget
require_once( GUTO_ACC_PATH . 'includes/widgets/guto-toolkit-contact-info.php' );

// CTA widget
require_once( GUTO_ACC_PATH . 'includes/widgets/guto-toolkit-stay-connected-widget.php' );

// Info widget
require_once( GUTO_ACC_PATH . 'includes/widgets/guto-toolkit-info-widget.php' );