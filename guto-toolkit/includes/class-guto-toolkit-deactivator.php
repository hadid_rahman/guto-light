<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://profiles.wordpress.org/gutotheme/
 * @since      1.3.0
 *
 * @package    Guto_Toolkit
 * @subpackage Guto_Toolkit/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.3.0
 * @package    Guto_Toolkit
 * @subpackage Guto_Toolkit/includes
 * @author     GutoTheme
 */
class Guto_Toolkit_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.3.0
	 */
	public static function deactivate() {

	}

}
