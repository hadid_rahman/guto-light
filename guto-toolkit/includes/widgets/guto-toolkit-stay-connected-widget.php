<?php
 /**
 * Social Links Widget
 */
class Guto_widget_socials_link extends WP_Widget{

    function __construct(){
        $widget_ops = array('description' => esc_html__('Display Social Links', 'guto-toolkit'));
        parent::__construct( false, esc_html__('Guto Social Links', 'guto-toolkit'), $widget_ops);
    }

    function widget($args, $instance){
        extract($args);
        global $guto_theme;

        $title  = apply_filters('widget_title', $instance['title']);

        echo wp_kses_post($before_widget);
        if($title) echo wp_kses_post($before_title.$title.$after_title);
        ?>
        <ul>
            <?php if( $instance['facebook'] != '' ): ?>
                <li>
                    <a href="<?php echo esc_url( $instance['facebook_link'] ); ?>" target="_blank"><i class="bx bxl-facebook"></i><?php echo wp_kses_post($instance['facebook']); ?></a>
                </li>
            <?php endif; ?>

            <?php if( $instance['twitter'] != '' ): ?>
                <li>
                    <a href="<?php echo esc_url( $instance['twitter_link'] ); ?>"><i class="bx bxl-twitter"></i><?php echo wp_kses_post($instance['twitter']); ?></a>
                </li>
            <?php endif; ?>

            <?php if( $instance['linkedin'] != '' ): ?>
                <li>
                    <a href="<?php echo esc_url( $instance['linkedin_link'] ); ?>"><i class="bx bxl-linkedin"></i><?php echo wp_kses_post($instance['linkedin']); ?></a>
                </li>
            <?php endif; ?>

            <?php if( $instance['instagram'] != '' ): ?>
                <li>
                    <a href="<?php echo esc_url( $instance['instagram_link'] ); ?>"><i class="bx bxl-instagram"></i><?php echo wp_kses_post($instance['instagram']); ?></a>
                </li>
            <?php endif; ?>
        </ul>
        <?php
        echo wp_kses_post($after_widget);
    }

    function update($new_instance, $old_instance){
        $instance                       = $old_instance;
        $instance['title']              = strip_tags($new_instance['title']);
        $instance['facebook']           = $new_instance['facebook'];
        $instance['facebook_link']      = $new_instance['facebook_link'];
        $instance['twitter']            = $new_instance['twitter'];
        $instance['twitter_link']       = $new_instance['twitter_link'];
        $instance['linkedin']           = $new_instance['linkedin'];
        $instance['linkedin_link']      = $new_instance['linkedin_link'];
        $instance['twitter_link']       = $new_instance['twitter_link'];
        $instance['instagram']          = $new_instance['instagram'];
        $instance['instagram_link']     = $new_instance['instagram_link'];
        return $instance;
    }

    function form($instance){
        $defaults = array(
            'title'             => esc_html__('Stay Connected', 'guto-toolkit'),
            'facebook'          => esc_html__('Facebook', 'guto-toolkit'),
            'facebook_link'     => '#',
            'twitter'           => esc_html__('Twitter', 'guto-toolkit'),
            'twitter_link'      => '#',
            'linkedin'          => esc_html__('Linkedin', 'guto-toolkit'),
            'linkedin_link'     => '#',
            'twitter_link'      => '#',
            'instagram'         => esc_html__('Instagram', 'guto-toolkit'),
            'instagram_link'    => '#',
        );
        $instance = wp_parse_args((array)$instance, $defaults);
        ?>
        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('title')); ?>">
                <?php esc_html_e('Title:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo wp_kses_post($instance['title']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('facebook')); ?>">
                <?php esc_html_e('Facebook:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('facebook')); ?>" name="<?php echo esc_attr($this->get_field_name('facebook')); ?>" type="text" value="<?php echo wp_kses_post($instance['facebook']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('facebook_link')); ?>">
                <?php esc_html_e('Facebook Link:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('facebook_link')); ?>" name="<?php echo esc_attr($this->get_field_name('facebook_link')); ?>" type="text" value="<?php echo wp_kses_post($instance['facebook_link']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('twitter')); ?>">
                <?php esc_html_e('Twitter Number:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('twitter')); ?>" name="<?php echo esc_attr($this->get_field_name('twitter')); ?>" type="text" value="<?php echo wp_kses_post($instance['twitter']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('twitter_link')); ?>">
                <?php esc_html_e('Twitter Link:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('twitter_link')); ?>" name="<?php echo esc_attr($this->get_field_name('twitter_link')); ?>" type="text" value="<?php echo wp_kses_post($instance['twitter_link']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('linkedin')); ?>">
                <?php esc_html_e('Linkedin:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('linkedin')); ?>" name="<?php echo esc_attr($this->get_field_name('linkedin')); ?>" type="text" value="<?php echo wp_kses_post($instance['linkedin']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('linkedin_link')); ?>">
                <?php esc_html_e('Linkedin Link:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('linkedin_link')); ?>" name="<?php echo esc_attr($this->get_field_name('linkedin_link')); ?>" type="text" value="<?php echo wp_kses_post($instance['linkedin_link']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('instagram')); ?>">
                <?php esc_html_e('Instagram:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('instagram')); ?>" name="<?php echo esc_attr($this->get_field_name('instagram')); ?>" type="text" value="<?php echo wp_kses_post($instance['instagram']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('instagram_link')); ?>">
                <?php esc_html_e('Instagram Link:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('instagram_link')); ?>" name="<?php echo esc_attr($this->get_field_name('instagram_link')); ?>" type="text" value="<?php echo wp_kses_post($instance['instagram_link']); ?>" />
            </label>
        </p>
        <?php
    }

}

function guto_register_socials_link() {
    register_widget('Guto_widget_socials_link');
}

add_action('widgets_init', 'guto_register_socials_link');