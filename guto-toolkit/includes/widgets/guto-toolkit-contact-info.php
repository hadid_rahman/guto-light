<?php
 /**
 * Contact Info Widget
 */
class Guto_contact_info extends WP_Widget{

    function __construct(){
        $widget_ops = array('description' => esc_html__('Display Contact Info', 'guto-toolkit'));
        parent::__construct( false, esc_html__('Guto Footer Contact Info', 'guto-toolkit'), $widget_ops);
    }

    function widget($args, $instance){
        extract($args);
        global $guto_theme;

        $title  = apply_filters('widget_title', $instance['title']);

        echo wp_kses_post($before_widget);
        if($title) echo wp_kses_post($before_title.$title.$after_title);
        ?>
        <ul class="footer-contact-info">
            <?php if( $instance['location'] != '' ): ?>
                <li>
                    <a href="<?php echo esc_url( $instance['location_link'] ); ?>" target="_blank"><span><?php echo esc_html__('Address: ', 'guto-toolkit'); ?></span><?php echo wp_kses_post($instance['location']); ?></a>
                </li>
            <?php endif; ?>

            <?php if( $instance['phone'] != '' ): ?>
                <li>
                    <a href="<?php echo esc_url( $instance['phone_link'] ); ?>"><span><?php echo esc_html__('Email: ', 'guto-toolkit'); ?></span><?php echo wp_kses_post($instance['phone']); ?></a>
                </li>
            <?php endif; ?>

            <?php if( $instance['email'] != '' ): ?>
                <li>
                    <a href="<?php echo esc_url( $instance['email_link'] ); ?>"><span><?php echo esc_html__('Phone: ', 'guto-toolkit'); ?></span><?php echo wp_kses_post($instance['email']); ?></a>
                </li>
            <?php endif; ?>

            <?php if( $instance['fax'] != '' ): ?>
                <li>
                    <a href="<?php echo esc_url( $instance['fax_link'] ); ?>"><span><?php echo esc_html__('Fax: ', 'guto-toolkit'); ?></span><?php echo wp_kses_post($instance['fax']); ?></a>
                </li>
            <?php endif; ?>
        </ul>
        <?php
        echo wp_kses_post($after_widget);
    }

    function update($new_instance, $old_instance){
        $instance                    = $old_instance;
        $instance['title']           = strip_tags($new_instance['title']);
        $instance['location']        = $new_instance['location'];
        $instance['location_link']   = $new_instance['location_link'];
        $instance['phone']           = $new_instance['phone'];
        $instance['phone_link']      = $new_instance['phone_link'];
        $instance['email']           = $new_instance['email'];
        $instance['email_link']      = $new_instance['email_link'];
        $instance['phone_link']      = $new_instance['phone_link'];
        $instance['fax']             = $new_instance['fax'];
        $instance['fax_link']        = $new_instance['fax_link'];
        return $instance;
    }

    function form($instance){
        $defaults = array(
            'title'             => esc_html__('Address', 'guto-toolkit'),
            'location'          => esc_html__('2750 Quadra Street Victoria Road, New York, Canada'),
            'location_link'     => '#',
            'phone'             => esc_html__('+1 (123) 456 7890', 'guto-toolkit'),
            'phone_link'        => esc_html__('tel:+11234567890', 'guto-toolkit'),
            'email'             => esc_html__('hello@guto.com', 'guto-toolkit'),
            'email_link'        => esc_html__('mailto:hello@guto.com', 'guto-toolkit'),
            'phone_link'        => esc_html__('tel:+11234567890', 'guto-toolkit'),
            'fax'               => esc_html__('+55 785 4578964', 'guto-toolkit'),
            'fax_link'          => esc_html__('tel:+55 785 4578964', 'guto-toolkit'),
        );
        $instance = wp_parse_args((array)$instance, $defaults);
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">
                <?php esc_html_e('Title:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo wp_kses_post($instance['title']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('location')); ?>">
                <?php esc_html_e('Location:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('location')); ?>" name="<?php echo esc_attr($this->get_field_name('location')); ?>" type="text" value="<?php echo wp_kses_post($instance['location']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('location_link')); ?>">
                <?php esc_html_e('Location Link:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('location_link')); ?>" name="<?php echo esc_attr($this->get_field_name('location_link')); ?>" type="text" value="<?php echo wp_kses_post($instance['location_link']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('phone')); ?>">
                <?php esc_html_e('Phone Number:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('phone')); ?>" name="<?php echo esc_attr($this->get_field_name('phone')); ?>" type="text" value="<?php echo wp_kses_post($instance['phone']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('phone_link')); ?>">
                <?php esc_html_e('Number Link:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('phone_link')); ?>" name="<?php echo esc_attr($this->get_field_name('phone_link')); ?>" type="text" value="<?php echo wp_kses_post($instance['phone_link']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('email')); ?>">
                <?php esc_html_e('Email:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('email')); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" type="text" value="<?php echo wp_kses_post($instance['email']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('email_link')); ?>">
                <?php esc_html_e('Email Link:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('email_link')); ?>" name="<?php echo esc_attr($this->get_field_name('email_link')); ?>" type="text" value="<?php echo wp_kses_post($instance['email_link']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('fax')); ?>">
                <?php esc_html_e('Fax:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('fax')); ?>" name="<?php echo esc_attr($this->get_field_name('fax')); ?>" type="text" value="<?php echo wp_kses_post($instance['fax']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('fax_link')); ?>">
                <?php esc_html_e('Fax Link:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('fax_link')); ?>" name="<?php echo esc_attr($this->get_field_name('fax_link')); ?>" type="text" value="<?php echo wp_kses_post($instance['fax_link']); ?>" />
            </label>
        </p>
        <?php
    }

}

function guto_register_contact_info() {
    register_widget('Guto_contact_info');
}

add_action('widgets_init', 'guto_register_contact_info');