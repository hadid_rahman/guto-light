<?php
/**
 * Post Thumbs Widget
 */

class Guto_posts_thumbs extends WP_Widget{

    function __construct(){
        $widget_ops = array('description' => esc_html__('Display Random or Recent posts with a small image.', 'guto-toolkit'));
        parent::__construct( false, esc_html__('Guto Recent Posts With Image', 'guto-toolkit'), $widget_ops);
    }

    function widget($args, $instance){
        global $guto_theme;
        extract($args); //it receives an associative array

        $title = apply_filters('widget_title', $instance['title']);
        $args = array(
            'posts_per_page' => $instance['number'],
            'post_type' => 'post',
            'order' => 'DESC',
            'orderby' => $instance['orderby']
        );
        $query = new WP_Query($args);

        if( !$query->have_posts() ) return;
        echo wp_kses_post($before_widget);
        if($title) echo wp_kses_post($before_title.$title.$after_title);
        if(!$instance['number']) $instance['number'] = 4;

        if($query->have_posts()):
            $c = 0;

            while($query->have_posts()): $query->the_post(); ?>
                <?php
                $class = 'item';
                $post_id = get_the_ID();
                $thumb_size = 'guto_widget_thumb';
                ?>
                <?php if( !has_post_thumbnail() ) $class .= ' no-thumb'; ?>
                <article <?php post_class($class); ?>>

                    <?php if( has_post_thumbnail() ): ?>
                        <?php
                        $thumb_id   = get_post_thumbnail_id($post_id);
                        $thumb_type = get_post_mime_type($thumb_id);
                        $image_alt  = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true);
                        if( !$image_alt ){
                            $image_alt = get_the_title($post_id);
                        }
                        if($thumb_type == 'image/gif'){
                            $thumb_size = '';
                        }

                        if( function_exists('guto_option')):
                            $read_text  = guto_option('read_text');
                        else:
                            $read_text  = '';
                        endif;

                        ?>
                            <a href="<?php the_permalink(); ?>" class="thumb"><span class="fullimage cover" role="img" style="background-image:url(<?php the_post_thumbnail_url('guto_lite_post_thumb'); ?>)"></span></a>
                            <div class="info">
                                <h4 class="title usmall"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                <ul class="meta">
                                    <li><i class='bx bx-calendar'></i> <?php the_time( get_option('date_format') ); ?></li>

                                    <?php if($read_text): ?>
                                        <li><i class='bx bx-time'></i> <?php echo esc_html(guto_toolkit_reading_time()) ?> <?php echo esc_html($read_text); ?></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                    <?php endif; ?>
                </article>
            <?php
            endwhile;
            wp_reset_postdata();
        endif;
        echo wp_kses_post($after_widget);
    }

    function update($new_instance, $old_instance){
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = (int) $new_instance['number'];
        $instance['orderby'] = $new_instance['orderby'];
        return $instance;
    }

    function form($instance){
        $defaults = array(
            'title' => esc_html__('Recent posts', 'guto-toolkit'),
            'number' => 4,
            'orderby' => 'date'
        );
        $instance = wp_parse_args((array)$instance, $defaults);
        $number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 4;
        ?>
        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('title')); ?>">
                <?php esc_html_e('Title:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo wp_kses_post($instance['title']); ?>" />
            </label>
        </p>
        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('number')); ?>"><?php esc_html_e( 'Number of posts to show:', 'guto-toolkit'); ?></label>
            <input id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo wp_kses_post($number); ?>" size="3" />
        </p>
        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('orderby')); ?>"><?php esc_html_e('Mode:', 'guto-toolkit') ?> </label>
            <select id="<?php echo esc_attr($this->get_field_id('orderby')); ?>" name="<?php echo esc_attr($this->get_field_name('orderby')); ?>">
                <option <?php if ($instance['orderby'] == 'date') echo 'selected="selected"'; ?> value="date"><?php esc_html_e('Recent Posts', 'guto-toolkit'); ?></option>
                <option <?php if ($instance['orderby'] == 'rand') echo 'selected="selected"'; ?> value="rand"><?php esc_html_e('Random Posts', 'guto-toolkit'); ?></option>
                <?php if( function_exists('get_field') ): // By views ?>
                    <option <?php if ($instance['orderby'] == 'views') echo 'selected="selected"'; ?> value="views"><?php esc_html_e('Post views', 'guto-toolkit'); ?></option>
                <?php endif; ?>
            </select>
        </p>
        <?php
    }
}

function guto_register_posts_thumbs() {
    register_widget('Guto_posts_thumbs');
}

add_action('widgets_init', 'guto_register_posts_thumbs');