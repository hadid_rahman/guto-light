<?php
/**
 * Guto Info Widget
 */
class Guto_widget_contact extends WP_Widget{

    function __construct(){
        $widget_ops = array('description' => esc_html__('Display Information', 'guto-toolkit'));
        parent::__construct( false, esc_html__('Guto Info Card', 'guto-toolkit'), $widget_ops);
    }

    function widget($args, $instance){
        extract($args);
        global $guto_theme;

        if( function_exists('guto_option')):
            $logo 	= GUTO_TOOLKIT_IMAGES.'/logo.png';
            $logo = guto_option('site_logo');
        else:
            $logo 	= '';
        endif;

        echo wp_kses_post($before_widget);
            ?>
            <div class="widget_buy_now">
                <?php if($instance['bg_image'] != ''): ?>
                    <img src="<?php echo esc_url( $instance['bg_image'] ); ?>" alt="<?php echo esc_attr( $instance['content'] ); ?>">
                <?php endif; ?>

                <div class="content">
                    <a href="<?php echo esc_url(home_url('/'));?>" class="logo">
                        <?php if(!empty($logo)): ?>
                            <img src="<?php echo esc_url($logo);  ?>" alt="<?php echo get_bloginfo(); ?>">
                        <?php else: ?>
                            <h2><?php echo get_bloginfo(); ?></h2>
                        <?php endif ?>
                    </a>

                    <?php if($instance['content'] != ''): ?>
                        <p><?php echo esc_html( $instance['content'] ); ?></p>
                    <?php endif; ?>

                    <?php if($instance['button'] != ''): ?>
                        <a href="<?php echo esc_url( $instance['button_link'] ); ?>" class="buy-now-btn"><?php echo esc_html( $instance['button'] ); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        <?php
        echo wp_kses_post($after_widget);
    }

    function update($new_instance, $old_instance){
        $instance                           = $old_instance;
        $instance['bg_image']               = $new_instance['bg_image'];
        $instance['content']                = $new_instance['content'];
        $instance['button']                 = $new_instance['button'];
        $instance['button_link']            = $new_instance['button_link'];
        return $instance;
    }

    function form($instance){
        $defaults = array(
            'content'               => esc_html__('Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod.', 'guto-toolkit'),
            'button'                => esc_html__('Buy Now', 'guto-toolkit'),
            'button_link'           => '#',
            'bg_image'              => '#',
        );
        $instance = wp_parse_args((array)$instance, $defaults);
        ?>
        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('content')); ?>">
                <?php esc_html_e('Content Text:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('content')); ?>" name="<?php echo esc_attr($this->get_field_name('content')); ?>" type="text" value="<?php echo wp_kses_post($instance['content']); ?>" />
            </label>
        </p>
        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('button')); ?>">
                <?php esc_html_e('Button Text:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('button')); ?>" name="<?php echo esc_attr($this->get_field_name('button')); ?>" type="text" value="<?php echo wp_kses_post($instance['button']); ?>" />
            </label>
        </p>
        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('button_link')); ?>">
                <?php esc_html_e('Button Link:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('button_link')); ?>" name="<?php echo esc_attr($this->get_field_name('button_link')); ?>" type="text" value="<?php echo wp_kses_post($instance['button_link']); ?>" />
            </label>
        </p>
        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('bg_image')); ?>">
                <?php esc_html_e('Image URL:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('bg_image')); ?>" name="<?php echo esc_attr($this->get_field_name('bg_image')); ?>" type="text" value="<?php echo wp_kses_post($instance['bg_image']); ?>" />
            </label>
        </p>

        <?php
    }

}

function guto_register_widget_contact() {
    register_widget('Guto_widget_contact');
}

add_action('widgets_init', 'guto_register_widget_contact');