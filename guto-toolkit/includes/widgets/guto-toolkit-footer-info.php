<?php
/**
 * Footer Info Widget
 */

class Guto_footer_info extends WP_Widget{

    function __construct(){
        $widget_ops = array('description' => esc_html__('Display Footer Info', 'guto-toolkit'));
        parent::__construct( false, esc_html__('Guto Footer Info', 'guto-toolkit'), $widget_ops);
    }

    function widget($args, $instance){
        extract($args);
        global $guto_theme;

        $title          = apply_filters('widget_title', $instance['title']);
        $social         = ! empty( $instance['social'] ) ? '1' : '0';

        if( function_exists('guto_lite_option')):
            $footer_logo 	= guto_lite_option('footer_logo');
            $social_links 	= guto_lite_option('footer_social_links');
        else:
            $footer_logo 	= '';
            $social_links 	= '';
        endif;

        ?>

        <div class="single-widget">
            <?php
            echo wp_kses_post($before_widget);
            if($title) echo wp_kses_post($before_title.$title.$after_title);
            ?>
            <a href="<?php echo esc_url(home_url('/'));?>" class="logo">
                <?php if(!empty($footer_logo)): ?>
                    <img src="<?php echo esc_url($footer_logo);  ?>" alt="<?php echo get_bloginfo(); ?>">
                <?php else: ?>
                    <h2><?php echo get_bloginfo(); ?></h2>
                <?php endif ?>
            </a>

            <?php if($instance['footer_content']): ?>
                <p><?php echo esc_html($instance['footer_content']); ?></p>
            <?php endif; ?>

            <?php if ( $social ) { ?>
                <?php if (is_array($social_links) && !empty($social_links)): ?>
                    <ul class="social-links">
                        <?php foreach ($social_links as $index => $item): ?>
                            <li>
                                <a href="<?php echo esc_url($item['social_url']) ?>" target="_blank">
                                    <i class="<?php echo esc_attr($item['social_icon']) ?>"></i>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            <?php } ?>
            <?php echo wp_kses_post($after_widget); ?>
        </div>
        <?php
    }

    function update($new_instance, $old_instance){
        $instance                                   = $old_instance;
        $instance['title']                          = strip_tags($new_instance['title']);
        $instance['footer_content']                 = $new_instance['footer_content'];
		$instance['social']                         = $new_instance['social'] ? 1 : 0;
        return $instance;
    }

    function form($instance){
        $defaults = array(
            'title'                             => '',
            'footer_content'                    => esc_html__('Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan.', 'guto-toolkit'),
            'social'                            => '',
        );
        $instance = wp_parse_args((array)$instance, $defaults);
        ?>
        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('title')); ?>">
                <?php esc_html_e('Title:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo wp_kses_post($instance['title']); ?>" />
            </label>
        </p>

        <p>
            <label for="<?php echo wp_kses_post($this->get_field_id('footer_content')); ?>">
                <?php esc_html_e('Footer Content:', 'guto-toolkit'); ?>
                <input class="widget" id="<?php echo esc_attr($this->get_field_id('footer_content')); ?>" name="<?php echo esc_attr($this->get_field_name('footer_content')); ?>" type="text" value="<?php echo wp_kses_post($instance['footer_content']); ?>" />
            </label>
        </p>

        <p>
			<input class="checkbox" type="checkbox"<?php checked( $instance['social'] ); ?> id="<?php echo esc_attr($this->get_field_id( 'social' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'social' )); ?>" /> <label for="<?php echo wp_kses_post($this->get_field_id( 'social' )); ?>"><?php _e( 'Display social icons' ); ?></label>
		</p>
        <?php
    }

}

function guto_register_footer_info() {
    register_widget('Guto_footer_info');
}

add_action('widgets_init', 'guto_register_footer_info');