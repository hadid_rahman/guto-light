<?php
/**
 * Custom Posts
 */

function guto_toolkit_custom_post() {
    register_post_type('services',
        array(
            'labels' => array(
                'name' => esc_html__('Services', 'guto-toolkit'),
                'singular_name' => esc_html__('Service', 'guto-toolkit'),
            ),
            'menu_icon'             => 'dashicons-groups',
            'supports'              => array('title', 'editor', 'excerpt', 'thumbnail'),
            'public'                => true,
            'show_in_rest'          => true,
            'publicly_queryable'    => true,
            'has_archive'           => false,
            'taxonomies '           => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'query_var'             => true,
            'exclude_from_search'   => false,
            'capability_type'       => 'post',
            'yarpp_support'         => true,


        )
	);
    register_post_type('projects',
        array(
            'labels' => array(
                'name' => esc_html__('Projects', 'guto-toolkit'),
                'singular_name' => esc_html__('Project', 'guto-toolkit'),
            ),
            'menu_icon'             => 'dashicons-feedback',
            'supports'              => array('title', 'editor', 'excerpt', 'thumbnail'),
            'public'                => true,
            'show_in_rest'          => true,
            'publicly_queryable'    => true,
            'has_archive'           => false,
            'taxonomies '           => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'query_var'             => true,
            'exclude_from_search'   => false,
            'capability_type'       => 'post',
            'yarpp_support'         => true,


        )
	);
}
add_action('init', 'guto_toolkit_custom_post');

// Taxonomy Custom Post
function guto_toolkit_custom_post_taxonomy(){

    register_taxonomy(
        'service_cat',
        'services',
            array(
            'hierarchical'      => true,
            'label'             => esc_html__('Service Category', 'guto-toolkit' ),
            'query_var'         => true,
            'show_admin_column' => true,
                'rewrite'         => array(
                'slug'          => 'service-category',
                'with_front'    => true
            )
        )
    );

    register_taxonomy(
        'projects_cat',
        'projects',
            array(
            'hierarchical'      => true,
            'label'             => esc_html__('Project Category', 'guto-toolkit' ),
            'query_var'         => true,
            'show_admin_column' => true,
                'rewrite'         => array(
                'slug'          => 'project-category',
                'with_front'    => true
            )
        )
    );

  }
add_action('init', 'guto_toolkit_custom_post_taxonomy');