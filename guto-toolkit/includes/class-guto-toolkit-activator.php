<?php

/**
 * Fired during plugin activation
 *
 * @link       https://profiles.wordpress.org/gutotheme/
 * @since      1.3.0
 *
 * @package    Guto_Toolkit
 * @subpackage Guto_Toolkit/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.3.0
 * @package    Guto_Toolkit
 * @subpackage Guto_Toolkit/includes
 * @author     GutoTheme
 */
class Guto_Toolkit_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.3.0
	 */
	public static function activate() {

	}

}
