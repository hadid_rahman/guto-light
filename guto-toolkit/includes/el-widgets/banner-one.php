<?php
/**
 * Banner One Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Banner_One extends Widget_Base {

	public function get_name() {
        return 'Guto_Banner_One';
    }

	public function get_title() {
        return esc_html__( 'Banner One', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-banner';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Banner_One_Area',
			[
				'label' => esc_html__( 'Banner Controls', 'guto-toolkit' ),
				'tab' 	=> Controls_Manager::TAB_CONTENT,
			]
		);

			$this->add_control(
				'top_title',
				[
					'label' 	=> esc_html__( 'Top Title', 'guto-toolkit' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> esc_html__('LET"S START YOUR PROJECT', 'guto-toolkit'),
				]
            );

			$this->add_control(
				'title',
				[
					'label' 	=> esc_html__( 'Title', 'guto-toolkit' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> esc_html__('Thriving Business With True Professionals', 'guto-toolkit'),
				]
			);

			$this->add_control(
                'title_tag',
                [
                    'label' 	=> esc_html__( 'Title Tag', 'guto-toolkit' ),
                    'type' 		=> Controls_Manager::SELECT,
                    'options' 	=> [
                        'h1'         => esc_html__( 'h1', 'guto-toolkit' ),
                        'h2'         => esc_html__( 'h2', 'guto-toolkit' ),
                        'h3'         => esc_html__( 'h3', 'guto-toolkit' ),
                        'h4'         => esc_html__( 'h4', 'guto-toolkit' ),
                        'h5'         => esc_html__( 'h5', 'guto-toolkit' ),
                        'h6'         => esc_html__( 'h6', 'guto-toolkit' ),
                    ],
                    'default' => 'h1',
                ]
            );

			$this->add_control(
				'content',
				[
					'label' 	=> esc_html__( 'Content', 'guto-toolkit' ),
					'type' 		=> Controls_Manager::TEXTAREA,
					'default' 	=> esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.', 'guto-toolkit'),
				]
            );

            $this->add_control(
				'button_icon',
				[
					'label' => esc_html__( 'Button Icon', 'guto-toolkit' ),
                    'type' => Controls_Manager::ICON,
                    'label_block' => true,
                    'options' => guto_toolkit_icons(),
				]
            );

			$this->add_control(
				'button_text',
				[
					'label' 	=> esc_html__( 'Button Text', 'guto-toolkit' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> esc_html__('Get Started a Project', 'guto-toolkit'),
				]
            );

            $this->add_control(
                'link_type',
                [
                    'label' 		=> esc_html__( 'Button Link Type', 'guto-toolkit' ),
                    'type' 			=> Controls_Manager::SELECT,
                    'label_block' 	=> true,
                    'options' => [
                        '1'  	=> esc_html__( 'Link To Page', 'guto-toolkit' ),
                        '2' 	=> esc_html__( 'External Link', 'guto-toolkit' ),
                    ],
                ]
            );

            $this->add_control(
                'link_to_page',
                [
                    'label' 		=> esc_html__( 'Button Link Page', 'guto-toolkit' ),
                    'type' 			=> Controls_Manager::SELECT,
                    'label_block' 	=> true,
                    'options' 		=> guto_toolkit_get_page_as_list(),
                    'condition' => [
                        'link_type' => '1',
                    ]
                ]
            );

            $this->add_control(
                'ex_link',
                [
                    'label'		=> esc_html__('Button External Link', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: URL,
                    'show_external' => true,
                    'default' => [
                        'url' => '#',
                        'is_external' => true,
                        'nofollow' => true,
                    ],
                    'condition' => [
                        'link_type' => '2',
                    ]
                ]
            );

        $this->end_controls_section();

        $this->start_controls_section(
			'banner_images',
			[
				'label' => esc_html__( 'Images', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

            $this->add_control(
                'image1',
                [
                    'label'		=> esc_html__('Banner Image One', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: MEDIA,
                ]
            );
            $this->add_control(
                'image2',
                [
                    'label'		=> esc_html__('Banner Image Two', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: MEDIA,
                ]
            );
            $this->add_control(
                'shape1',
                [
                    'label'		=> esc_html__('Shape Image One', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: MEDIA,
                ]
            );

            $this->add_control(
                'shape2',
                [
                    'label'		=> esc_html__('Shape Image Two', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: MEDIA,
                ]
            );

            $this->add_control(
                'shape3',
                [
                    'label'		=> esc_html__('Shape Image Three', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: MEDIA,
                ]
            );

            $this->add_control(
                'shape4',
                [
                    'label'		=> esc_html__('Shape Image Four', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: MEDIA,
                ]
            );

            $this->add_control(
                'shape5',
                [
                    'label'		=> esc_html__('Shape Image Five', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: MEDIA,
                ]
            );

            $this->add_control(
                'shape6',
                [
                    'label'		=> esc_html__('Shape Image Six', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: MEDIA,
                ]
            );
            $this->add_control(
                'shape7',
                [
                    'label'		=> esc_html__('Shape Image Seven', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: MEDIA,
                ]
            );

        $this->end_controls_section();

        $this->start_controls_section(
			'banner_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

            $this->add_control(
                'bg_color',
                [
                    'label' => esc_html__( 'Section Background Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .main-banner-area' => 'background-color: {{VALUE}}',
                    ],
                ]
            );

			$this->add_control(
				'top_title_color',
				[
					'label' => esc_html__( 'Top Title Color', 'guto-toolkit' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .main-banner-content .sub-title' => 'color: {{VALUE}}',
					],
				]
			);

			$this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'top_title_typography',
                    'label' => esc_html__( 'Top Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .main-banner-content .sub-title',
                ]
            );

			$this->add_control(
				'title_color',
				[
					'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .main-banner-content h1, .main-banner-content h2, .main-banner-content h3, .main-banner-content h4, .main-banner-content h5, .main-banner-content h6' => 'color: {{VALUE}}',
					],
				]
			);

			$this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .main-banner-content h1, .main-banner-content h2, .main-banner-content h3, .main-banner-content h4, .main-banner-content h5, .main-banner-content h6',
                ]
            );

			$this->add_control(
				'content_color',
				[
					'label' => esc_html__( 'Content Color', 'guto-toolkit' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .main-banner-content p' => 'color: {{VALUE}}',
					],
				]
			);

			$this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'content_typography',
                    'label' => esc_html__( 'Content Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .main-banner-content p',
                ]
            );

        $this->end_controls_section();

    }

	protected function render() {

		$settings = $this->get_settings_for_display();

        // Inline Editing
        $this-> add_inline_editing_attributes('title','none');
        $this-> add_inline_editing_attributes('content','none');

        // Get Button Link
        if($settings['link_type'] == 1){
            $link = get_page_link( $settings['link_to_page'] );
        } elseif($settings['link_type'] == 2) {
            $link = $settings['ex_link']['url'];
            $target = $settings['ex_link']['is_external'] ? ' target="_blank"' : '';
		    $nofollow = $settings['ex_link']['nofollow'] ? ' rel="nofollow"' : '';
        }

        $button_text = $settings['button_text'];
		?>
            <div class="main-banner-area">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-12">
                            <div class="main-banner-content">
                                <span class="sub-title"><?php echo esc_html( $settings['top_title'] ); ?></span>
                                <<?php echo esc_attr( $settings['title_tag'] ); ?> <?php echo $this-> get_render_attribute_string('title'); ?>>
                                    <?php echo esc_html( $settings['title'] ); ?>
                                </<?php echo esc_attr( $settings['title_tag'] ); ?>>
                                <p <?php echo $this-> get_render_attribute_string('content'); ?>><?php echo wp_kses_post( $settings['content'] ); ?></p>

                                <?php if( $button_text != '' ): ?>
                                    <?php if($settings['link_type'] == 1): ?>
                                        <a href="<?php echo esc_url( $link ); ?>" class="default-btn"><i class="<?php echo esc_attr( $settings['button_icon'] ); ?>"></i> <?php echo esc_html( $button_text ); ?></a>
                                    <?php elseif($settings['link_type'] == 2):
                                        echo '<a href="' .  $link . '" class="default-btn" ' . $target . $nofollow . '> <i class="'.$settings['button_icon'].'"></i>' . $button_text . ' </a>';
                                    endif; ?>
                                <?php endif; ?>

                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <div class="main-banner-image">
                                <?php if( $settings['image1']['url'] != '' ): ?>
                                    <img src="<?php echo esc_url($settings['image1']['url']); ?>" alt="<?php echo esc_attr($settings['title']); ?>">
                                <?php endif; ?>
                                <?php if( $settings['image2']['url'] != '' ): ?>
                                    <img src="<?php echo esc_url($settings['image2']['url']); ?>" alt="<?php echo esc_attr($settings['title']); ?>">
                                <?php endif; ?>

                                <?php if( $settings['shape1']['url'] != '' ): ?>
                                    <div class="shape6">
                                        <img src="<?php echo esc_url($settings['shape1']['url']); ?>" alt="<?php esc_attr_e('Shape Image', 'guto-toolkit'); ?>">
                                    </div>
                                <?php endif; ?>

                                <?php if( $settings['shape2']['url'] != '' ): ?>
                                    <div class="shape5">
                                        <img src="<?php echo esc_url($settings['shape2']['url']); ?>" alt="<?php esc_attr_e('Shape Image', 'guto-toolkit'); ?>">
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if( $settings['shape3']['url'] != '' ): ?>
                    <div class="shape1">
                        <img src="<?php echo esc_url($settings['shape3']['url']); ?>" alt="<?php esc_attr_e('Shape Image', 'guto-toolkit'); ?>">
                    </div>
                <?php endif; ?>

                <?php if( $settings['shape4']['url'] != '' ): ?>
                    <div class="shape2">
                        <img src="<?php echo esc_url($settings['shape4']['url']); ?>" alt="<?php esc_attr_e('Shape Image', 'guto-toolkit'); ?>">
                    </div>
                <?php endif; ?>

                <?php if( $settings['shape5']['url'] != '' ): ?>
                    <div class="shape3">
                        <img src="<?php echo esc_url($settings['shape5']['url']); ?>" alt="<?php esc_attr_e('Shape Image', 'guto-toolkit'); ?>">
                    </div>
                <?php endif; ?>

                <?php if( $settings['shape6']['url'] != '' ): ?>
                    <div class="shape4">
                        <img src="<?php echo esc_url($settings['shape6']['url']); ?>" alt="<?php esc_attr_e('Shape Image', 'guto-toolkit'); ?>">
                    </div>
                <?php endif; ?>

                <?php if( $settings['shape7']['url'] != '' ): ?>
                    <div class="shape7">
                        <img src="<?php echo esc_url($settings['shape7']['url']); ?>" alt="<?php esc_attr_e('Shape Image', 'guto-toolkit'); ?>">
                    </div>
                <?php endif; ?>

            </div>
        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Banner_One );