<?php
/**
 * Services Widget
 */

namespace Elementor;
class Guto_Services extends Widget_Base {

	public function get_name() {
        return 'Services';
    }

	public function get_title() {
        return esc_html__( 'Services', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-tools';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'services_section',
			[
				'label' => esc_html__( 'Services', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

            $this->add_control(
                'card_style',
                [
                    'label' => esc_html__( 'Choose Style', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        '1'   => esc_html__( 'Style One', 'guto-toolkit' ),
                        '2'   => esc_html__( 'Style Two', 'guto-toolkit' ),
                    ],
                    'default' => '1',
                ]
            );

            $this->add_control(
                'columns',
                [
                    'label' => esc_html__( 'Choose Columns', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        '1'   => esc_html__( '1', 'guto-toolkit' ),
                        '2'   => esc_html__( '2', 'guto-toolkit' ),
                        '3'   => esc_html__( '3', 'guto-toolkit' ),
                        '4'   => esc_html__( '4', 'guto-toolkit' ),
                    ],
                    'default' => '3',
                ]
            );

            $this->add_control(
                'read_more',
                [
                    'label' => esc_html__( 'Read More Text', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__('Learn More', 'guto-toolkit'),
                ]
            );

            $this->add_control(
                'cat', [
                    'label' => esc_html__( 'Category', 'guto-toolkit' ),
                    'description' => esc_html__( 'Enter the category slugs separated by commas (Eg. cat1, cat2)', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'label_block' => true,
                ]
            );

            $this->add_control(
                'order',
                [
                    'label' => esc_html__( 'Services Order By', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        'DESC'      => esc_html__( 'DESC', 'guto-toolkit' ),
                        'ASC'       => esc_html__( 'ASC', 'guto-toolkit' ),
                    ],
                    'default' => 'DESC',
                ]
            );

            $this->add_control(
                'count',
                [
                    'label' => esc_html__( 'Post Per Page', 'guto-toolkit' ),
                    'type' => Controls_Manager::NUMBER,
                    'default' => 3,
                ]
            );

        $this->end_controls_section();

        $this->start_controls_section(
			'service_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
        );

            $this->add_control(
                'title_color',
                [
                    'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .single-featured-services-box h3 a, .single-services-box h3 a' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-featured-services-box h3, .single-services-box h3',
                ]
            );

            $this->add_control(
                'content_color',
                [
                    'label' => esc_html__( 'Content Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .single-featured-services-box p, .single-services-box p' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'content_typography',
                    'label' => esc_html__( 'Content Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-featured-services-box p, .single-services-box p',
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'read_more_typography',
                    'label' => esc_html__( 'Red More Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-featured-services-box .link-btn, .single-services-box .link-btn',
                ]
            );

        $this->end_controls_section();

    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        // Card Columns
        $columns = $settings['columns'];
        if ($columns == '1') {
            $column = 'col-lg-12 col-sm-6 col-md-6';
        }elseif ($columns == '2') {
            $column = 'col-lg-6 col-sm-6 col-md-6';
        }elseif ($columns == '3') {
            $column = 'col-lg-4 col-sm-6 col-md-6';
        }elseif ($columns == '4') {
            $column = 'col-lg-3 col-sm-6 col-md-6';
        }
        
        // Services Query
        if( $settings['cat'] != '' ) {
            $args = array(
                'post_type'     => 'services',
                'posts_per_page'=> $settings['count'],
                'order'         => $settings['order'],
                'tax_query'     => array(
                    array(
                        'taxonomy'      => 'service_cat',
                        'field'         => 'slug',
                        'terms'         => $settings['cat'],
                        'hide_empty'    => false
                    )
                )
            );
        } else {
            $args = array(
                'post_type'         => 'services',
                'posts_per_page'    => $settings['count'],
                'order'             => $settings['order']
            );
        }
        $services_array = new \WP_Query( $args );
                
        ?>

        <?php if( $settings['card_style']  == '1' ): ?>
            <div class="container">
                <div class="row justify-content-center">
                    <?php while($services_array->have_posts()): $services_array->the_post(); ?>
                        <div class="<?php echo esc_attr( $column );?>">
                            <div class="single-featured-services-box">
                                <?php if( get_the_post_thumbnail_url() ): ?>
                                    <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                                <?php endif; ?>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></h3>
                                <p><?php the_excerpt(); ?></p>
                                <?php if( $settings['read_more'] != '' ): ?>
                                    <a href="<?php the_permalink(); ?>" class="link-btn"><?php echo esc_html( $settings['read_more'] ); ?> <i class='bx bx-right-arrow-alt'></i></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if( $settings['card_style']  == '2' ): ?>
            <div class="container">
                <div class="row justify-content-center">
                    <?php while($services_array->have_posts()): $services_array->the_post(); 

                        // Card Icon
                        if ( defined( 'FW' ) ) {
                            $icon = fw_get_db_post_option( get_the_ID(), 'services_icon' );
                        }else{
                            $icon = '';
                        } ?>
                        <div class="<?php echo esc_attr( $column );?>">
                            <div class="single-services-box">
                                <?php if( $icon != '' ): ?>
                                    <div class="icon">
                                        <i class="<?php echo esc_attr($icon); ?>"></i>
                                    </div>
                                <?php endif; ?>
                                <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></h3>
                                <p><?php the_excerpt(); ?></p>
                                <?php if( $settings['read_more'] != '' ): ?>
                                    <a href="<?php the_permalink(); ?>" class="link-btn"><?php echo esc_html( $settings['read_more'] ); ?> <i class='bx bx-right-arrow-alt'></i></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
        <?php endif; ?>

        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Services );