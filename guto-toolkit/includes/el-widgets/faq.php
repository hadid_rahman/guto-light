<?php
/**
 * FAQ Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Faq extends Widget_Base {

	public function get_name() {
        return 'FAQ';
    }

	public function get_title() {
        return esc_html__( 'Guto FAQ', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-help-o';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Faq',
			[
				'label' => esc_html__( 'Faq Control', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

		$this->add_control(
            'faq_tabs',
            [
                'label' => esc_html__('Faq Tabs', 'guto-toolkit'),
                'type' => Controls_Manager::REPEATER,
                'separator' => 'before',
                'fields' => [
					[	
						'name'	=> 'title',
						'label' => esc_html__( 'Title', 'guto-toolkit' ),
						'type' => Controls_Manager::TEXT,
						'default' => esc_html__( 'How does Guto Production house Work?', 'guto-toolkit' ),
					],
            
                    [
                        'name'  => 'content',
                        'label' => esc_html__( 'Content', 'guto-toolkit' ),
                        'type' => Controls_Manager::TEXTAREA,
                        'label_block' => true,
						'default' => "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.",
                    ],
                ],
            ]
        );
       
        $this->end_controls_section();

        $this->start_controls_section(
			'partner_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
        );

        $this->add_control(
            'title_color',
            [
                'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .faq-accordion .card .card-header button' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .faq-accordion .card .card-header button',
            ]
        );

        $this->add_control(
            'content_color',
            [
                'label' => esc_html__( 'Content Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .card-body p, .card-body li' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'label' => esc_html__( 'Content Typography', 'guto-toolkit' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .card-body p, .card-body li',
            ]
        );

        $this->end_controls_section();

    }

	protected function render() {

        $settings = $this->get_settings_for_display();
        ?>
            <div class="faq-accordion accordion" id="faqAccordion">
                <?php $i = 1; foreach( $settings['faq_tabs'] as $item ): ?>
                    <div class="card">
                        <div class="card-header">
                            <button class="collapsed" type="button" data-toggle="collapse" data-target="#collapse<?php echo esc_attr($i); ?>" aria-expanded="false" aria-controls="collapse<?php echo esc_attr($i); ?>">
                                <?php echo esc_html( $item['title'] ); ?>
                            </button>
                        </div>

                        <div id="collapse<?php echo esc_attr($i); ?>" class="collapse" data-parent="#faqAccordion">
                            <div class="card-body">
                                <?php echo wp_kses_post( $item['content'] ); ?>
                            </div>
                        </div>
                    </div>
                <?php $i++; endforeach; ?>
            </div>
        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Faq );