<?php
/**
 * Product Grid Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Products_Grid extends Widget_Base {

	public function get_name() {
        return 'Products_Grid';
    }

	public function get_title() {
        return esc_html__( 'Products Grid', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-products';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
	}
	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Products_Grid',
			[
				'label' => esc_html__( 'Guto Products', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );  
            
            $this->add_control(
                'style',
                [
                    'label' => esc_html__( 'Section Style', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        '1'         => esc_html__( 'Style One', 'guto-toolkit' ),
                        '2'         => esc_html__( 'Style Two', 'guto-toolkit' ),
                    ],
                    'default' => '1',
                ]
            );

            $this->add_control(
                'section_title', [
                    'label'     => esc_html__( 'Title', 'guto-toolkit' ),
                    'type'      => Controls_Manager::TEXT,
                    'default'   => esc_html__('Best Selling Products', 'guto-toolkit') 
                ]
            );

            $this->add_control(
                'section_desc', [
                    'label'     => esc_html__( 'Description', 'guto-toolkit' ),
                    'type'      => Controls_Manager::WYSIWYG,
                    'default'   => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.', 'guto-toolkit') 
                ]
            );

            $this->add_control(
                'image',
                [
                    'label' => esc_html__( 'Products Big Image', 'guto-toolkit' ),
                    'type'   => Controls_Manager::MEDIA,
                    'default' => [
                        'url' => plugin_dir_url( dirname( __FILE__ ) ) . '../public/img/big-products.jpg'
                    ],
                    'show_label' => true,
                    'condition' => [
                        'style' => '2',
                    ]
                ]
            );

            $this->add_control(
                'title',
                [
                    'label' => esc_html__( 'Title', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__("Trending Items", 'guto-toolkit'),
                    'condition' => [
                        'style' => '2',
                    ]
                ]
            );

            $this->add_control(
                'cat_products_number',
                [
                    'label' => esc_html__( 'Number of Products', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__("35 Items", 'guto-toolkit'),
                    'condition' => [
                        'style' => '2',
                    ]
                ]
            );

            $this->add_control(
                'button_text',
                [
                    'label'     => esc_html__( 'Button Text', 'guto-toolkit' ),
                    'type'      => Controls_Manager::TEXT,
                    'default'   => esc_html__("Shop Now", 'guto-toolkit'),
                    'condition' => [
                        'style' => '2',
                    ]
                ]
            );

            $this->add_control(
                'button_icon',
                [
                    'label' => esc_html__( 'Button Icon', 'guto-toolkit' ),
                    'type' => Controls_Manager::ICON,
                    'label_block' => true,
                    'default' => 'bx bx-shopping-bag',
                    'options' => guto_toolkit_icons(),
                    'condition' => [
                        'style' => '2',
                    ]
                ]
            );

            $this->add_control(
                'link_type',
                [
                    'label'         => esc_html__( 'Button Link Type', 'guto-toolkit' ),
                    'type'          => Controls_Manager::SELECT,
                    'label_block'   => true,
                    'options' => [
                        '1'     => esc_html__( 'Link To Page', 'guto-toolkit' ),
                        '2'     => esc_html__( 'External Link', 'guto-toolkit' ),
                    ],
                    'default' => '2',
                    'condition' => [
                        'style' => '2',
                    ]
                ]
            );

            $this->add_control(
                'link_to_page',
                [
                    'label'         => esc_html__( 'Button Link Page', 'guto-toolkit' ),
                    'type'          => Controls_Manager::SELECT,
                    'label_block'   => true,
                    'options'       => guto_toolkit_get_page_as_list(),
                    'condition' => [
                        'style' => '2',
                        'link_type' => '1'
                    ]
                ]
            );

            $this->add_control(
                'ex_link',
                [
                    'label'     => esc_html__('Button External Link', 'guto-toolkit'),
                    'type'      => Controls_Manager:: URL,
                    'show_external' => true,
                    'default' => [
                        'url' => '#',
                        'is_external' => true,
                        'nofollow' => true,
                    ],
                    'condition' => [
                        'style' => '2',
                        'link_type' => '2'
                    ]
                ]
            );

            $this->add_control(
                'show_product',
                [
                    'label' => esc_html__( 'Show Product By', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        'recent'      => esc_html__( 'Recent products', 'guto-toolkit' ),
                        'trending'    => esc_html__( 'Trending Products', 'guto-toolkit' ),
                        'sellers'     => esc_html__( 'Best Sellers', 'guto-toolkit' ),
                    ],
                    'default' => 'recent',
                ]
            );

            $this->add_control(
                'category_name', [
                    'label' => esc_html__( 'Category Name', 'guto-toolkit' ),
                    'description' => esc_html__( 'Enter the category name slug. Use comma to separate multiple category slugs.', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                ]
            );

            $this->add_control(
                'order',
                [
                    'label' => esc_html__( 'Products Order By', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        'DESC'      => esc_html__( 'DESC', 'guto-toolkit' ),
                        'ASC'       => esc_html__( 'ASC', 'guto-toolkit' ),
                    ],
                    'default' => 'DESC',
                ]
            );

            $this->add_control(
                'count',
                [
                    'label' => esc_html__( 'Item', 'guto-toolkit' ),
                    'type' => Controls_Manager::NUMBER,
                    'default' => 4,
                ]
            );


        $this->end_controls_section();

        $this->start_controls_section(
            'products_grid_style',
            [
                'label' => esc_html__( 'Style', 'guto-toolkit' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );  

            $this->add_control(
                'products_grid_bg_color',
                [
                    'label' => esc_html__( 'Section Background Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .products-area' => 'background-color: {{VALUE}}',
                    ],
                ]
            );

        $this->end_controls_section();

    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        if ( !guto_toolkit_plugin_active( 'woocommerce/woocommerce.php' ) ) {
            if( is_user_logged_in() ):
                ?>
                <div class="container">
                    <div class="alert alert-danger" role="alert">
                        <?php echo esc_html__( 'Please Install and activated WooCommerce plugin', 'guto-toolkit' ); ?>
                    </div>
                </div>
                <?php
            endif;
			return;
		}

         // Post Query
         if (!empty($settings['category_name'])) {
            if( $settings['show_product'] == 'recent' ):
                $posts = new \WP_Query(array(
                    'post_type'         => 'product',
                    'posts_per_page'    => $settings['count'],
                    'order'             => $settings['order'],
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_cat',
                            'field'    => 'slug',
                            'terms'    => explode( ',', str_replace( ' ', '', $settings['category_name'])),
                        ),
                    ),
                ));
            elseif( $settings['show_product'] == 'trending' ):
                $posts = new \WP_Query(array(
                    'post_type'             => 'product',
                    'posts_per_page'        => $settings['count'],
                    'order'                 => $settings['order'],
                    'meta_key'              => 'guto_post_views_count',
                    'ignore_sticky_posts'   => 1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_cat',
                            'field'    => 'slug',
                            'terms'    => explode( ',', str_replace( ' ', '', $settings['category_name'])),
                        ),
                    ),

                ));

            elseif( $settings['show_product'] == 'sellers' ):
                $posts = new \WP_Query(array(
                    'post_type'             => 'product',
                    'posts_per_page'        => $settings['count'],
                    'order'                 => $settings['order'],
                    'meta_key'              => 'total_sales',
                    'orderby'               => 'meta_value_num',
                    'ignore_sticky_posts'   => 1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_cat',
                            'field'    => 'slug',
                            'terms'    => explode( ',', str_replace( ' ', '', $settings['category_name'])),
                        ),
                    ),
                ));
            endif;
        }
        else {
            if( $settings['show_product'] == 'recent' ):
                $posts = new \WP_Query(array(
                    'post_type'         => 'product',
                    'posts_per_page'    => $settings['count'],
                    'order'             => $settings['order'],
                ));
            elseif( $settings['show_product'] == 'trending' ):
                $posts = new \WP_Query(array(
                    'post_type'             => 'product',
                    'posts_per_page'        => $settings['count'],
                    'order'                 => $settings['order'],
                    'meta_key'              => 'guto_post_views_count',
                    'ignore_sticky_posts'   => 1,
                ));

            elseif( $settings['show_product'] == 'sellers' ):
                $posts = new \WP_Query(array(
                    'post_type'             => 'product',
                    'posts_per_page'        => $settings['count'],
                    'order'                 => $settings['order'],
                    'meta_key'              => 'total_sales',
                    'orderby'               => 'meta_value_num',
                    'ignore_sticky_posts'   => 1,
                ));
            endif;
        }

		if( $settings['style'] == '1' ): ?>

            <!-- Start Best Selling Products Area -->
            <section class="products-area pt-100 pb-70">
                <div class="container">
                    <?php if( ! $settings['section_title'] == '' && ! $settings['section_desc'] == '' || ! $settings['section_title'] == '' || ! $settings['section_desc'] == '' ) : ?>
                        <div class="section-title">
                            <h2><?php echo esc_html__( $settings['section_title'] ); ?></h2>
                            <p><?php echo wp_kses_post( $settings['section_desc'] ); ?></p>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <?php while($posts->have_posts()): $posts->the_post();  ?>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                    <div class="single-products-box">
                                        <div class="image">
                                            <a href="<?php the_permalink(); ?>" class="d-block">
                                                <?php the_post_thumbnail( 'full' ); ?>
                                            </a>
                                            <?php woocommerce_product_cart_style_fs();
                                            $wc_image_fs = wp_get_attachment_image_src( get_post_thumbnail_id( $posts->post->ID ), 'single-post-thumbnail' ); ?>
                                            <ul class="products-button">
                                                <li><a href="#" data-toggle="modal" data-target="#productsQuickView<?php echo esc_attr(get_the_ID()); ?>"><i class='bx bx-show-alt'></i></a></li>
                                                <li><a href="<?php  echo $wc_image_fs[0]; ?>" class="popup-btn-fs"><i class='bx bx-scan'></i></a></li>
                                                <li><a href="<?php the_permalink(); ?>"><i class='bx bx-link'></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="content">
                                            <h3>
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                            </h3>
                                            <div class="price">
                                                <span class="new-price">
                                                    <?php woocommerce_template_loop_price(); ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Start QuickView Modal Area -->
                                <div class="modal fade productsQuickView" id="productsQuickView<?php echo esc_attr(get_the_ID()); ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true"><i class='bx bx-x'></i></span>
                                            </button>

                                            <div class="row align-items-center">
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="products-image">
                                                        <?php the_post_thumbnail( 'full' ); ?>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6">
                                                    <div class="products-content">
                                                        <h3><?php the_title(); ?></h3>

                                                        <div class="price">
                                                            <span class="new-price">
                                                                <?php woocommerce_template_loop_price(); ?>
                                                            </span>
                                                        </div>

                                                        <div class="products-review">
                                                            <?php woocommerce_template_loop_rating(); ?>
                                                        </div>

                                                        <?php woocommerce_template_single_excerpt(); ?>

                                                        <div class="products-add-to-cart">
                                                            <?php woocommerce_product_cart_style_fs_two(); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End QuickView Modal Area -->
                        <?php endwhile; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
            </section>
            <!-- End Best Selling Products Area --> 

        <?php elseif( $settings['style'] == '2' ):

            // Get Button Link
            if($settings['link_type'] == 1){
                $link = get_page_link( $settings['link_to_page'] );
            } elseif($settings['link_type'] == 2) {
                $link = $settings['ex_link']['url'];
                $target = $settings['ex_link']['is_external'] ? ' target="_blank"' : '';
                $nofollow = $settings['ex_link']['nofollow'] ? ' rel="nofollow"' : '';
            }

            $button_text = $settings['button_text']; ?>

            <!-- Start New Design Products Area -->
            <section class="products-area pt-100 pb-70">
                <div class="container">
                    <?php if( ! $settings['section_title'] == '' && ! $settings['section_desc'] == '' || ! $settings['section_title'] == '' || ! $settings['section_desc'] == '' ) : ?>
                        <div class="section-title">
                            <h2><?php echo esc_html__( $settings['section_title'] ); ?></h2>
                            <p><?php echo wp_kses_post( $settings['section_desc'] ); ?></p>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="single-products-item">
                                <img src="<?php echo esc_url( $settings['image']['url'] ); ?>" alt="image">
                                <div class="content">
                                    <h3><?php echo esc_html( $settings['title'] ); ?></h3>
                                    <span><?php echo esc_html( $settings['cat_products_number'] ); ?></span>
                                    <?php if( $button_text != '' ): ?>
                                        <?php if($settings['link_type'] == 1): ?>
                                            <a href="<?php echo esc_url( $link ); ?>" class="default-btn"><i class="<?php echo esc_attr( $settings['button_icon'] ); ?>"></i> <?php echo esc_html( $button_text ); ?></a>
                                        <?php elseif($settings['link_type'] == 2):
                                            echo '<a href="' .  $link . '" class="default-btn" ' . $target . $nofollow . '> <i class="'.$settings['button_icon'].'"></i>' . $button_text . ' </a>';
                                        endif; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="row">
                                <?php while($posts->have_posts()): $posts->the_post();  ?>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="single-products-box">
                                                <div class="image">
                                                    <a href="<?php the_permalink(); ?>" class="d-block">
                                                        <?php the_post_thumbnail( 'full' ); ?>
                                                    </a>
                                                    <?php woocommerce_product_cart_style_fs();
                                                    $wc_image_fs = wp_get_attachment_image_src( get_post_thumbnail_id( $posts->post->ID ), 'single-post-thumbnail' ); ?>
                                                    <ul class="products-button">
                                                        <li><a href="#" data-toggle="modal" data-target="#productsQuickView<?php echo esc_attr(get_the_ID()); ?>"><i class='bx bx-show-alt'></i></a></li>
                                                        <li><a href="<?php  echo $wc_image_fs[0]; ?>" class="popup-btn-fs"><i class='bx bx-scan'></i></a></li>
                                                        <li><a href="<?php the_permalink(); ?>"><i class='bx bx-link'></i></a></li>
                                                    </ul>
                                                </div>
                                                <div class="content">
                                                    <h3>
                                                        <a href="<?php the_permalink(); ?>">
                                                            <?php the_title(); ?>
                                                        </a>
                                                    </h3>
                                                    <div class="price">
                                                        <span class="new-price">
                                                            <?php woocommerce_template_loop_price(); ?>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Start QuickView Modal Area -->
                                        <div class="modal fade productsQuickView" id="productsQuickView<?php echo esc_attr(get_the_ID()); ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true"><i class='bx bx-x'></i></span>
                                                    </button>

                                                    <div class="row align-items-center">
                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="products-image">
                                                                <?php the_post_thumbnail( 'full' ); ?>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-6">
                                                            <div class="products-content">
                                                                <h3><?php the_title(); ?></h3>

                                                                <div class="price">
                                                                    <span class="new-price">
                                                                        <?php woocommerce_template_loop_price(); ?>
                                                                    </span>
                                                                </div>

                                                                <div class="products-review">
                                                                    <?php woocommerce_template_loop_rating(); ?>
                                                                </div>

                                                                <?php woocommerce_template_single_excerpt(); ?>

                                                                <div class="products-add-to-cart">
                                                                    <?php woocommerce_product_cart_style_fs_two(); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End QuickView Modal Area -->
                                <?php endwhile; ?>
                                <?php wp_reset_query(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End New Design Products Area -->           

        <?php endif;

	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Products_Grid );