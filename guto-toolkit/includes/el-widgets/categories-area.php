<?php
/**
 * Categories Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Categories extends Widget_Base {

	public function get_name() {
        return 'Categories';
    }

	public function get_title() {
        return esc_html__( 'Categories Area', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-product-categories';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

        $this->add_control(
            'section_style',
            [
                'label' => esc_html__( 'Section Style', 'guto-toolkit' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '1'   => esc_html__( 'Style One', 'guto-toolkit' ),
                    '2'   => esc_html__( 'Style Two', 'guto-toolkit' ),
                    '3'   => esc_html__( 'Style Three', 'guto-toolkit' ),
                ],
                'default' => '1',
            ]
        );

        $this->add_control(
            'columns',
            [
                'label' => esc_html__( 'Choose Columns', 'guto-toolkit' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '1'   => esc_html__( '1', 'guto-toolkit' ),
                    '2'   => esc_html__( '2', 'guto-toolkit' ),
                    '3'   => esc_html__( '3', 'guto-toolkit' ),
                    '4'   => esc_html__( '4', 'guto-toolkit' ),
                ],
                'default' => '3',
            ]
        );

		$this->add_control(
            'items',
            [
                'label' => esc_html__('Card Item', 'guto-toolkit'),
                'type' => Controls_Manager::REPEATER,
                'default' => [
                    [ 'name' => esc_html__(' Item #1', 'guto-toolkit') ],

                ],
                'fields' => [
                    [
                        'name'  => 'image',
                        'label' => esc_html__( 'Image', 'guto-toolkit' ),
                        'type' => Controls_Manager::MEDIA,
                    ],

                    [
                        'name' => 'title',
                        'label' => esc_html__('Title', 'guto-toolkit'),
                        'type' => Controls_Manager::TEXT,
                        'default' => esc_html__('Woman Shirts', 'guto-toolkit'),
                        'label_block' => true,
                    ],

                    [
                        'name' => 'cat_products_number',
                        'label' => esc_html__('Number of Products', 'guto-toolkit'),
                        'type' => Controls_Manager::TEXT,
                        'default' => esc_html__('35 Items', 'guto-toolkit'),
                        'label_block' => true,
                        'description' => esc_html__('This field will work only on Style Three', 'guto-toolkit'),
                    ],

                    [
                        'name' => 'button_icon',
                        'label' => esc_html__( 'Button Icon', 'guto-toolkit' ),
                        'type' => Controls_Manager::ICON,
                        'label_block' => true,
                        'options' => guto_toolkit_icons(),
                    ],

                    [
                        'name' => 'button_text',
                        'label' 	=> esc_html__( 'Button Text', 'guto-toolkit' ),
                        'type' 		=> Controls_Manager::TEXT,
                        'default' 	=> esc_html__('Shop Now', 'guto-toolkit'),
                    ],

                    [
                        'name' => 'link_type',
                        'label' 		=> esc_html__( 'Button Link Type', 'guto-toolkit' ),
                        'type' 			=> Controls_Manager::SELECT,
                        'label_block' 	=> true,
                        'options' => [
                            '1'  	=> esc_html__( 'Link To Page', 'guto-toolkit' ),
                            '2' 	=> esc_html__( 'External Link', 'guto-toolkit' ),
                        ],
                    ],

                    [
                        'name' => 'link_to_page',
                        'label' 		=> esc_html__( 'Button Link Page', 'guto-toolkit' ),
                        'type' 			=> Controls_Manager::SELECT,
                        'label_block' 	=> true,
                        'options' 		=> guto_toolkit_get_page_as_list(),
                        'condition' => [
                            'link_type' => '1',
                        ]
                    ],

                    [
                        'name' => 'ex_link',
                        'label'		=> esc_html__('Button External Link', 'guto-toolkit'),
                        'type'		=> Controls_Manager:: URL,
                        'show_external' => true,
                        'default' => [
                            'url' => '#',
                            'is_external' => true,
                            'nofollow' => true,
                        ],
                        'condition' => [
                            'link_type' => '2',
                        ]
                    ],
                ],
            ]
        );

    $this->end_controls_section();

    $this->start_controls_section(
        'style',
        [
            'label' => esc_html__( 'Style', 'guto-toolkit' ),
            'tab' => Controls_Manager::TAB_STYLE,
        ]
    );

        $this->add_control(
            'bg_color',
            [
                'label' => esc_html__( 'Section Background Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .categories-area' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .single-categories-box h1, .single-categories-box h2, .single-categories-box h3, .single-categories-box h4, .single-categories-box h5, .single-categories-box h6, .single-categories-item .content h3, .single-categories-box-fs h1, .single-categories-box-fs h2, .single-categories-box-fs h3, .single-categories-box-fs h4, .single-categories-box-fs h5, .single-categories-box-fs h6' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .single-categories-box h1, .single-categories-box h2, .single-categories-box h3, .single-categories-box h4, .single-categories-box h5, .single-categories-box h6, .single-categories-item .content h3, .single-categories-box-fs h1, .single-categories-box-fs h2, .single-categories-box-fs h3, .single-categories-box-fs h4, .single-categories-box-fs h5, .single-categories-box-fs h6',
            ]
        );

        $this->add_control(
            'button_color',
            [
                'label' => esc_html__( 'Button Background Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .single-categories-box .default-btn, .single-categories-item .content .default-btn, .single-categories-box-fs .content .default-btn' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'button_hover_color',
            [
                'label' => esc_html__( 'Button Hover Background Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .single-categories-box:hover .default-btn, .single-categories-item .content .default-btn:hover, .single-categories-box-fs:hover .default-btn' => 'background-color: {{VALUE}}',
                ],
            ]
        );

    $this->end_controls_section();

    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        $items = $settings['items'];

        // Card Columns
        $columns = $settings['columns'];
        if ($columns == '1') {
            $column = 'col-lg-12 col-sm-6 col-md-6';
        }elseif ($columns == '2') {
            $column = 'col-lg-6 col-sm-6 col-md-6';
        }elseif ($columns == '3') {
            $column = 'col-lg-4 col-sm-6 col-md-6';
        }elseif ($columns == '4') {
            $column = 'col-lg-3 col-sm-6 col-md-6';
        }

        ?>
        <?php if($settings['section_style'] == '1'): ?>
            <div class="categories-area pt-100 pb-70">
                <div class="container">
                    <div class="row justify-content-center">
                        <?php foreach ($items as $key => $value):

                            // Get Button Link
                            if($value['link_type'] == 1){
                                $link = get_page_link( $value['link_to_page'] );
                            } elseif($value['link_type'] == 2) {
                                $link = $value['ex_link']['url'];
                                $target = $value['ex_link']['is_external'] ? ' target="_blank"' : '';
                                $nofollow = $value['ex_link']['nofollow'] ? ' rel="nofollow"' : '';
                            }

                            $button_text = $value['button_text'];
                            ?>
                            <div class="<?php echo esc_attr($column); ?>">
                                <div class="single-categories-box">
                                    <?php if( $value['image']['url'] != '' ): ?>
                                        <img src="<?php echo esc_url( $value['image']['url'] ); ?>" alt="<?php echo esc_attr( $value['title'] ) ?>">
                                    <?php endif; ?>

                                    <h3><span><?php echo esc_html( $value['title'] ); ?></span></h3>

                                    <?php if( $button_text != '' ): ?>
                                        <?php if($value['link_type'] == 1): ?>
                                            <a href="<?php echo esc_url( $link ); ?>" class="default-btn"><i class="<?php echo esc_attr( $value['button_icon'] ); ?>"></i> <?php echo esc_html( $button_text ); ?></a>
                                        <?php elseif($value['link_type'] == 2):
                                            echo '<a href="' .  $link . '" class="default-btn" ' . $target . $nofollow . '> <i class="'.$value['button_icon'].'"></i>' . $button_text . ' </a>';
                                        endif; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if($settings['section_style'] == '2'): ?>
            <div class="categories-area pt-100 pb-70">
                <div class="container">
                    <div class="row justify-content-center">
                        <?php foreach ($items as $key => $value):

                            // Get Button Link
                            if($value['link_type'] == 1){
                                $link = get_page_link( $value['link_to_page'] );
                            } elseif($value['link_type'] == 2) {
                                $link = $value['ex_link']['url'];
                                $target = $value['ex_link']['is_external'] ? ' target="_blank"' : '';
                                $nofollow = $value['ex_link']['nofollow'] ? ' rel="nofollow"' : '';
                            }

                            $button_text = $value['button_text'];
                            ?>
                            <div class="<?php echo esc_attr($column); ?>">
                                <div class="single-categories-item">
                                    <?php if( $value['image']['url'] != '' ): ?>
                                        <img src="<?php echo esc_url( $value['image']['url'] ); ?>" alt="<?php echo esc_attr( $value['title'] ) ?>">
                                    <?php endif; ?>

                                    <div class="content">
                                        <h3><?php echo esc_html( $value['title'] ); ?></h3>

                                        <?php if( $button_text != '' ): ?>
                                            <?php if($value['link_type'] == 1): ?>
                                                <a href="<?php echo esc_url( $link ); ?>" class="default-btn"><i class="<?php echo esc_attr( $value['button_icon'] ); ?>"></i> <?php echo esc_html( $button_text ); ?></a>
                                            <?php elseif($value['link_type'] == 2):
                                                echo '<a href="' .  $link . '" class="default-btn" ' . $target . $nofollow . '> <i class="'.$value['button_icon'].'"></i>' . $button_text . ' </a>';
                                            endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if($settings['section_style'] == '3'): ?>
            <div class="categories-area pt-100 pb-70">
                <div class="container">
                    <div class="row justify-content-center">
                        <?php foreach ($items as $key => $value):

                            // Get Button Link
                            if($value['link_type'] == 1){
                                $link = get_page_link( $value['link_to_page'] );
                            } elseif($value['link_type'] == 2) {
                                $link = $value['ex_link']['url'];
                                $target = $value['ex_link']['is_external'] ? ' target="_blank"' : '';
                                $nofollow = $value['ex_link']['nofollow'] ? ' rel="nofollow"' : '';
                            }

                            $button_text = $value['button_text'];
                            ?>
                            <div class="<?php echo esc_attr($column); ?>">
                                <div class="single-categories-box-fs">
                                    <?php if( $value['image']['url'] != '' ): ?>
                                        <img src="<?php echo esc_url( $value['image']['url'] ); ?>" alt="<?php echo esc_attr( $value['title'] ) ?>">
                                    <?php endif; ?>
                                    <div class="content">
                                        <h3><?php echo esc_html( $value['title'] ); ?></h3>
                                        <span><?php echo esc_html( $value['cat_products_number'] ); ?></span>
                                        <?php if( $button_text != '' ): ?>
                                            <?php if($value['link_type'] == 1): ?>
                                                <a href="<?php echo esc_url( $link ); ?>" class="default-btn"><i class="<?php echo esc_attr( $value['button_icon'] ); ?>"></i> <?php echo esc_html( $button_text ); ?></a>
                                            <?php elseif($value['link_type'] == 2):
                                                echo '<a href="' .  $link . '" class="default-btn" ' . $target . $nofollow . '> <i class="'.$value['button_icon'].'"></i>' . $button_text . ' </a>';
                                            endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Categories );