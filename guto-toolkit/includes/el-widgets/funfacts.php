<?php
/**
 * Funfacts Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Funfacts extends Widget_Base {

	public function get_name() {
        return 'Guto_Funfacts';
    }

	public function get_title() {
        return esc_html__( 'Funfacts', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-counter';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'funfacts_section',
			[
				'label' => esc_html__( 'Funfacts Control', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$this->add_control(
				'bg_image',
				[
					'label' => esc_html__( 'Card Background Image', 'guto-toolkit' ),
					'type' => Controls_Manager::MEDIA,
				]
			);

			$this->add_control(
                'columns',
                [
                    'label' => esc_html__( 'Choose Columns', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        '1'   => esc_html__( '1', 'guto-toolkit' ),
                        '2'   => esc_html__( '2', 'guto-toolkit' ),
                        '3'   => esc_html__( '3', 'guto-toolkit' ),
                        '4'   => esc_html__( '4', 'guto-toolkit' ),
                    ],
                    'default' => '4',
                ]
            );

            $this->add_control(
                'items',
                [
                    'type'    => Controls_Manager::REPEATER,
                    'label'   => esc_html__( 'Add Item', 'guto-toolkit' ),			                 
                    'fields'  => array(	
                        array(
                            'type'    => Controls_Manager::NUMBER,
                            'name'    => 'number',
                            'label'   => esc_html__( 'Ending Number', 'guto-toolkit' ),
                            'default' => 10,
                        ),		
                        array(
                            'type'    => Controls_Manager::TEXT,
                            'name'    => 'title',
                            'label'   => esc_html__( 'Title', 'guto-toolkit' ),
						),
						array(
                            'type'    => Controls_Manager::TEXT,
                            'name'    => 'number_suffix',
                            'label'   => esc_html__( 'Number Suffix', 'guto-toolkit' ),
                        ),
                    ),	
                ]			
            );

        $this->end_controls_section();

        $this->start_controls_section(
			'counter_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

            $this->add_control(
                'counter_color',
                [
                    'label' => esc_html__( 'Number Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .single-funfacts-box h3' => 'color: {{VALUE}}',
                    ],
                ]
			);
			
			$this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'number_typography',
                    'label' => esc_html__( 'Number Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-funfacts-box h3',
                ]
            );

            $this->add_control(
				'title_color',
				[
					'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .single-funfacts-box p' => 'color: {{VALUE}}',
					],
				]
			);

			$this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-funfacts-box p',
                ]
            );

        $this->end_controls_section();
    }

	protected function render() {
		$settings = $this->get_settings_for_display();
		
		// Card Columns
        $columns = $settings['columns'];
        if ($columns == '1') {
            $column = 'col-lg-12 col-sm-6 col-md-6';
        }elseif ($columns == '2') {
            $column = 'col-lg-6 col-sm-6 col-md-6';
        }elseif ($columns == '3') {
            $column = 'col-lg-4 col-sm-6 col-md-6';
        }elseif ($columns == '4') {
            $column = 'col-lg-3 col-sm-6 col-md-6';
        }
        ?>
			<div class="container">
                <div class="row justify-content-center">
                    <?php foreach( $settings['items'] as $item ): ?>
                        <div class="<?php echo esc_attr( $column );?>">
							<div class="single-funfacts-box" style="background-image:url(<?php echo esc_url($settings['bg_image']['url']); ?>);">
                            	<h3><span class="odometer" data-count="<?php echo esc_attr( $item['number'] ); ?>">00</span><?php echo esc_html( $item['number_suffix'] ); ?></h3>

								<p><?php echo esc_html( $item['title'] ); ?></p>
							</div>
						</div>
                    <?php endforeach; ?>   
				</div>
			</div>
        <?php
	}
	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Funfacts );