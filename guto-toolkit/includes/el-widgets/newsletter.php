<?php
/**
 * Newsletter Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Newsletter extends Widget_Base {

	public function get_name() {
        return 'Guto_Newsletter';
    }

	public function get_title() {
        return __( 'Newsletter', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-email-field';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

    public function get_keywords() {
        return [ 'mailchimp', 'form', 'newsletter' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Newsletter',
			[
				'label' => esc_html__( 'Guto Newsletter', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );
            $this->add_control(
                'select_style',
                [
                    'label' => esc_html__( 'Newsletter Style', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        '1'            => esc_html__( 'Style One', 'guto-toolkit' ),
                        '2'            => esc_html__( 'Style Two', 'guto-toolkit' ),
                        '3'            => esc_html__( 'Style Three', 'guto-toolkit' ),
                    ],
                    'default' => '1',
                ]
            );

            $this->add_control(
                'action_url', [
                    'label' => esc_html__( 'Action URL', 'guto-toolkit' ),
                    'description' => esc_html__( 'Enter here your MailChimp action URL. <a href="https://doc.gutotheme.com/docs/tips-guides-troubleshoots/get-mailchimp-newsletter-form-action-url/" target="_blank"> How to </a>', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'label_block' => true,
                ]
            );

            $this->add_control(
                'top_title', [
                    'label' => esc_html__( 'Top Title', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__( 'GET UPDATES', 'guto-toolkit' ),
                    'label_block' => true,
                ]
            );

            $this->add_control(
                'title', [
                    'label' => esc_html__( 'Title', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__( 'Our Newsletter' , 'guto-toolkit' ),
                    'label_block' => true,
                ]
            );

            $this->add_control(
                'placeholder_text', [
                    'label' => esc_html__( 'Email Placeholder Text', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__( 'Enter your email address' , 'guto-toolkit' ),
                    'label_block' => true,
                ]
            );

            $this->add_control(
                'button_text', [
                    'label' => esc_html__( 'Button text', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__( 'Subscribe Now' , 'guto-toolkit' ),
                    'label_block' => true,
                ]
            );

            $this->add_control(
				'icon',
				[
					'label' => esc_html__( 'Button Icon', 'guto-toolkit' ),
                    'type' => Controls_Manager::ICON,
                    'label_block' => true,
                    'options' => guto_toolkit_icons(),
				]
            );

            $this->add_control(
				'image',
				[
					'label' => esc_html__( 'Section Image', 'guto-toolkit' ),
					'type' => Controls_Manager::MEDIA,
				]
            );
            $this->add_control(
				'image2',
				[
					'label' => esc_html__( 'Section Image Two', 'guto-toolkit' ),
					'type' => Controls_Manager::MEDIA,
                    'condition' => [
                        'select_style' => ['1'],
                    ]
				]
            );


        $this->end_controls_section();

        $this->start_controls_section(
			'newsletter_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
        );
            $this->add_control(
                'top_title_color',
                [
                    'label' => esc_html__( 'Top Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .subscribe-inner-area .sub-title, .subscribe-inner-area-two  .sub-title' => 'color: {{VALUE}} !important',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'top_title_typography',
                    'label' => esc_html__( 'Top Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .subscribe-inner-area .sub-title, .subscribe-inner-area-two  .sub-title',
                ]
            );

            $this->add_control(
                'title_color',
                [
                    'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .subscribe-inner-area h2, .subscribe-inner-area-two h2' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .subscribe-inner-area h2, .subscribe-inner-area-two h2',
                ]
            );

        $this->end_controls_section();
    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        // Inline Editing
        $this-> add_inline_editing_attributes('title','none');


        ?>
        <?php if( $settings['select_style'] == '1' ): ?>
            <section class="subscribe-area">
                <div class="container">
                    <div class="subscribe-inner-area ptb-100">

                        <?php if($settings['top_title']): ?>
                            <span class="sub-title"><?php echo esc_html($settings['top_title']); ?></span>
                        <?php endif; ?>

                        <?php if($settings['title']): ?>
                            <h2><?php echo esc_html($settings['title']); ?></h2>
                        <?php endif; ?>

                        <form class="mailchimp newsletter-form" method="post">
                            <label><i class='bx bx-envelope-open'></i></label>
                            <input type="email" class="input-newsletter memail" placeholder="<?php echo esc_attr($settings['placeholder_text']); ?>" name="EMAIL" required>

                            <?php if( $settings['button_text'] != '' ): ?>
                                <button type="submit" class="default-btn"><i class="<?php echo esc_attr( $settings['icon'] ); ?>"></i><?php echo esc_html( $settings['button_text'] ); ?><span></span></button>
                            <?php endif; ?>

                            <p class="mchimp-errmessage" style="display: none;"></p>
                            <p class="mchimp-sucmessage" style="display: none;"></p>
                        </form>

                        <?php if($settings['image']['url']): ?>
                            <div class="subscribe-shape1"><img src="<?php echo esc_url($settings['image']['url']);  ?>" alt="<?php echo esc_attr($settings['title']); ?>"></div>
                        <?php endif; ?>
                        <?php if($settings['image2']['url']): ?>
                            <div class="subscribe-shape2"><img src="<?php echo esc_url($settings['image2']['url']);  ?>" alt="<?php echo esc_attr($settings['title']); ?>"></div>
                        <?php endif; ?>
                    </div>
                </div>
            </section>
        <?php endif; ?>

        <?php if( $settings['select_style'] == '2' ): ?>
            <section class="subscribe-area-two ptb-100">
                <div class="container">
                    <div class="subscribe-inner-area-two ptb-100 jarallax" data-jarallax='{"speed": 0.3}' style="background-image:url(<?php echo esc_url($settings['image']['url']); ?>);">

                        <?php if($settings['top_title']): ?>
                            <span class="sub-title"><?php echo esc_html($settings['top_title']); ?></span>
                        <?php endif; ?>

                        <?php if($settings['title']): ?>
                            <h2><?php echo esc_html($settings['title']); ?></h2>
                        <?php endif; ?>

                        <form class="mailchimp newsletter-form" method="post">
                            <label><i class='bx bx-envelope-open'></i></label>
                            <input type="email" class="input-newsletter memail" placeholder="<?php echo esc_attr($settings['placeholder_text']); ?>" name="EMAIL" required>

                            <?php if( $settings['button_text'] != '' ): ?>
                                <button type="submit" class="default-btn"><i class="<?php echo esc_attr( $settings['icon'] ); ?>"></i><?php echo esc_html( $settings['button_text'] ); ?><span></span></button>
                            <?php endif; ?>

                            <p class="mchimp-errmessage" style="display: none;"></p>
                            <p class="mchimp-sucmessage" style="display: none;"></p>
                        </form>
                    </div>
                </div>
            </section>
        <?php endif; ?>

        <?php if( $settings['select_style'] == '3' ): ?>
            <section class="subscribe-area-fs ptb-100 jarallax" data-jarallax='{"speed": 0.3}' style="background-image:url(<?php echo esc_url($settings['image']['url']); ?>);">
                <div class="container">
                    <div class="subscribe-inner-area-two ptb-100 jarallax">

                        <?php if($settings['top_title']): ?>
                            <span class="sub-title"><?php echo esc_html($settings['top_title']); ?></span>
                        <?php endif; ?>

                        <?php if($settings['title']): ?>
                            <h2><?php echo esc_html($settings['title']); ?></h2>
                        <?php endif; ?>

                        <form class="mailchimp newsletter-form" method="post">
                            <label><i class='bx bx-envelope-open'></i></label>
                            <input type="email" class="input-newsletter memail" placeholder="<?php echo esc_attr($settings['placeholder_text']); ?>" name="EMAIL" required>

                            <?php if( $settings['button_text'] != '' ): ?>
                                <button type="submit" class="default-btn"><i class="<?php echo esc_attr( $settings['icon'] ); ?>"></i><?php echo esc_html( $settings['button_text'] ); ?><span></span></button>
                            <?php endif; ?>

                            <p class="mchimp-errmessage" style="display: none;"></p>
                            <p class="mchimp-sucmessage" style="display: none;"></p>
                        </form>
                    </div>
                </div>
            </section>
        <?php endif; ?>

            <script>
                ;(function($){
                    "use strict";
                    $(document).ready(function () {
                        // MAILCHIMP
                        if ($(".mailchimp").length > 0) {
                            $(".mailchimp").ajaxChimp({
                                callback: mailchimpCallback,
                                url: "<?php echo esc_js($settings['action_url']) ?>"
                            });
                        }
                        if ($(".mailchimp_two").length > 0) {
                            $(".mailchimp_two").ajaxChimp({
                                callback: mailchimpCallback,
                                url: "<?php echo esc_js($settings['action_url']) ?>" //Replace this with your own mailchimp post URL. Don't remove the "". Just paste the url inside "".
                            });
                        }
                        $(".memail").on("focus", function () {
                            $(".mchimp-errmessage").fadeOut();
                            $(".mchimp-sucmessage").fadeOut();
                        });
                        $(".memail").on("keydown", function () {
                            $(".mchimp-errmessage").fadeOut();
                            $(".mchimp-sucmessage").fadeOut();
                        });
                        $(".memail").on("click", function () {
                            $(".memail").val("");
                        });

                        function mailchimpCallback(resp) {
                            if (resp.result === "success") {
                                $(".mchimp-errmessage").html(resp.msg).fadeIn(1000);
                                $(".mchimp-sucmessage").fadeOut(500);
                            } else if (resp.result === "error") {
                                $(".mchimp-errmessage").html(resp.msg).fadeIn(1000);
                            }
                        }
                    });
                })(jQuery)
            </script>
    <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Newsletter );