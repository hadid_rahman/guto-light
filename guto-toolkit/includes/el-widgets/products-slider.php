<?php
/**
 * Product Slider Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Products_Slider extends Widget_Base {

	public function get_name() {
        return 'Products_Slider';
    }

	public function get_title() {
        return esc_html__( 'Products Slider', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-post-slider';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
	}
	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Products_Slider',
			[
				'label' => esc_html__( 'Guto Products', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

            $this->add_control(
                'show_product',
                [
                    'label' => esc_html__( 'Show Product By', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        'recent'      => esc_html__( 'Recent products', 'guto-toolkit' ),
                        'trending'    => esc_html__( 'Trending Products', 'guto-toolkit' ),
                        'sellers'     => esc_html__( 'Best Sellers', 'guto-toolkit' ),
                    ],
                    'default' => 'recent',
                ]
            );

            $this->add_control(
                'category_name', [
                    'label' => esc_html__( 'Category Name', 'guto-toolkit' ),
                    'description' => esc_html__( 'Enter the category name slug. Use comma to separate multiple category slugs.', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                ]
            );

            $this->add_control(
                'order',
                [
                    'label' => esc_html__( 'Posts Order By', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        'DESC'      => esc_html__( 'DESC', 'guto-toolkit' ),
                        'ASC'       => esc_html__( 'ASC', 'guto-toolkit' ),
                    ],
                    'default' => 'DESC',
                ]
            );

            $this->add_control(
                'count',
                [
                    'label' => esc_html__( 'Item', 'guto-toolkit' ),
                    'type' => Controls_Manager::NUMBER,
                    'default' => 5,
                ]
            );


        $this->end_controls_section();

    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        if ( !guto_toolkit_plugin_active( 'woocommerce/woocommerce.php' ) ) {
            if( is_user_logged_in() ):
                ?>
                <div class="container">
                    <div class="alert alert-danger" role="alert">
                        <?php echo esc_html__( 'Please Install and activated WooCommerce plugin', 'guto-toolkit' ); ?>
                    </div>
                </div>
                <?php
            endif;
			return;
		}

         // Post Query
         if (!empty($settings['category_name'])) {
            if( $settings['show_product'] == 'recent' ):
                $posts = new \WP_Query(array(
                    'post_type'         => 'product',
                    'posts_per_page'    => $settings['count'],
                    'order'             => $settings['order'],
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_cat',
                            'field'    => 'slug',
                            'terms'    => explode( ',', str_replace( ' ', '', $settings['category_name'])),
                        ),
                    ),
                ));
            elseif( $settings['show_product'] == 'trending' ):
                $posts = new \WP_Query(array(
                    'post_type'             => 'product',
                    'posts_per_page'        => $settings['count'],
                    'order'                 => $settings['order'],
                    'meta_key'              => 'guto_post_views_count',
                    'ignore_sticky_posts'   => 1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_cat',
                            'field'    => 'slug',
                            'terms'    => explode( ',', str_replace( ' ', '', $settings['category_name'])),
                        ),
                    ),

                ));

            elseif( $settings['show_product'] == 'sellers' ):
                $posts = new \WP_Query(array(
                    'post_type'             => 'product',
                    'posts_per_page'        => $settings['count'],
                    'order'                 => $settings['order'],
                    'meta_key'              => 'total_sales',
                    'orderby'               => 'meta_value_num',
                    'ignore_sticky_posts'   => 1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_cat',
                            'field'    => 'slug',
                            'terms'    => explode( ',', str_replace( ' ', '', $settings['category_name'])),
                        ),
                    ),
                ));
            endif;
        }
        else {
            if( $settings['show_product'] == 'recent' ):
                $posts = new \WP_Query(array(
                    'post_type'         => 'product',
                    'posts_per_page'    => $settings['count'],
                    'order'             => $settings['order'],
                ));
            elseif( $settings['show_product'] == 'trending' ):
                $posts = new \WP_Query(array(
                    'post_type'             => 'product',
                    'posts_per_page'        => $settings['count'],
                    'order'                 => $settings['order'],
                    'meta_key'              => 'guto_post_views_count',
                    'ignore_sticky_posts'   => 1,
                ));

            elseif( $settings['show_product'] == 'sellers' ):
                $posts = new \WP_Query(array(
                    'post_type'             => 'product',
                    'posts_per_page'        => $settings['count'],
                    'order'                 => $settings['order'],
                    'meta_key'              => 'total_sales',
                    'orderby'               => 'meta_value_num',
                    'ignore_sticky_posts'   => 1,
                ));
            endif;
        }

		?>
        <div class="container">
            <?php $i = 0; while($posts->have_posts()): $posts->the_post();  ?>
            <?php ++$i; endwhile; ?>

            <?php if( $i == 1 ): ?>
                <div class="row four-grid-card text-align-center">
                    <?php while($posts->have_posts()): $posts->the_post(); ?>
                        <?php wc_get_template_part( 'content', 'product' ); ?>
                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </div>
            <?php else: ?>
                <ul class="products-slides owl-carousel owl-theme">
                    <?php while($posts->have_posts()): $posts->the_post(); ?>
                        <?php wc_get_template_part( 'content', 'product' ); ?>
                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </ul>
            <?php endif; ?>
        </div>

        <!-- Start Products Modal -->
        <?php while($posts->have_posts()): $posts->the_post();  ?>
                <div class="modal productsQuickView fade" id="productsQuickView<?php echo esc_attr(get_the_ID());?>" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="products-image">
                                    <?php the_post_thumbnail( 'full' ); ?>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="products-content">
                                        <h3><?php the_title(); ?></h3>
                                        <?php woocommerce_template_loop_price(); ?>
                                        <?php woocommerce_template_loop_rating(); ?>
                                        <?php woocommerce_template_single_excerpt(); ?>

                                        <?php woocommerce_template_single_add_to_cart(); ?>

                                        <div class="product-meta">
                                            <?php woocommerce_template_single_meta(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>
        <!-- End Products Modal -->

        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Products_Slider );