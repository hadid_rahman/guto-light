<?php
/**
 * Popup Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Popup extends Widget_Base {

	public function get_name() {
        return 'Popup';
    }

	public function get_title() {
        return esc_html__( 'Guto Popup', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-zoom-in';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Popup',
			[
				'label' => esc_html__( 'Popup Control', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

        $this->add_control(
            'columns',
            [
                'label' => esc_html__( 'Choose Columns', 'guto-toolkit' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '1'   => esc_html__( '1', 'guto-toolkit' ),
                    '2'   => esc_html__( '2', 'guto-toolkit' ),
                    '3'   => esc_html__( '3', 'guto-toolkit' ),
                    '4'   => esc_html__( '4', 'guto-toolkit' ),
                ],
                'default' => '2',
            ]
        );

		$this->add_control(
            'items',
            [
                'label' => esc_html__('Popup Items', 'guto-toolkit'),
                'type' => Controls_Manager::REPEATER,
                'separator' => 'before',
                'fields' => [
					[
						'name'	=> 'image',
						'label' => esc_html__( 'Image', 'guto-toolkit'),
                        'label_block' => true,
                        'type' => Controls_Manager::MEDIA,
					],
					[
						'name'	=> 'popup_type',
						'label' => esc_html__( 'Popup Type', 'guto-toolkit'),
                        'type' => Controls_Manager::SELECT,
                        'label_block' => true,
                        'options' => [
                            '1'   => esc_html__( 'YouTube Video', 'guto-toolkit' ),
                            '2'   => esc_html__( 'Image', 'guto-toolkit' ),
                        ],
                        'default' => '1',
                    ],
                    [
						'name'	=> 'link',
						'label' => esc_html__( 'YouTube Video Link', 'guto-toolkit'),
                        'type' => Controls_Manager::TEXT,
                        'label_block' => true,
                        'default' => 'https://www.youtube.com/watch?v=bk7McNUjWgw',
                        'condition' => [
                            'popup_type' => '1',
                        ]
                    ],
                    [
						'name'	=> 'icon',
						'label' => esc_html__( 'Icon', 'guto-toolkit'),
                        'type' => Controls_Manager::ICON,
                        'label_block' => true,
                        'options' => guto_toolkit_icons(),
					],
                ],
            ]
        );

        $this->end_controls_section();

    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        // Card Columns
        $columns = $settings['columns'];
        if ($columns == '1') {
            $column = 'col-lg-12 col-sm-6 col-md-6';
        }elseif ($columns == '2') {
            $column = 'col-lg-6 col-sm-6 col-md-6';
        }elseif ($columns == '3') {
            $column = 'col-lg-4 col-sm-6 col-md-6';
        }elseif ($columns == '4') {
            $column = 'col-lg-3 col-sm-6 col-md-6';
        }

        ?>
        <div class="container">
            <div class="row justify-content-center">
                <?php foreach( $settings['items'] as $item ): ?>
                    <div class="<?php echo esc_attr($column); ?>">
                        <?php if($item['image']['url'] != ''): ?>
                            <div class="project-details-image">
                                <img src="<?php echo esc_url($item['image']['url']); ?>" alt="<?php echo esc_attr_e('Project Images', 'guto-toolkit'); ?>">

                                <?php if($item['popup_type'] == '1'):  ?>
                                    <a href="<?php echo esc_url($item['link']); ?>" class="popup-youtube"><i class='<?php echo esc_attr($item['icon']); ?>'></i></a>
                                <?php endif; ?>

                                <?php if($item['popup_type'] == '2'):  ?>
                                    <a href="<?php echo esc_url($item['image']['url']); ?>" class="popup-btn"><i class='<?php echo esc_attr($item['icon']); ?>'></i></a>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Popup );