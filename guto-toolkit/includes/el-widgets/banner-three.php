<?php
/**
 * Banner Three Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Banner_Three extends Widget_Base {

	public function get_name() {
        return 'Guto_Banner_Three';
    }

	public function get_title() {
        return esc_html__( 'Banner Three', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-banner';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

		$this->add_control(
            'guto_banner_slider_items',
            [
                'label' => esc_html__('Slider Item', 'guto-toolkit'),
                'type' => Controls_Manager::REPEATER,
                'default' => [
                    [ 'name' => esc_html__(' Item #1', 'guto-toolkit') ],
         
                ],
                'fields' => [
                    [   
                        'name'  => 'top_title',
                        'label'     => esc_html__( 'Top Title', 'guto-toolkit' ),
                        'type'      => Controls_Manager::TEXT,
                        'default'   => esc_html__('NEW INSPIRATION 2021', 'guto-toolkit'),
                    ],
                    [   
                        'name'  => 'title',
                        'label'     => esc_html__( 'Title', 'guto-toolkit' ),
                        'type'      => Controls_Manager::TEXT,
                        'default'   => esc_html__('MADE WITH LOVE, INTERIOR DESIGNS!', 'guto-toolkit'),
                    ],
                    [   
                        'name'  => 'title_tag',
                        'label'     => esc_html__( 'Title Tag', 'guto-toolkit' ),
                        'type'      => Controls_Manager::SELECT,
                        'options'   => [
                            'h1'         => esc_html__( 'h1', 'guto-toolkit' ),
                            'h2'         => esc_html__( 'h2', 'guto-toolkit' ),
                            'h3'         => esc_html__( 'h3', 'guto-toolkit' ),
                            'h4'         => esc_html__( 'h4', 'guto-toolkit' ),
                            'h5'         => esc_html__( 'h5', 'guto-toolkit' ),
                            'h6'         => esc_html__( 'h6', 'guto-toolkit' ),
                        ],
                        'default' => 'h1',
                    ],
                    [
                        'name' => 'content',
                        'label'     => esc_html__( 'Content', 'guto-toolkit' ),
                        'type'      => Controls_Manager::TEXTAREA,
                        'default'   => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.', 'guto-toolkit'),
                    ],
                    [
                        'name'  => 'image',
                        'label' => esc_html__( 'Image', 'guto-toolkit' ),
                        'type' => Controls_Manager::MEDIA,
                        'default' => [
                            'url' => plugin_dir_url( dirname( __FILE__ ) ) . '../public/img/main-banner-bg1.jpg'
                        ],
                        'show_label' => true,
                    ],
                    [   
                        'name'  => 'button_icon', 
                        'label' => esc_html__( 'Button Icon', 'guto-toolkit' ),
                        'type' => Controls_Manager::ICON,
                        'label_block' => true,
                        'default' => 'bx bx-shopping-bag',
                        'options' => guto_toolkit_icons(),
                    ],
                    [   
                        'name'  => 'button_text', 
                        'label'     => esc_html__( 'Button Text', 'guto-toolkit' ),
                        'type'      => Controls_Manager::TEXT,
                        'default'   => esc_html__('Shop Now', 'guto-toolkit'),
                    ],
                    [   
                        'name'  => 'link_type', 
                        'label'         => esc_html__( 'Button Link Type', 'guto-toolkit' ),
                        'type'          => Controls_Manager::SELECT,
                        'label_block'   => true,
                        'options' => [
                            '1'     => esc_html__( 'Link To Page', 'guto-toolkit' ),
                            '2'     => esc_html__( 'External Link', 'guto-toolkit' ),
                        ],
                        'default' => '2',
                    ],
                    [   
                        'name'  => 'link_to_page',  
                        'label'         => esc_html__( 'Button Link Page', 'guto-toolkit' ),
                        'type'          => Controls_Manager::SELECT,
                        'label_block'   => true,
                        'options'       => guto_toolkit_get_page_as_list(),
                        'condition' => [
                            'link_type' => '1',
                        ]
                    ],
                    [   
                        'name'  => 'ex_link',  
                        'label'        => esc_html__('Button External Link', 'guto-toolkit'),
                        'type'      => Controls_Manager:: URL,
                        'show_external' => true,
                        'default' => [
                            'url' => '#',
                            'is_external' => true,
                            'nofollow' => true,
                        ],
                        'condition' => [
                            'link_type' => '2',
                        ]
                    ]
                ],
            ]
        );

    $this->end_controls_section();

    $this->start_controls_section(
            'banner_style',
            [
                'label' => esc_html__( 'Style', 'guto-toolkit' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

            $this->add_control(
                'top_title_color',
                [
                    'label' => esc_html__( 'Top Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .main-banner-content-fs .sub-title' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'top_title_typography',
                    'label' => esc_html__( 'Top Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .main-banner-content-fs .sub-title',
                ]
            );

            $this->add_control(
                'title_color',
                [
                    'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .main-banner-content-fs h1, .main-banner-content-fs h2, .main-banner-content-fs h3, .main-banner-content-fs h4, .main-banner-content-fs h5, .main-banner-content-fs h6' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .main-banner-content-fs h1, .main-banner-content-fs h2, .main-banner-content-fs h3, .main-banner-content-fs h4, .main-banner-content-fs h5, .main-banner-content-fs h6',
                ]
            );

            $this->add_control(
                'content_color',
                [
                    'label' => esc_html__( 'Content Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .main-banner-content-fs p' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'content_typography',
                    'label' => esc_html__( 'Content Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .main-banner-content-fs p',
                ]
            );

        $this->end_controls_section();


    }

	protected function render() {

        $settings = $this->get_settings_for_display();       

        $this-> add_inline_editing_attributes('title','none');
        $this-> add_inline_editing_attributes('content','none');

        $banner_slider = $settings['guto_banner_slider_items'];

        $count = 0;
        foreach ($banner_slider as $items => $counts): 
            $count++;
        endforeach;
        ?>

        <div class="home-slides owl-carousel owl-theme">
            <?php foreach ($banner_slider as $key => $value): 
                // Get Button Link
                if($value['link_type'] == 1){
                    $link = get_page_link( $value['link_to_page'] );
                } elseif($value['link_type'] == 2) {
                    $link = $value['ex_link']['url'];
                    $target = $value['ex_link']['is_external'] ? ' target="_blank"' : '';
                    $nofollow = $value['ex_link']['nofollow'] ? ' rel="nofollow"' : '';
                } 
                $button_text = $value['button_text']; ?>
                <div class="main-banner-area" style="background-image:url(<?php echo esc_url($value['image']['url']); ?>);">
                    <div class="container-fluid">
                        <div class="main-banner-content-fs">
                            <span class="sub-title"><?php echo esc_html( $value['top_title'] ); ?></span>
                            <<?php echo esc_attr( $value['title_tag'] ); ?> <?php echo $this-> get_render_attribute_string('title'); ?>>
                                <?php echo esc_html( $value['title'] ); ?>
                            </<?php echo esc_attr( $value['title_tag'] ); ?>>
                            <p <?php echo $this-> get_render_attribute_string('content'); ?>><?php echo wp_kses_post( $value['content'] ); ?></p>

                            <?php if( $button_text != '' ): ?>
                                <?php if($value['link_type'] == 1): ?>
                                    <a href="<?php echo esc_url( $link ); ?>" class="default-btn"><i class="<?php echo esc_attr( $value['button_icon'] ); ?>"></i> <?php echo esc_html( $button_text ); ?></a>
                                <?php elseif($value['link_type'] == 2):
                                    echo '<a href="' .  $link . '" class="default-btn" ' . $target . $nofollow . '> <i class="'.$value['button_icon'].'"></i>' . $button_text . ' </a>';
                                endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Banner_Three );