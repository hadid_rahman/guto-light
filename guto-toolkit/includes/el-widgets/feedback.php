<?php
/**
 * Feedback Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Feedback extends Widget_Base {

	public function get_name() {
        return 'Feedback';
    }

	public function get_title() {
        return esc_html__( 'Feedback', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-testimonial';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

        $this->add_control(
            'top_title',
            [
                'label' => esc_html__( 'Top Title', 'guto-toolkit' ),
                'type' => Controls_Manager::TEXT,
                'default' => esc_html__('TESTIMONIALS', 'guto-toolkit'),
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => esc_html__( 'Title', 'guto-toolkit' ),
                'type' => Controls_Manager::TEXT,
                'default' => esc_html__('What Clients Say About Us', 'guto-toolkit'),
            ]
        );

        $this->add_control(
            'title_tag',
            [
                'label' => esc_html__( 'Title Tag', 'guto-toolkit' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'h1'         => esc_html__( 'h1', 'guto-toolkit' ),
                    'h2'         => esc_html__( 'h2', 'guto-toolkit' ),
                    'h3'         => esc_html__( 'h3', 'guto-toolkit' ),
                    'h4'         => esc_html__( 'h4', 'guto-toolkit' ),
                    'h5'         => esc_html__( 'h5', 'guto-toolkit' ),
                    'h6'         => esc_html__( 'h6', 'guto-toolkit' ),
                ],
                'default' => 'h2',
            ]
        );

		$this->add_control(
            'guto_feedback_items',
            [
                'label' => esc_html__('Slider Item', 'guto-toolkit'),
                'type' => Controls_Manager::REPEATER,
                'default' => [
                    [ 'name' => esc_html__(' Item #1', 'guto-toolkit') ],
         
                ],
                'fields' => [
                    [
                        'name'  => 'image',
                        'label' => esc_html__( 'Image', 'guto-toolkit' ),
                        'type' => Controls_Manager::MEDIA,
                    ],
                    [
                        'name' => 'name',
                        'label' => esc_html__('Name', 'guto-toolkit'),
                        'type' => Controls_Manager::TEXT,
                        'default' => esc_html__('Olivar Lucy', 'guto-toolkit'),
                        'label_block' => true,
                    ],
                    [
                        'name' => 'designation',
                        'label' => esc_html__('Designation', 'guto-toolkit'),
                        'type' => Controls_Manager::TEXT,
                        'default' => esc_html__('Designer', 'guto-toolkit'),
                        'label_block' => true,
                    ],
                    [
                        'name' => 'feedback',
                        'label' => esc_html__('Feedback Content', 'guto-toolkit'),
                        'type' => Controls_Manager::TEXTAREA,
                        'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.', 'guto-toolkit'),
                        'label_block' => true,
                    ],
                ],
            ]
        );

    $this->end_controls_section();

    $this->start_controls_section(
        'style',
        [
            'label' => esc_html__( 'Style', 'guto-toolkit' ),
            'tab' => Controls_Manager::TAB_STYLE,
        ]
    );	

        $this->add_control(
            'bg_color',
            [
                'label' => esc_html__( 'Section Background Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .testimonials-area.bg-f3f3f4' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'top_title_color',
            [
                'label' => esc_html__( 'Top Title Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .section-title .sub-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'top_title_typography',
                'label' => esc_html__( 'Top Title Typography', 'guto-toolkit' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .section-title .sub-title',
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .section-title h1, .section-title h2, .section-title h3, .section-title h4, .section-title h5, .section-title h6' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .section-title h1, .section-title h2, .section-title h3, .section-title h4, .section-title h5, .section-title h6',
            ]
        );
    
        $this->add_control(
            'feedback_color',
            [
                'label' => esc_html__( 'Feedback Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .single-testimonials-item p' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'feedback_typography',
                'label' => esc_html__( 'Feedback Typography', 'guto-toolkit' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .single-testimonials-item p',
            ]
        );
        
        $this->add_control(
            'name_color',
            [
                'label' => esc_html__( 'Name Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .single-testimonials-item .clients-info .info h3' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'name_typography',
                'label' => esc_html__( 'Name Typography', 'guto-toolkit' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .single-testimonials-item .clients-info .info h3',
            ]
        );

        $this->add_control(
            'designation_color',
            [
                'label' => esc_html__( 'Designation Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .single-testimonials-item .clients-info .info span' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'designation_typography',
                'label' => esc_html__( 'Designation Typography', 'guto-toolkit' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .single-testimonials-item .clients-info .info span',
            ]
        );
        
    $this->end_controls_section();

    }

	protected function render() {

        $settings = $this->get_settings_for_display();
        
        $slider = $settings['guto_feedback_items'];       

        // Inline Editing
        $this-> add_inline_editing_attributes('title','none');

        $count = 0;
        foreach ($slider as $items => $counts): 
            $count++;
        endforeach;
        ?>

        <div class="testimonials-area bg-f3f3f4 ptb-100">
            <div class="container">
                <div class="section-title">
                    <span class="sub-title"><?php echo esc_html( $settings['top_title'] ); ?></span>

                    <<?php echo esc_attr( $settings['title_tag'] ); ?> <?php echo $this-> get_render_attribute_string('title'); ?>>
                        <?php echo esc_html( $settings['title'] ); ?>
                    </<?php echo esc_attr( $settings['title_tag'] ); ?>>
                </div>

                <?php if( $count == 1 ): ?>
                    <div class="col-lg-12">
                <?php else: ?>
                    <div class="testimonials-slides owl-carousel owl-theme">
                <?php endif; ?>
                    <?php foreach ($slider as $key => $value): ?>
                        <div class="single-testimonials-item">
                            <p><?php echo wp_kses_post( $value['feedback'] ); ?></p>
                            <div class="clients-info d-flex align-items-center justify-content-center">

                                <?php if( $value['image']['url'] != '' ): ?>
                                    <img src="<?php echo esc_url( $value['image']['url'] ); ?>" alt="<?php echo esc_attr( $value['name'] ) ?>">
                                <?php endif; ?>

                                <div class="info">
                                    <h3><?php echo esc_html( $value['name'] ); ?></h3>
                                    <span><?php echo esc_html( $value['designation'] ); ?></span>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Feedback );