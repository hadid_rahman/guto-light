<?php
/**
 * Projects Widget
 */

namespace Elementor;
class Guto_Projects extends Widget_Base {

	public function get_name() {
        return 'Projects';
    }

	public function get_title() {
        return esc_html__( 'Projects', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-image';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'projects_section',
			[
				'label' => esc_html__( 'Projects', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

            $this->add_control(
                'columns',
                [
                    'label' => esc_html__( 'Choose Columns', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        '1'   => esc_html__( '1', 'guto-toolkit' ),
                        '2'   => esc_html__( '2', 'guto-toolkit' ),
                        '3'   => esc_html__( '3', 'guto-toolkit' ),
                        '4'   => esc_html__( '4', 'guto-toolkit' ),
                    ],
                    'default' => '3',
                ]
            );

            $this->add_control(
                'cat', [
                    'label' => esc_html__( 'Category', 'guto-toolkit' ),
                    'description' => esc_html__( 'Enter the category slugs separated by commas (Eg. cat1, cat2)', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'label_block' => true,
                ]
            );

            $this->add_control(
                'order',
                [
                    'label' => esc_html__( 'Projects Order By', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        'DESC'      => esc_html__( 'DESC', 'guto-toolkit' ),
                        'ASC'       => esc_html__( 'ASC', 'guto-toolkit' ),
                    ],
                    'default' => 'DESC',
                ]
            );

            $this->add_control(
                'count',
                [
                    'label' => esc_html__( 'Post Per Page', 'guto-toolkit' ),
                    'type' => Controls_Manager::NUMBER,
                    'default' => 3,
                ]
            );

            $this->add_control(
				'button_text',
				[
					'label' 	=> esc_html__( 'Button Text', 'guto-toolkit' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> esc_html__('Discover More Projects', 'guto-toolkit'),
				]
            );

            $this->add_control(
				'button_icon',
				[
					'label' => esc_html__( 'Button Icon', 'guto-toolkit' ),
                    'type' => Controls_Manager::ICON,
                    'label_block' => true,
                    'options' => guto_toolkit_icons(),
				]
            );

            $this->add_control(
                'link_type',
                [
                    'label' 		=> esc_html__( 'Button Link Type', 'guto-toolkit' ),
                    'type' 			=> Controls_Manager::SELECT,
                    'label_block' 	=> true,
                    'options' => [
                        '1'  	=> esc_html__( 'Link To Page', 'guto-toolkit' ),
                        '2' 	=> esc_html__( 'External Link', 'guto-toolkit' ),
                    ],
                ]
            );

            $this->add_control(
                'link_to_page',
                [
                    'label' 		=> esc_html__( 'Button Link Page', 'guto-toolkit' ),
                    'type' 			=> Controls_Manager::SELECT,
                    'label_block' 	=> true,
                    'options' 		=> guto_toolkit_get_page_as_list(),
                    'condition' => [
                        'link_type' => '1',
                    ]
                ]
            );

            $this->add_control(
                'ex_link',
                [
                    'label'		=> esc_html__('Button External Link', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: URL,
                    'show_external' => true,
                    'default' => [
                        'url' => '#',
                        'is_external' => true,
                        'nofollow' => true,
                    ],
                    'condition' => [
                        'link_type' => '2',
                    ]
                ]
            );


            $this->add_control(
                'pagination',
                [
                    'label' => esc_html__( 'Show Pagination', 'guto-toolkit' ),
                    'description' => esc_html__( 'Pagination Not Working On Home Page!', 'guto-toolkit' ),
                    'type' =>Controls_Manager::SWITCHER,
                    'label_on' => esc_html__( 'Show', 'guto-toolkit' ),
                    'label_off' => esc_html__( 'Hide', 'guto-toolkit' ),
                    'return_value' => 'yes',
                    'default' => 'no',
                ]
            );

        $this->end_controls_section();

        $this->start_controls_section(
			'project_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
        );

            $this->add_control(
                'title_color',
                [
                    'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .single-projects-box .projects-content h3 a' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-projects-box .projects-content h3',
                ]
            );

            $this->add_control(
                'cat_color',
                [
                    'label' => esc_html__( 'Category Text Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} ..single-projects-box .projects-content .category' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'cat_typography',
                    'label' => esc_html__( 'Category Text Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-projects-box .projects-content .category',
                ]
            );

        $this->end_controls_section();

    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        // Card Columns
        $columns = $settings['columns'];
        if ($columns == '1') {
            $column = 'col-lg-12 col-sm-6 col-md-6';
        }elseif ($columns == '2') {
            $column = 'col-lg-6 col-sm-6 col-md-6';
        }elseif ($columns == '3') {
            $column = 'col-lg-4 col-sm-6 col-md-6';
        }elseif ($columns == '4') {
            $column = 'col-lg-3 col-sm-6 col-md-6';
        }

        // Projects Query
        if( $settings['cat'] != '' ) {
            $args = array(
                'post_type'     => 'projects',
                'posts_per_page'=> $settings['count'],
                'order'         => $settings['order'],
                'tax_query'     => array(
                    array(
                        'taxonomy'      => 'projects_cat',
                        'field'         => 'slug',
                        'terms'    => explode( ',', str_replace( ' ', '', $settings['cat'])),
                    )
                ),
                'paged' => get_query_var('paged') ? get_query_var('paged') : 1
            );
        } else {
            $args = array(
                'post_type'         => 'projects',
                'posts_per_page'    => $settings['count'],
                'order'             => $settings['order'],
                'paged' => get_query_var('paged') ? get_query_var('paged') : 1
            );
        }
        $projects_array = new \WP_Query( $args );

        // Get Button Link
        if($settings['link_type'] == 1){
            $link = get_page_link( $settings['link_to_page'] );
        } elseif($settings['link_type'] == 2) {
            $link = $settings['ex_link']['url'];
            $target = $settings['ex_link']['is_external'] ? ' target="_blank"' : '';
		    $nofollow = $settings['ex_link']['nofollow'] ? ' rel="nofollow"' : '';
        }

        $button_text = $settings['button_text'];

        ?>
            <div class="container">
                <div class="row justify-content-center">
                    <?php while($projects_array->have_posts()): $projects_array->the_post();
                        $idd = get_the_ID();
                        $terms = wp_get_post_terms(get_the_ID(), 'projects_cat');

                        $output = array();
                        if ($terms) {
                            foreach ($terms as $term) {
                                $output[] = $term->name ;
                                $id[] = $term->term_id ;
                            }
                        }
                        ?>
                        <div class="<?php echo esc_attr( $column );?>">
                            <div class="single-projects-box">
                                <?php if( get_the_post_thumbnail_url() ): ?>
                                    <img src="<?php the_post_thumbnail_url('guto_lite_post_thumb'); ?>" alt="<?php the_title(); ?>">
                                <?php endif; ?>

                                <div class="projects-content">
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></h3>
                                    <span class="category"><?php echo join( ', ', $output ); ?></span>
                                </div>

                                <div class="plus-icon">
                                    <a href="<?php the_post_thumbnail_url('full'); ?>" class="popup-btn">
                                        <span></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>

                    <?php if( $button_text != '' ): ?>
                        <div class="col-lg-12 col-sm-12 col-md-12">
                            <div class="discover-more-projects-btn text-center">
                                <?php if($settings['link_type'] == 1): ?>
                                    <a href="<?php echo esc_url( $link ); ?>" class="default-btn"><i class="<?php echo esc_attr( $settings['button_icon'] ); ?>"></i> <?php echo esc_html( $button_text ); ?></a>
                                <?php elseif($settings['link_type'] == 2):
                                    echo '<a href="' .  $link . '" class="default-btn" ' . $target . $nofollow . '> <i class="'.$settings['button_icon'].'"></i>' . $button_text . ' </a>';
                                endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php
                    $pagin    = 999999999;
                    $pageLinks  =  paginate_links( array(
                        "base"      => str_replace( $pagin, "%#%", get_pagenum_link( $pagin ) ),
                        "format"    => "?paged=%#%",
                        "current"   => max( 1, get_query_var("paged") ),
                        "total"     => $projects_array->max_num_pages,
                        "prev_text" => "<i class='bx bx-chevrons-left'></i>",
                        "next_text" => "<i class='bx bx-chevrons-right'></i>",
                        "type"      => "plain",
                    ) );

                    if(!is_front_page() && !is_home() && $settings['pagination'] == 'yes'){
                        ?>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="pagination-area text-center">
                                <?php echo wp_kses_post($pageLinks, 'guto-toolkit'); ?>
                            </div>
                        </div>
                        <?php
                    }
                    wp_reset_query();
                    ?>

                </div>
            </div>
        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Projects );