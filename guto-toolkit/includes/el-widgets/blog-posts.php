<?php
/**
 * Posts Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Posts extends Widget_Base {

	public function get_name() {
        return 'Guto_Posts';
    }

	public function get_title() {
        return esc_html__( 'Posts', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-post-slider';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
	}
	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Posts',
			[
				'label' => esc_html__( 'Guto Posts', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

            $this->add_control(
                'posts_style',
                [
                    'label' => esc_html__( 'Posts Style', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        '1'         => esc_html__( 'Style One', 'guto-toolkit' ),
                        '2'         => esc_html__( 'Style Two', 'guto-toolkit' ),
                    ],
                    'default' => '1',
                ]
            );

            $this->add_control(
                'post_subtitle', [
                    'label' => esc_html__( 'Subtitle', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'label_block' => true,
                    'default' => 'LATEST NEWS',
                    'condition' => [
                        'posts_style' => '2',
                    ]
                ]
            );

            $this->add_control(
                'post_title', [
                    'label' => esc_html__( 'Title', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'label_block' => true,
                    'default' => 'Our Latest Insights Are On Top',
                    'condition' => [
                        'posts_style' => '2',
                    ]
                ]
            );

            $this->add_control(
                'cat', [
                    'label' => esc_html__( 'Category', 'guto-toolkit' ),
                    'description' => esc_html__( 'Enter the category slugs separated by commas (Eg. cat1, cat2)', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'label_block' => true,
                ]
            );

            $this->add_control(
                'order',
                [
                    'label' => esc_html__( 'Posts Order By', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        'DESC'      => esc_html__( 'DESC', 'guto-toolkit' ),
                        'ASC'       => esc_html__( 'ASC', 'guto-toolkit' ),
                    ],
                    'default' => 'DESC',
                ]
            );

            $this->add_control(
                'meta_desc',
                [
                    'label' => esc_html__( 'Post Info', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        '1'   => esc_html__( 'Show', 'guto-toolkit' ),
                        '2'   => esc_html__( 'Hide', 'guto-toolkit' ),
                    ],
                    'default' => '1',
                ]
            );

            $this->add_control(
                'columns',
                [
                    'label' => esc_html__( 'Choose Columns', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        '1'   => esc_html__( '1', 'guto-toolkit' ),
                        '2'   => esc_html__( '2', 'guto-toolkit' ),
                        '3'   => esc_html__( '3', 'guto-toolkit' ),
                        '4'   => esc_html__( '4', 'guto-toolkit' ),
                    ],
                    'default' => '3',
                ]
            );

            $this->add_control(
                'count',
                [
                    'label' => esc_html__( 'Item', 'guto-toolkit' ),
                    'type' => Controls_Manager::NUMBER,
                    'default' => 3,
                ]
            );

            $this->add_control(
                'read_more',
                [
                    'label' => esc_html__( 'Read More Button', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'condition' => [
                        'posts_style' => '1',
                    ]
                ]
            );

            $this->add_control(
                'by_text',
                [
                    'label' => esc_html__( 'Post By Text', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                ]
            );

            $this->add_control(
                'read_text',
                [
                    'label' => esc_html__( 'Post Reading Time', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                ]
            );

        $this->end_controls_section();

        $this->start_controls_section(
			'blog_style',
			[
				'label' => esc_html__( 'Style Controls', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
        );

            $this->add_control(
                'title_color',
                [
                    'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .single-blog-post .content h3 a, .single-blog-post-fs .content h3 a' => 'color: {{VALUE}}',
                    ],
                ]
            );
            $this->add_control(
                'title_color_hover',
                [
                    'label' => esc_html__( 'Title Color Hover', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .single-blog-post .content h3 a:hover, .single-blog-post-fs .content h3 a:hover' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-blog-post .content h3, .single-blog-post-fs .content h3',
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'rm_typography',
                    'label' => esc_html__( 'Read More Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-blog-post:hover .content .link-btn',
                    'condition' => [
                        'posts_style' => '1',
                    ]
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'meta_typography',
                    'label' => esc_html__( 'Meta Description Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-blog-post .content .meta li a, .single-blog-post-fs .content .meta li',
                ]
            );

        $this->end_controls_section();

    }

	protected function render() {

        $settings = $this->get_settings_for_display();


         // Card Columns
        $columns = $settings['columns'];
        if ($columns == '1') {
            $column = 'col-lg-12 col-sm-6 col-md-6';
        }elseif ($columns == '2') {
            $column = 'col-lg-6 col-sm-6 col-md-6';
        }elseif ($columns == '3') {
            $column = 'col-lg-4 col-sm-6 col-md-6';
        }elseif ($columns == '4') {
            $column = 'col-lg-3 col-sm-6 col-md-6';
        }

        // Post Query
        if( $settings['cat'] != '' ) {
            $args = array(
                'post_type'     => 'post',
                'posts_per_page'=> $settings['count'],
                'order'         => $settings['order'],
                'tax_query'     => array(
                    array(
                        'taxonomy'      => 'category',
                        'field'         => 'slug',
                        'terms'         => $settings['cat'],
                        'hide_empty'    => false
                    )
                )
            );
        } else {
            $args = array(
                'post_type'         => 'post',
                'posts_per_page'    => $settings['count'],
                'order'             => $settings['order']
            );
        }
        $post_array = new \WP_Query( $args );

		if($settings['posts_style'] == '1'): ?>
            <div class="container">
                <div class="row justify-content-center">
                    <?php while($post_array->have_posts()): $post_array->the_post();
                        $get_author_id          = get_the_author_meta('ID');
                        $get_author_gravatar    = get_avatar_url($get_author_id, array('size' => 50));
                        ?>
                        <div class="<?php echo esc_attr( $column );?>">
                            <div class="single-blog-post">
                                <?php if(get_the_post_thumbnail_url() != ''): ?>
                                    <div class="image">
                                        <img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'guto_lite_post_thumb' ); ?>" alt="<?php the_post_thumbnail_caption();  ?>">

                                        <div class="user d-flex align-items-center">
                                            <img src="<?php echo esc_url($get_author_gravatar); ?>" alt="<?php echo esc_attr( get_the_author() ); ?>">

                                            <div class="info">
                                                <span><?php echo esc_html($settings['by_text']); ?> <?php echo esc_html( get_the_author() ); ?></span>
                                                <?php echo get_the_date(); ?>
                                            </div>
                                        </div>

                                        <a href="<?php the_permalink(); ?>" class="link-btn"></a>
                                    </div>
                                <?php endif; ?>

                                <a href="<?php the_permalink(); ?>">
                                </a>

                                <div class="content">
                                    <?php if($settings['meta_desc'] == '1'): ?>
                                        <ul class="meta">
                                            <li><i class="bx bx-purchase-tag"></i>
                                                <?php
                                                $posttags = get_the_tags();
                                                $count = 0; $sep = '';
                                                if ( $posttags ) {
                                                    foreach( $posttags as $tag ) {
                                                        $count++;
                                                        echo '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a>';
                                                        if( $count > 0 ) break;
                                                    }
                                                }
                                                ?>
                                            </li>

                                            <?php if($settings['read_text']): ?>
                                                <li>
                                                    <i class="bx bx-time"></i>
                                                    <?php echo esc_html(guto_toolkit_reading_time()) ?>
                                                    <?php echo esc_html($settings['read_text']); ?>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    <?php endif; ?>
                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                                    <?php if( $settings['read_more'] != '' ): ?>
                                        <a href="<?php the_permalink(); ?>" class="link-btn">
                                            <?php echo esc_html( $settings['read_more'] ); ?>
                                            <i class="bx bx-right-arrow-alt"></i>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>

        <?php elseif( $settings['posts_style'] == '2' ) : ?>
            <section class="blog-area pt-100 pb-70">
                <div class="container">
                    <div class="section-title">
                        <span class="sub-title"><?php echo esc_html__( $settings['post_subtitle']); ?></span>
                        <h2><?php echo esc_html__( $settings['post_title']); ?></h2>
                    </div>
                    <div class="row justify-content-center">
                        <?php while($post_array->have_posts()): $post_array->the_post();
                            $get_author_id          = get_the_author_meta('ID');
                            $get_author_gravatar    = get_avatar_url($get_author_id, array('size' => 50));
                            ?>
                            <div class="<?php echo esc_attr( $column );?>">
                                <div class="single-blog-post-fs">
                                    <?php if(get_the_post_thumbnail_url() != ''): ?>
                                        <div class="image">
                                            <a href="<?php the_permalink(); ?>" class="d-block">
                                                <img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'guto_lite_post_thumb' ); ?>" alt="<?php the_post_thumbnail_caption();  ?>">
                                            </a>
                                        </div>
                                    <?php endif; ?>

                                    <div class="content">
                                        <?php if($settings['meta_desc'] == '1'): ?>
                                            <ul class="meta">
                                                <?php
                                                    $posttags = get_the_tags();
                                                    $count = 0; $sep = '';
                                                    if ( $posttags ) { ?>
                                                    <li><i class="bx bx-purchase-tag"></i>
                                                        <?php
                                                            foreach( $posttags as $tag ) {
                                                                $count++;
                                                                echo '<a href="'.get_tag_link($tag->term_id).'">'.$tag->name.'</a>';
                                                                if( $count > 0 ) break;
                                                            } 
                                                        ?>
                                                    </li>

                                                <?php }

                                                if($settings['read_text']): ?>
                                                    <li>
                                                        <i class="bx bx-time"></i>
                                                        <?php echo esc_html(guto_toolkit_reading_time()) ?>
                                                        <?php echo esc_html($settings['read_text']); ?>
                                                    </li>
                                                <?php endif; ?>
                                            </ul>
                                        <?php endif; ?>
                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                                        <div class="user d-flex align-items-center">
                                            <img src="<?php echo esc_url($get_author_gravatar); ?>" alt="<?php echo esc_attr( get_the_author() ); ?>">

                                            <div class="info">
                                                <span><?php echo esc_html($settings['by_text']); ?> <?php echo esc_html( get_the_author() ); ?></span>
                                                <?php echo get_the_date(); ?>
                                            </div>
                                        </div>
                                        <a href="<?php the_permalink(); ?>" class="link-btn"><i class='bx bx-right-arrow-alt'></i></a>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
            </section>
        <?php endif;
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Posts );