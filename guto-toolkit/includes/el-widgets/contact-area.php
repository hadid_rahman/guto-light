<?php
/**
 * Contact Area Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Contact_Area extends Widget_Base {

	public function get_name() {
        return 'Contact_Area';
    }

	public function get_title() {
        return esc_html__( 'Contact Area', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-form-horizontal';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
            'Guto_Contact_Area',
            [
                'label' => esc_html__( 'Guto Contact Area', 'guto-toolkit' ),
                'tab' => Controls_Manager::TAB_CONTENT,
            ]
        );

            $this->add_control(
                'contact_style',
                [
                    'label' => esc_html__( 'Choose Style', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        '1'   => esc_html__( 'Style One', 'guto-toolkit' ),
                        '2'   => esc_html__( 'Style Two', 'guto-toolkit' ),
                    ],
                    'default' => '1',
                ]
            );

            $this->add_control(
                'shortcode',
                [
                    'label' => esc_html__( 'Contact Shortcode', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                ]
            );

            $this->add_control(
                'list_items',
                [
                    'label' => esc_html__('List Item', 'guto-toolkit'),
                    'type' => Controls_Manager::REPEATER,
                    'separator' => 'before',
                    'fields' => [
                        [
                            'name'	    => 'title',
                            'label'     => esc_html__( 'Title', 'guto-toolkit' ),
                            'type'      => Controls_Manager::TEXT,
                            'default'   => esc_html__('Our Address', 'guto-toolkit'),
                        ],
                        [
                            'name'  => 'icon',
                            'label' => esc_html__( 'Select Icon', 'guto-toolkit' ),
                            'type' => Controls_Manager::ICON,
                            'label_block' => true,
                            'options' => guto_toolkit_icons(),
                        ],
                        [
                            'name'	    => 'content',
                            'label'     => esc_html__( 'Content', 'guto-toolkit' ),
                            'type'      => Controls_Manager::TEXTAREA,
                            'default'   => esc_html__('175 5th Ave, New York, NY 10010, United States', 'guto-toolkit'),
                        ],
                    ],
                ]
            );

        $this->end_controls_section();
    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        // Inline Editing
        $this-> add_inline_editing_attributes('title','none');

        ?>
        <?php if($settings['contact_style'] == '1'): ?>
            <div class="contact-area ptb-100">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-7 col-md-12">
                            <?php if($settings['shortcode']): ?>
                                <div class="contact-form">
                                    <?php echo do_shortcode( $settings['shortcode'] ); ?>
                                </div>
                            <?php endif; ?>
                        </div>

                        <div class="col-lg-5 col-md-12">
                            <?php foreach( $settings['list_items'] as $item ):
                                // Icon
                                $icon = $item['icon'];
                                ?>
                                <div class="contact-info-box">
                                    <div class="back-icon">
                                        <i class="<?php echo esc_attr( $icon ); ?> bx-fade-left"></i>
                                    </div>
                                    <div class="icon">
                                        <i class="<?php echo esc_attr( $icon ); ?>"></i>
                                    </div>
                                    <h3><?php echo esc_html( $item['title'] ); ?></h3>
                                    <?php echo wp_kses_post( $item['content'] ); ?>
                                </div>
                            <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if($settings['contact_style'] == '2'): ?>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-12">
                        <?php foreach( $settings['list_items'] as $item ):
                            // Icon
                            $icon = $item['icon'];
                            ?>
                            <div class="contact-info-box">
                                <div class="back-icon">
                                    <i class="<?php echo esc_attr( $icon ); ?> bx-fade-left"></i>
                                </div>
                                <div class="icon">
                                    <i class="<?php echo esc_attr( $icon ); ?>"></i>
                                </div>
                                <h3><?php echo esc_html( $item['title'] ); ?></h3>
                                <?php echo wp_kses_post( $item['content'] ); ?>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="col-lg-7 col-md-12">
                        <?php if($settings['shortcode']): ?>
                            <div class="contact-form">
                                <?php echo do_shortcode( $settings['shortcode'] ); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Contact_Area );