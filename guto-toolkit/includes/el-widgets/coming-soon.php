<?php
/**
 * Coming Soon Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Coming_Soon extends Widget_Base {

    public function __construct($data = [], $args = null) {
        parent::__construct($data, $args);

        wp_register_script( 'countdowntimer', GUTO_TOOLKIT_SCRIPTS . '/countdowntimer.min.js', [ 'elementor-frontend' ], '1.2.0', true );
    }

    public function get_script_depends() {
        return [ 'countdowntimer' ];
    }

	public function get_name() {
        return 'Guto_Coming_Soon';
    }

	public function get_title() {
        return esc_html__( 'Coming Soon', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-number-field';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Coming_Soon_Area',
			[
				'label' => esc_html__( 'Coming Soon Controls', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

            $this->add_control(
                'bg_image',
                [
                    'label' => esc_html__( 'Background Image', 'guto-toolkit' ),
                    'type'  => Controls_Manager::MEDIA,
                ]
            );

			$this->add_control(
				'title',
				[
					'label' => esc_html__( 'Title', 'guto-toolkit' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__('COMING SOON', 'guto-toolkit'),
				]
			);

            $this->add_control(
                'action_url', [
                    'label' => esc_html__( 'Action URL', 'guto-toolkit' ),
                    'description' => esc_html__( 'Enter here your MailChimp action URL. <a href="https://doc.gutotheme.com/docs/tips-guides-troubleshoots/get-mailchimp-newsletter-form-action-url/" target="_blank"> How to </a>', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'label_block' => true,
                ]
            );

            $this->add_control(
                'placeholder_text', [
                    'label' => esc_html__( 'Placeholder Text', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__( 'Enter your email' , 'guto-toolkit' ),
                    'label_block' => true,
                ]
            );

            $this->add_control(
                'button_text', [
                    'label' => esc_html__( 'Button text', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__( 'Subscribe' , 'guto-toolkit' ),
                    'label_block' => true,
                ]
            );
        $this->end_controls_section();

        $this->start_controls_section(
			'Guto_Time',
			[
				'label' => esc_html__( 'Guto Date', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );
        $this->add_control(
            'guto_due_date',
            [
                'label' => esc_html__( 'Due Date', 'guto-toolkit' ),
                'type' => Controls_Manager::DATE_TIME,
                'default' => date( 'Y-m-d H:i', strtotime( '+1 month' ) + ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS ) ),
                'description' => sprintf( __( 'Date set according to your timezone: %s.', 'guto-toolkit' ), Utils::get_timezone_string() ),

            ]
        );
            $this->add_control(
                'guto_show_days',
                [
                    'label' => esc_html__( 'Days', 'guto-toolkit' ),
                    'type' => Controls_Manager::SWITCHER,
                    'label_on' => esc_html__( 'Show', 'guto-toolkit' ),
                    'label_off' => esc_html__( 'Hide', 'guto-toolkit' ),
                    'return_value' => 'yes',
                    'default' => 'yes',
                ]
            );
            $this->add_control(
                'guto_show_hours',
                [
                    'label' => esc_html__( 'Hours', 'guto-toolkit' ),
                    'type' => Controls_Manager::SWITCHER,
                    'label_on' => esc_html__( 'Show', 'guto-toolkit' ),
                    'label_off' => esc_html__( 'Hide', 'guto-toolkit' ),
                    'return_value' => 'yes',
                    'default' => 'yes',
                ]
            );
            $this->add_control(
                'guto_show_minutes',
                [
                    'label' => esc_html__( 'Minutes', 'guto-toolkit' ),
                    'type' => Controls_Manager::SWITCHER,
                    'label_on' => esc_html__( 'Show', 'guto-toolkit' ),
                    'label_off' => esc_html__( 'Hide', 'guto-toolkit' ),
                    'return_value' => 'yes',
                    'default' => 'yes',
                ]
            );
            $this->add_control(
                'guto_show_seconds',
                [
                    'label' => esc_html__( 'Seconds', 'guto-toolkit' ),
                    'type' => Controls_Manager::SWITCHER,
                    'label_on' => esc_html__( 'Show', 'guto-toolkit' ),
                    'label_off' => esc_html__( 'Hide', 'guto-toolkit' ),
                    'return_value' => 'yes',
                    'default' => 'yes',
                ]
            );

        $this->end_controls_section();

        $this->start_controls_section(
			'guto_expire_section',
			[
				'label' => esc_html__( 'Countdown Expire' , 'guto-toolkit' )
			]
		);
            $this->add_control(
                'guto_expire_show_type',
                [
                    'label'			=> esc_html__('Expire Type', 'guto-toolkit'),
                    'label_block'	=> false,
                    'type'			=> Controls_Manager::SELECT,
                    'description'   => esc_html__('Select whether you want to set a message or a redirect link after expire countdown', 'guto-toolkit'),
                    'options'		=> [
                        'message'		=> esc_html__('Message', 'guto-toolkit'),
                        'redirect_link'		=> esc_html__('Redirect to Link', 'guto-toolkit')
                    ],
                    'default' => 'message'
                ]
            );
            $this->add_control(
                'guto_expire_message',
                [
                    'label'			=> esc_html__('Expire Message', 'guto-toolkit'),
                    'type'			=> Controls_Manager::TEXTAREA,
                    'default'		=> esc_html__('Sorry you are late!','guto-toolkit'),
                    'condition'		=> [
                        'guto_expire_show_type' => 'message'
                    ]
                ]
            );
            $this->add_control(
                'guto_expire_redirect_link',
                [
                    'label'			=> esc_html__('Redirect On', 'guto-toolkit'),
                    'type'			=> Controls_Manager::URL,
                    'condition'		=> [
                        'guto_expire_show_type' => 'redirect_link'
                    ],
                ]
            );

        $this->end_controls_section();

        $this->start_controls_section(
			'guto_label_text_section',
			[
				'label' => esc_html__( 'Change Labels Text' , 'guto-toolkit' )
			]
		);
            $this->add_control(
                'guto_change_labels',
                [
                    'label' => esc_html__( 'Change Labels', 'guto-toolkit' ),
                    'type' => Controls_Manager::SWITCHER,
                    'label_on' => esc_html__( 'Yes', 'guto-toolkit' ),
                    'label_off' => esc_html__( 'No', 'guto-toolkit' ),
                    'return_value' => 'yes',
                    'default' => 'yes',
                ]
            );
            $this->add_control(
                'guto_label_days',
                [
                    'label' => esc_html__( 'Days', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__( 'Days', 'guto-toolkit' ),
                    'placeholder' => esc_html__( 'Days', 'guto-toolkit' ),
                    'condition' => [
                        'guto_change_labels' => 'yes',
                        'guto_show_days' => 'yes',
                    ],
                ]
            );
            $this->add_control(
                'guto_label_hours',
                [
                    'label' => esc_html__( 'Hours', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__( 'Hours', 'guto-toolkit' ),
                    'placeholder' => esc_html__( 'Hours', 'guto-toolkit' ),
                    'condition' => [
                        'guto_change_labels' => 'yes',
                        'guto_show_hours' => 'yes',
                    ],
                ]
            );
            $this->add_control(
                'guto_label_minutes',
                [
                    'label' => esc_html__( 'Minutes', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__( 'Minutes', 'guto-toolkit' ),
                    'placeholder' => esc_html__( 'Minutes', 'guto-toolkit' ),
                    'condition' => [
                        'guto_change_labels' => 'yes',
                        'guto_show_minutes' => 'yes',
                    ],
                ]
            );
            $this->add_control(
                'guto_label_seconds',
                [
                    'label' => esc_html__( 'Seconds', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__( 'Seconds', 'guto-toolkit' ),
                    'placeholder' => esc_html__( 'Seconds', 'guto-toolkit' ),
                    'condition' => [
                        'guto_change_labels' => 'yes',
                        'guto_show_seconds' => 'yes',
                    ],
                ]
            );
		$this->end_controls_section();

    }

	protected function render() {

		$settings = $this->get_settings_for_display();

        // Inline Editing
        $this-> add_inline_editing_attributes('title','none');

        global $guto_opt;

        // Main site logo
        if ( defined( 'FW' ) ) {
            // Page settings
            $custom_logo	 = fw_get_db_post_option( get_the_ID(), 'custom_logo' );
        }

        if( function_exists('guto_lite_option')):
            $default_log = guto_lite_option('site_logo');
        else:
            $default_log = '';
        endif;

        if( function_exists('guto_lite_option')):
            if(isset($custom_logo['url']) && $custom_logo['url'] !=''){
                $logo = $custom_logo['url'];
            }else{
                $logo = guto_lite_option('site_logo');
            }
        else:
            $logo = '';
        endif;

        $day                        =   $settings['guto_show_days'];
		$hours                      =   $settings['guto_show_hours'];
		$minute                     =   $settings['guto_show_minutes'];
		$seconds                    =   $settings['guto_show_seconds'];

		?>
        <div class="coming-soon-area" style="background-image:url(<?php echo esc_url( $settings['bg_image']['url'] ); ?>);">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="coming-soon-content">

                            <a href="<?php echo esc_url(home_url('/'));?>" class="logo">
                                <?php if(!empty($logo)): ?>
                                    <img src="<?php echo esc_url($logo);  ?>" alt="<?php echo get_bloginfo(); ?>">
                                <?php else: ?>
                                    <img src="<?php echo esc_url(GUTO_TOOLKIT_IMAGES.'/logo.png');  ?>" alt="<?php echo get_bloginfo(); ?>">
                                <?php endif ?>
                            </a>

						    <h2><?php echo esc_html( $settings['title'] ); ?></h2>

                            <div id="timer" class="flex-wrap d-flex justify-content-center">
                                <div id="countdown-timer-<?php echo esc_attr($this->get_id()); ?>" class="countdown-timer-init flex-wrap d-flex justify-content-center"></div>
                                <div id="finished-message-<?php echo esc_attr($this->get_id()); ?>" class="finished-message"></div>
                            </div>

                            <form class="mailchimp newsletter-form" method="post">

                                <div class="form-group">
                                    <input type="email" class="input-newsletter memail" placeholder="<?php echo esc_attr( $settings['placeholder_text'] ); ?>" name="EMAIL" required>
                                    <span class="label-title"><i class="bx bx-envelope"></i></span>
                                </div>

                                <?php if( $settings['button_text'] != '' ): ?>
                                    <button type="submit" class="default-btn">
                                        <i class="bx bx-paper-plane"></i>
                                        <?php echo esc_html( $settings['button_text'] ); ?>
                                    </button>
                                <?php endif; ?>
                                <p class="mchimp-errmessage" style="display: none;"></p>
                                <p class="mchimp-sucmessage" style="display: none;"></p>
                            </form>

                            <script>
                                ;(function($){
                                    "use strict";
                                    $(document).ready(function () {
                                        // MAILCHIMP
                                        if ($(".mailchimp").length > 0) {
                                            $(".mailchimp").ajaxChimp({
                                                callback: mailchimpCallback,
                                                url: "<?php echo esc_js($settings['action_url']) ?>"
                                            });
                                        }
                                        if ($(".mailchimp_two").length > 0) {
                                            $(".mailchimp_two").ajaxChimp({
                                                callback: mailchimpCallback,
                                                url: "<?php echo esc_js($settings['action_url']) ?>" //Replace this with your own mailchimp post URL. Don't remove the "". Just paste the url inside "".
                                            });
                                        }
                                        $(".memail").on("focus", function () {
                                            $(".mchimp-errmessage").fadeOut();
                                            $(".mchimp-sucmessage").fadeOut();
                                        });
                                        $(".memail").on("keydown", function () {
                                            $(".mchimp-errmessage").fadeOut();
                                            $(".mchimp-sucmessage").fadeOut();
                                        });
                                        $(".memail").on("click", function () {
                                            $(".memail").val("");
                                        });

                                        function mailchimpCallback(resp) {
                                            if (resp.result === "success") {
                                                $(".mchimp-errmessage").html(resp.msg).fadeIn(1000);
                                                $(".mchimp-sucmessage").fadeOut(500);
                                            } else if (resp.result === "error") {
                                                $(".mchimp-errmessage").html(resp.msg).fadeIn(1000);
                                            }
                                        }
                                    });
                                })(jQuery)
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
			jQuery(function(){
				jQuery('#countdown-timer-<?php echo esc_attr($this->get_id()); ?>').countdowntimer({
                    dateAndTime : "<?php echo preg_replace('/-/', '/', $settings['guto_due_date']); ?>",
                    regexpMatchFormat: "([0-9]{1,3}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",
      				regexpReplaceWith: "<?php if ($day == "yes"){?><div class='countdown-items align-items-center flex-column d-flex justify-content-center'><span class='guto-digits'>$1</span><span class='guto-label'><?php echo $settings['guto_label_days']; ?></span> </div><?php } ?><?php if ($hours == "yes"){?> <div class='countdown-items align-items-center flex-column d-flex justify-content-center'><span class='guto-digits'>$2 </span><span class='guto-label'><?php echo $settings['guto_label_hours']; ?></span></div><?php } ?><?php if ($minute == "yes"){?><div class='countdown-items align-items-center flex-column d-flex justify-content-center'> <span class='guto-digits'> $3 </span><span class='guto-label'><?php echo $settings['guto_label_minutes']; ?></span> </div><?php } ?><?php if ($seconds == "yes"){?><div class='countdown-items align-items-center flex-column d-flex justify-content-center'><span class='guto-digits'> $4</span><span class='guto-label'><?php echo $settings['guto_label_seconds']; ?></span></div><?php } ?>",
					<?php if( $settings['guto_expire_show_type'] == "redirect_link"){ ?>
					expiryUrl : link_url,
					<?php } else { ?>
					timeUp : timeisUp,
					<?php } ?>
                });
                <?php if( $settings['guto_expire_show_type'] == "redirect_link"){ ?>
					var ele_backend = jQuery('body').find('#elementor-frontend-css').length;

					if( ele_backend > 0 ) {
						jQuery(this).find('.countdown-timer-init').html( '<h1>You can not redirect url from elementor Editor!!</h1>' );

					} else {
						var link_url = '<?php echo $settings['guto_expire_redirect_link']['url'] ?>';
						window.open("<?php echo $settings['guto_expire_redirect_link']['url'] ?>", "<?php echo $target ?>");

					}
				<?php } else { ?>
					function timeisUp(){
						jQuery("#finished-message-<?php echo esc_attr($this->get_id()); ?>").html( "<span><?php echo $settings['guto_expire_message'];?></span>" );
				    }
				<?php } ?>
			});

        </script>

        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Coming_Soon );