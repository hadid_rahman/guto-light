<?php
/**
 * Partner Logo Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Partner_Logo extends Widget_Base {

	public function get_name() {
        return 'Partner_Logo';
    }

	public function get_title() {
        return esc_html__( 'Partner Logo', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-logo';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'partner_section',
			[
				'label' => esc_html__( 'Partner Logo Control', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

            $this->add_control(
                'style',
                [
                    'label' => esc_html__( 'Partner Style', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        '1'         => esc_html__( 'Style One', 'guto-toolkit' ),
                        '2'         => esc_html__( 'Style Two', 'guto-toolkit' ),
                        '3'         => esc_html__( 'Style Three', 'guto-toolkit' ),
                    ],
                    'default' => '1',
                ]
            );

            $this->add_control(
                'title',
                [
                    'label' => esc_html__( 'Title', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__('Clients We Work For', 'guto-toolkit'),
                    'condition' => [
                        'style' => ['1', '2'],
                    ]
                ]
            );

            $this->add_control(
                'title_tag',
                [
                    'label' => esc_html__( 'Title Tag', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        'h1'         => esc_html__( 'h1', 'guto-toolkit' ),
                        'h2'         => esc_html__( 'h2', 'guto-toolkit' ),
                        'h3'         => esc_html__( 'h3', 'guto-toolkit' ),
                        'h4'         => esc_html__( 'h4', 'guto-toolkit' ),
                        'h5'         => esc_html__( 'h5', 'guto-toolkit' ),
                        'h6'         => esc_html__( 'h6', 'guto-toolkit' ),
                    ],
                    'default' => 'h2',
                    'condition' => [
                        'style' => ['1', '2'],
                    ]
                ]
            );

            $this->add_control(
                'logos',
                [
                    'type'    => Controls_Manager::GALLERY,
                    'label'   => esc_html__( 'Add Partner Logos', 'guto-toolkit' ),		
                ]			
            );

        $this->end_controls_section();

        $this->start_controls_section(
            'partner_style',
            [
                'label' => esc_html__( 'Style', 'guto-toolkit' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

            $this->add_control(
                'partner_bg_color',
                [
                    'label' => esc_html__( 'Section Background Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .partner-inner-area, .partner-area-two, .partner-area-fs' => 'background-color: {{VALUE}}',
                    ],
                ]
            );

        $this->end_controls_section();

    }

	protected function render() {
        $settings = $this->get_settings_for_display();
        ?>
        <?php if($settings['style'] == '1'): ?>
            <div class="partner-area">
                <div class="container">
                    <div class="partner-inner-area ptb-70">
                        <div class="row">
                            <div class="col-lg-5 col-md-12">
                                <div class="section-title">
                                    <<?php echo esc_attr( $settings['title_tag'] ); ?> <?php echo $this-> get_render_attribute_string('title'); ?>><?php echo wp_kses_post( $settings['title'] ); ?></<?php echo esc_attr( $settings['title_tag'] ); ?>>
                                </div>
                            </div>

                            <div class="col-lg-7 col-md-12">
                                <div class="partner-list">
                                    <div class="row align-items-center">
                                    <?php foreach ( $settings['logos'] as $image ) { ?>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                                            <?php if($image['url'] != ''): ?>
                                                <div class="partner-item">
                                                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($settings['title']); ?>">
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if($settings['style'] == '2'): ?>
            <div class="partner-area-two ptb-100">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-12">
                            <div class="section-title">
                                <<?php echo esc_attr( $settings['title_tag'] ); ?> <?php echo $this-> get_render_attribute_string('title'); ?>><?php echo wp_kses_post( $settings['title'] ); ?></<?php echo esc_attr( $settings['title_tag'] ); ?>>
                            </div>
                        </div>

                        <div class="col-lg-7 col-md-12">
                            <div class="row align-items-center">
                            <?php foreach ( $settings['logos'] as $image ) { ?>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                                    <?php if($image['url'] != ''): ?>
                                        <div class="partner-item-fs">
                                            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($settings['title']); ?>">
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if($settings['style'] == '3'): ?>
            <div class="partner-area-fs pt-100 pb-70 bg-fafafa">
                <div class="container">
                    <div class="row align-items-center">
                    <?php foreach ( $settings['logos'] as $image ) { ?>
                        <div class="col-lg-2 col-md-4 col-sm-4 col-6">
                            <?php if($image['url'] != ''): ?>
                                <div class="partner-item">
                                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($settings['title']); ?>">
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php
	}
	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Partner_Logo );