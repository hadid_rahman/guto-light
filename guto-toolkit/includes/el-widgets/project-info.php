<?php
/**
 * Project Info Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_ProjectInfo extends Widget_Base {

	public function get_name() {
        return 'ProjectInfo';
    }

	public function get_title() {
        return esc_html__( 'Guto Project Info', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-single-post';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_ProjectInfo',
			[
				'label' => esc_html__( 'Project Info Control', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

            $this->add_control(
                'items',
                [
                    'label' => esc_html__('Project Info Items', 'guto-toolkit'),
                    'type' => Controls_Manager::REPEATER,
                    'separator' => 'before',
                    'fields' => [
                        [
                            'name'	=> 'title',
                            'label' => esc_html__( 'Title', 'guto-toolkit'),
                            'label_block' => true,
                            'type' => Controls_Manager::TEXT,
                            'default' => esc_html__( 'Client', 'guto-toolkit'),
                        ],
                        [
                            'name'	=> 'content',
                            'label' => esc_html__( 'Content', 'guto-toolkit'),
                            'label_block' => true,
                            'type' => Controls_Manager::TEXT,
                            'default' => esc_html__( 'James Anderson', 'guto-toolkit'),
                        ],
                    ],
                ]
            );

            $this->add_control(
                'social_title',
                [
                    'label' => esc_html__( 'Social Title', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__('Social', 'guto-toolkit'),
                ]
            );

            $this->add_control(
                'links',
                [
                    'label' => esc_html__('Project Info Social Links', 'guto-toolkit'),
                    'type' => Controls_Manager::REPEATER,
                    'separator' => 'before',
                    'fields' => [
                        [
                            'name'	=> 'icon',
                            'label' => esc_html__( 'Icon', 'guto-toolkit'),
                            'type' => Controls_Manager::ICON,
                            'label_block' => true,
                            'options' => guto_toolkit_icons(),
                        ],
                        [
                            'name'	=> 'link',
                            'label' => esc_html__( 'Link', 'guto-toolkit'),
                            'label_block' => true,
                            'type' => Controls_Manager::TEXT,
                            'default' => esc_html__( '#', 'guto-toolkit'),
                        ],
                    ],
                ]
            );

            $this->add_control(
				'button_text',
				[
					'label' 	=> esc_html__( 'Button Text', 'guto-toolkit' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> esc_html__('Live Preview', 'guto-toolkit'),
				]
            );

            $this->add_control(
				'button_icon',
				[
					'label' => esc_html__( 'Button Icon', 'guto-toolkit' ),
                    'type' => Controls_Manager::ICON,
                    'label_block' => true,
                    'options' => guto_toolkit_icons(),
				]
            );

            $this->add_control(
                'link_type',
                [
                    'label' 		=> esc_html__( 'Button Link Type', 'guto-toolkit' ),
                    'type' 			=> Controls_Manager::SELECT,
                    'label_block' 	=> true,
                    'options' => [
                        '1'  	=> esc_html__( 'Link To Page', 'guto-toolkit' ),
                        '2' 	=> esc_html__( 'External Link', 'guto-toolkit' ),
                    ],
                ]
            );

            $this->add_control(
                'link_to_page',
                [
                    'label' 		=> esc_html__( 'Button Link Page', 'guto-toolkit' ),
                    'type' 			=> Controls_Manager::SELECT,
                    'label_block' 	=> true,
                    'options' 		=> guto_toolkit_get_page_as_list(),
                    'condition' => [
                        'link_type' => '1',
                    ]
                ]
            );

            $this->add_control(
                'ex_link',
                [
                    'label'		=> esc_html__('Button External Link', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: URL,
                    'show_external' => true,
                    'default' => [
                        'url' => '#',
                        'is_external' => true,
                        'nofollow' => true,
                    ],
                    'condition' => [
                        'link_type' => '2',
                    ]
                ]
            );

        $this->end_controls_section();

        $this->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
        );
            $this->add_control(
                'title_color',
                [
                    'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .projects-details-desc .project-details-info .single-info-box h4' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .projects-details-desc .project-details-info .single-info-box h4, .why-choose-content .sub-title',
                ]
            );

            $this->add_control(
                'content_color',
                [
                    'label' => esc_html__( 'Content Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .projects-details-desc .project-details-info .single-info-box span' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'content_typography',
                    'label' => esc_html__( 'Content Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .projects-details-desc .project-details-info .single-info-box span',
                ]
            );

        $this->end_controls_section();

    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        // Get Button Link
        if($settings['link_type'] == 1){
            $link = get_page_link( $settings['link_to_page'] );
        } elseif($settings['link_type'] == 2) {
            $link = $settings['ex_link']['url'];
            $target = $settings['ex_link']['is_external'] ? ' target="_blank"' : '';
            $nofollow = $settings['ex_link']['nofollow'] ? ' rel="nofollow"' : '';
        }

        $button_text = $settings['button_text'];

        ?>
            <div class="project-details-info">
                <?php foreach( $settings['items'] as $item ): ?>
                    <div class="single-info-box">
                        <h4><?php echo esc_html($item['title']); ?></h4>
                        <span><?php echo esc_html($item['content']); ?></span>
                    </div>
                <?php endforeach; ?>

                <div class="single-info-box">
                    <h4><?php echo esc_html($settings['social_title']); ?></h4>
                    <ul class="social">
                        <?php foreach( $settings['links'] as $item ): ?>
                            <li><a href="<?php echo esc_url($item['link']); ?>" target="_blank"><i class="<?php echo esc_attr($item['icon']); ?>"></i></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <div class="single-info-box">
                    <?php if( $button_text != '' ): ?>
                        <?php if($settings['link_type'] == 1): ?>
                            <a href="<?php echo esc_url( $link ); ?>" class="default-btn"><i class="<?php echo esc_attr( $settings['button_icon'] ); ?>"></i> <?php echo esc_html( $button_text ); ?></a>
                        <?php elseif($settings['link_type'] == 2):
                            echo '<a href="' .  $link . '" class="default-btn" ' . $target . $nofollow . '> <i class="'.$settings['button_icon'].'"></i>' . $button_text . ' </a>';
                        endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_ProjectInfo );