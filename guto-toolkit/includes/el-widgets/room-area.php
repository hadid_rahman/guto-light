<?php
/**
 * Room Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Room extends Widget_Base {

	public function get_name() {
        return 'Guto_Room';
    }

	public function get_title() {
        return esc_html__( 'Room', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-lightbox-expand';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Room_Area',
			[
				'label' => esc_html__( 'Room Controls', 'guto-toolkit' ),
				'tab' 	=> Controls_Manager::TAB_CONTENT,
			]
		);

			$this->add_control(
				'title',
				[
					'label' 	=> esc_html__( 'Title', 'guto-toolkit' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> esc_html__('Living Room', 'guto-toolkit'),
				]
			);

			$this->add_control(
                'title_tag',
                [
                    'label' 	=> esc_html__( 'Title Tag', 'guto-toolkit' ),
                    'type' 		=> Controls_Manager::SELECT,
                    'options' 	=> [
                        'h1'         => esc_html__( 'h1', 'guto-toolkit' ),
                        'h2'         => esc_html__( 'h2', 'guto-toolkit' ),
                        'h3'         => esc_html__( 'h3', 'guto-toolkit' ),
                        'h4'         => esc_html__( 'h4', 'guto-toolkit' ),
                        'h5'         => esc_html__( 'h5', 'guto-toolkit' ),
                        'h6'         => esc_html__( 'h6', 'guto-toolkit' ),
                    ],
                    'default' => 'h1',
                ]
            );

			$this->add_control(
				'content',
				[
					'label' 	=> esc_html__( 'Content', 'guto-toolkit' ),
					'type' 		=> Controls_Manager::WYSIWYG,
					'default' 	=> __('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
                    <ul>
                        <li>Opt for light colours</li>
                        <li>Think carefully about flooring</li>
                        <li>Add a mirror</li>
                        <li>Wall features</li>
                        <li>Keep the space cosy</li>
                    </ul>', 'guto-toolkit'),
				]
            );

            $this->add_control(
				'button_icon',
				[
					'label' => esc_html__( 'Button Icon', 'guto-toolkit' ),
                    'type' => Controls_Manager::ICON,
                    'label_block' => true,
                    'options' => guto_toolkit_icons(),
                    'default' => 'bx bx-shopping-bag'
				]
            );

			$this->add_control(
				'button_text',
				[
					'label' 	=> esc_html__( 'Button Text', 'guto-toolkit' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> esc_html__('Shop Living Room', 'guto-toolkit'),
				]
            );

            $this->add_control(
                'link_type',
                [
                    'label' 		=> esc_html__( 'Button Link Type', 'guto-toolkit' ),
                    'type' 			=> Controls_Manager::SELECT,
                    'label_block' 	=> true,
                    'options' => [
                        '1'  	=> esc_html__( 'Link To Page', 'guto-toolkit' ),
                        '2' 	=> esc_html__( 'External Link', 'guto-toolkit' ),
                    ],
                    'default' => '2'
                ]
            );

            $this->add_control(
                'link_to_page',
                [
                    'label' 		=> esc_html__( 'Button Link Page', 'guto-toolkit' ),
                    'type' 			=> Controls_Manager::SELECT,
                    'label_block' 	=> true,
                    'options' 		=> guto_toolkit_get_page_as_list(),
                    'condition' => [
                        'link_type' => '1',
                    ]
                ]
            );

            $this->add_control(
                'ex_link',
                [
                    'label'		=> esc_html__('Button External Link', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: URL,
                    'show_external' => true,
                    'default' => [
                        'url' => '#',
                        'is_external' => true,
                        'nofollow' => true,
                    ],
                    'condition' => [
                        'link_type' => '2',
                    ]
                ]
            );

            $this->add_control(
                'room_bg_img',
                [
                    'label' => esc_html__( 'Background Image', 'guto-toolkit' ),
                    'type' => Controls_Manager::MEDIA,
                    'default' => [
                        'url' => plugin_dir_url( dirname( __FILE__ ) ) . '../public/img/rooms-bg1.jpg'
                    ],
                    'show_label' => true,
                ]
            );

        $this->end_controls_section();

        $this->start_controls_section(
			'room_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

            $this->add_control(
                'bg_color',
                [
                    'label' => esc_html__( 'Section Background Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .rooms-area' => 'background-color: {{VALUE}}',
                    ],
                ]
            );

			$this->add_control(
				'title_color',
				[
					'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .rooms-content h1, .rooms-content h2, .rooms-content h3, .rooms-content h4, .rooms-content h5, .rooms-content h6' => 'color: {{VALUE}}',
					],
				]
			);

			$this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .rooms-content h1, .rooms-content h2, .rooms-content h3, .rooms-content h4, .rooms-content h5, .rooms-content h6',
                ]
            );

			$this->add_control(
				'content_color',
				[
					'label' => esc_html__( 'Content Color', 'guto-toolkit' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .rooms-area .rooms-content p' => 'color: {{VALUE}}',
					],
				]
			);

			$this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'content_typography',
                    'label' => esc_html__( 'Content Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .rooms-area .rooms-content p',
                ]
            );

        $this->end_controls_section();

    }

	protected function render() {

		$settings = $this->get_settings_for_display();

        // Inline Editing
        $this-> add_inline_editing_attributes('title','none');
        //$this-> add_inline_editing_attributes('content','none');

        // Get Button Link
        if($settings['link_type'] == 1){
            $link = get_page_link( $settings['link_to_page'] );
        } elseif($settings['link_type'] == 2) {
            $link = $settings['ex_link']['url'];
            $target = $settings['ex_link']['is_external'] ? ' target="_blank"' : '';
		    $nofollow = $settings['ex_link']['nofollow'] ? ' rel="nofollow"' : '';
        }

        $button_text = $settings['button_text'];
		?>  
            <!-- Start Living Room Area -->
            <div class="rooms-area rooms-bg1 pt-100 jarallax" data-jarallax='{"speed": 0.3}' style="background-image:url(<?php echo esc_url($settings['room_bg_img']['url']); ?>);">
                <div class="container">
                    <div class="rooms-content">
                        <<?php echo esc_attr( $settings['title_tag'] ); ?> <?php echo $this-> get_render_attribute_string('title'); ?>>
                            <?php echo esc_html( $settings['title'] ); ?>
                        </<?php echo esc_attr( $settings['title_tag'] ); ?>>
                        <p><?php echo wp_kses_post( $settings['content'] ); ?></p>
                        <?php if( $button_text != '' ): ?>
                            <?php if($settings['link_type'] == 1): ?>
                                <a href="<?php echo esc_url( $link ); ?>" class="default-btn"><i class="<?php echo esc_attr( $settings['button_icon'] ); ?>"></i> <?php echo esc_html( $button_text ); ?></a>
                            <?php elseif($settings['link_type'] == 2):
                                echo '<a href="' .  $link . '" class="default-btn" ' . $target . $nofollow . '> <i class="'.$settings['button_icon'].'"></i>' . $button_text . ' </a>';
                            endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <!-- Start Living Room Area -->
        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Room );