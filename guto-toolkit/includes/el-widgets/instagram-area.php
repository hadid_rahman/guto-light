<?php
/**
 * Instagram Area Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Instagram_Area extends Widget_Base {

	public function get_name() {
        return 'Instagram_Area';
    }

	public function get_title() {
        return esc_html__( 'Instagram Area', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-instagram-post';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Instagram_Area',
			[
				'label' => esc_html__( 'Guto Instagram Area', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

            $this->add_control(
                'title',
                [
                    'label' => esc_html__( 'Title', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__('Subscribe Now
                    Follow Us On Instagram', 'guto-toolkit'),
                ]
            );

            $this->add_control(
                'title_tag',
                [
                    'label' => esc_html__( 'Title Tag', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        'h1'         => esc_html__( 'h1', 'guto-toolkit' ),
                        'h2'         => esc_html__( 'h2', 'guto-toolkit' ),
                        'h3'         => esc_html__( 'h3', 'guto-toolkit' ),
                        'h4'         => esc_html__( 'h4', 'guto-toolkit' ),
                        'h5'         => esc_html__( 'h5', 'guto-toolkit' ),
                        'h6'         => esc_html__( 'h6', 'guto-toolkit' ),
                    ],
                    'default' => 'h2',
                ]
            );



		$this->add_control(
            'items',
            [
                'label' => esc_html__('Images', 'guto-toolkit'),
                'type' => Controls_Manager::REPEATER,
                'separator' => 'before',
                'fields' => [
					[
						'name'	=> 'link',
						'label' => esc_html__( 'Link', 'guto-toolkit' ),
                        'type'		=> Controls_Manager:: URL,
                        'show_external' => true,
                        'default' => [
                            'url' => '#',
                            'is_external' => true,
                            'nofollow' => true,
                        ],
					],

                    [
                        'name'  => 'image',
                        'label' => esc_html__( 'Image', 'guto-toolkit' ),
                        'type' => Controls_Manager::MEDIA,
                        'label_block' => true,
                    ],

                    [
                        'name'  => 'icon',
                        'label' => esc_html__( 'Icon', 'guto-toolkit' ),
                        'type' => Controls_Manager::ICON,
                        'label_block' => true,
                        'options' => guto_toolkit_icons(),
                    ],
                ]
            ]
        );


        $this->end_controls_section();

        $this->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
        );

            $this->add_control(
                'title_color',
                [
                    'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .section-title h2, .section-title h3, .section-title h4, .section-title h5, .section-title h5, .section-title h6, .section-title h1' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .section-title h2, .section-title h3, .section-title h4, .section-title h5, .section-title h5, .section-title h6, .section-title h1',
                ]
            );


        $this->end_controls_section();
    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        // Inline Editing
        $this-> add_inline_editing_attributes('title','none');

        ?>

        <div class="instagram-area">
            <div class="container">
                <div class="row">
                    <?php if($settings['title'] != ''): ?>
                        <div class="col-lg-5 col-md-12">
                            <div class="section-title">
                                <<?php echo esc_attr( $settings['title_tag'] ); ?> <?php echo $this-> get_render_attribute_string('title'); ?>><?php echo wp_kses_post( $settings['title'] ); ?></<?php echo esc_attr( $settings['title_tag'] ); ?>>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-12">
                    <?php else: ?>
                        <div class="col-lg-12 col-md-12">
                    <?php endif; ?>
                        <div class="instagram-list">
                            <div class="row justify-content-center">
                                <?php foreach($settings['items'] as $item):
                                    $link       = $item['link']['url'];
                                    $target     = $item['link']['is_external'] ? ' target="_blank"' : '';
                                    $nofollow   = $item['link']['nofollow'] ? ' rel="nofollow"' : '';
                                    ?>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-6 p-0">
                                        <div class="instagram-item">

                                            <?php if($item['image']['url'] != ''): ?>
                                                <img src="<?php echo esc_url($item['image']['url']); ?>" alt="<?php echo esc_attr($settings['title']); ?>">
                                            <?php endif; ?>

                                            <?php if($item['icon'] != ''): ?>
                                                <i class='<?php echo esc_attr($item['icon']); ?>'></i>
                                            <?php endif; ?>

                                            <?php  echo '<a href="' .  $link . '" class="link-btn" ' . $target . $nofollow . '></i></a>'; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Instagram_Area );