<?php
/**
 * Pricing Widget
 */

namespace Elementor;
class Guto_Pricing extends Widget_Base {

	public function get_name() {
        return 'Guto_Pricing';
    }

	public function get_title() {
        return __( 'Pricing Table', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-price-table';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Pricing_Area',
			[
				'label' => esc_html__( 'Pricing Controls', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

            $pricing_items = new Repeater();

            $pricing_items->add_control(
                'icon_color',
                [
                    'label' => esc_html__( 'Header Background Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        ''          => esc_html__('Color One', 'guto-toolkit' ),
                        'red'       => esc_html__('Color Two', 'guto-toolkit' ),
                        'orange'    => esc_html__('Color Three', 'guto-toolkit' ),
                    ],
                ]
            );
            $pricing_items->add_control(
                'title',
                [
                    'label' => esc_html__( 'Title', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__('Basic', 'guto-toolkit'),
                ]
            );

            $pricing_items->add_control(
                'price_prefix',
                [
                    'label' => esc_html__( 'Price Prefix', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__('$', 'guto-toolkit'),
                ]
            );

            $pricing_items->add_control(
                'price',
                [
                    'label' => esc_html__( 'Price', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__('39.99', 'guto-toolkit'),
                ]
            );

            $pricing_items->add_control(
                'price_suffix',
                [
                    'label' => esc_html__( 'Number Suffix', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__('/mo', 'guto-toolkit'),
                ]
            );

            $pricing_items->add_control(
                'features',
                [
                    'label' => esc_html__( 'Features', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXTAREA,
                    'default' =>
'<li><i class="bx bx-check"></i> 1 Projects</li>
<li><i class="bx bx-check"></i> Email Support</li>
<li><i class="bx bx-check"></i> Phone Support</li>
<li><i class="bx bx-check"></i> Article Promotion</li>
<li><i class="bx bx-check"></i> Editorial Services</li>
<li><i class="bx bx-x"></i> Profile Management</li>
<li><i class="bx bx-x"></i> Selection Support</li>',
                ]
            );
            $pricing_items->add_control(
                'button',
                [
                    'label' => esc_html__( 'Button Text', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__('Get Started', 'guto-toolkit'),
                ]
            );

            $pricing_items->add_control(
                'button_link',
                [
                    'label' => esc_html__( 'Button Link', 'guto-toolkit' ),
                    'type' => Controls_Manager::URL,
                ]
            );

            $this->add_control(
                'list_items',
                [
                    'label' => esc_html__( 'Add Item', 'guto-toolkit' ),
                    'type' => Controls_Manager::REPEATER,
                    'fields' => $pricing_items->get_controls(),
                    'title_field' => '{{{ title }}}',
                ]
            );

            $this->add_control(
                'icon',
                [
                    'label' => esc_html__( 'Button Icons', 'guto-toolkit' ),
                    'type' => Controls_Manager::ICON,
                    'label_block' => true,
                    'options' => guto_toolkit_icons(),
                ]
            );
        $this->end_controls_section();

        $this->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
        );

            $this->add_control(
                'color_one',
                [
                    'label' => esc_html__( 'Color One', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .single-pricing-box .pricing-header' => 'background-color: {{VALUE}}',
                    ],
                ]
            );
            $this->add_control(
                'color_two',
                [
                    'label' => esc_html__( 'Color Two', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .single-pricing-box.red .pricing-header, .single-pricing-box.red .default-btn' => 'background-color: {{VALUE}}',
                    ],
                ]
            );
            $this->add_control(
                'color_three',
                [
                    'label' => esc_html__( 'Color Three', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .single-pricing-box.orange .pricing-header, .single-pricing-box.orange .default-btn' => 'background-color: {{VALUE}}',
                    ],
                ]
            );

			$this->add_control(
				'title_color',
				[
					'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .single-pricing-box .pricing-header h3' => 'color: {{VALUE}}',
					],
				]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-pricing-box .pricing-header h3',
                ]
            );

            $this->add_control(
				'price_color',
				[
					'label' => esc_html__( 'Price Color', 'guto-toolkit' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .single-pricing-box .price' => 'color: {{VALUE}}',
					],
				]
			);

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'price_typography',
                    'label' => esc_html__( 'Price Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-pricing-box .price',
                ]
            );

            $this->add_control(
				'suf_color',
				[
					'label' => esc_html__( 'Price Prefix and Suffix Color', 'guto-toolkit' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .single-pricing-box .price sub' => 'color: {{VALUE}}',
					],
				]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'suf_typography',
                    'label' => esc_html__( 'Price Prefix and Suffix Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-pricing-box .price sub',
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'content_typography',
                    'label' => esc_html__( 'Content Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-pricing-box .price-features-list li',
                ]
            );

        $this->end_controls_section();

    }

	protected function render() {

        $settings = $this->get_settings_for_display();
        $table_item_count = 1;
        ?>
            <div class="container">
                <div class="row justify-content-center">
                    <?php foreach( $settings['list_items'] as $item ): ?>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="single-pricing-box <?php echo esc_attr( $item['icon_color'] ); ?>">
                                <div class="pricing-header">
                                    <h3><?php echo esc_html( $item['title'] ); ?></h3>
                                </div>

                                <div class="price">
                                    <sub><?php echo esc_html( $item['price_prefix'] ); ?></sub>
                                    <?php echo esc_html( $item['price'] ); ?>
                                    <sub><?php echo esc_html( $item['price_suffix'] ); ?></sub>
                                </div>

                                <ul class="price-features-list">
                                    <?php echo wp_kses_post($item['features']); ?>
                                </ul>

                                <?php if( $item['button'] != '' ): ?>
                                    <a href="<?php echo esc_url( $item['button_link']['url'] ); ?>" class="default-btn"><i class='<?php echo esc_attr($settings['icon']); ?>'></i> <?php echo esc_html( $item['button'] ); ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php
    }

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Pricing );