<?php
/**
 * Promotions Area Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Promotions_Area extends Widget_Base {

	public function get_name() {
        return 'Promotions_Area';
    }

	public function get_title() {
        return esc_html__( 'Promotions Area', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-posts-group';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Promotions_Area',
			[
				'label' => esc_html__( 'Guto Promotions Area', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );
            $this->add_control(
                'style',
                [
                    'label' => esc_html__( 'Section Style', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        '1'         => esc_html__( 'Style One', 'guto-toolkit' ),
                        '2'         => esc_html__( 'Style Two', 'guto-toolkit' ),
                        '3'         => esc_html__( 'Style Three', 'guto-toolkit' ),
                    ],
                    'default' => '1',
                ]
            );

            $this->add_control(
                'title',
                [
                    'label' => esc_html__( 'Title', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__("The comfort you’ve been looking for", 'guto-toolkit'),
                ]
            );

            $this->add_control(
                'sub_title',
                [
                    'label' => esc_html__( 'Sub Title', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__("Cloths Accessories & Shoes", 'guto-toolkit'),
                    'condition' => [
                        'style' => ['2', '3'],
                    ]
                ]
            );

            $this->add_control(
                'title_tag',
                [
                    'label' => esc_html__( 'Title Tag', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        'h1'         => esc_html__( 'h1', 'guto-toolkit' ),
                        'h2'         => esc_html__( 'h2', 'guto-toolkit' ),
                        'h3'         => esc_html__( 'h3', 'guto-toolkit' ),
                        'h4'         => esc_html__( 'h4', 'guto-toolkit' ),
                        'h5'         => esc_html__( 'h5', 'guto-toolkit' ),
                        'h6'         => esc_html__( 'h6', 'guto-toolkit' ),
                    ],
                    'default' => 'h2',
                ]
            );

            $this->add_control(
                'content',
                [
                    'label' => esc_html__( 'Content', 'guto-toolkit' ),
                    'type' => Controls_Manager::WYSIWYG,
                    'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.', 'guto-toolkit'),
                    'condition' => [
                        'style' => ['1', '2']
                    ]
                ]
            );

            $this->add_control(
				'button_text',
				[
					'label' 	=> esc_html__( 'Button Text', 'guto-toolkit' ),
					'type' 		=> Controls_Manager::TEXT,
					'default' 	=> esc_html__("Shop Women's Blazer", 'guto-toolkit'),
				]
            );

            $this->add_control(
				'button_icon',
				[
					'label' => esc_html__( 'Button Icon', 'guto-toolkit' ),
                    'type' => Controls_Manager::ICON,
                    'label_block' => true,
                    'options' => guto_toolkit_icons(),
				]
            );

            $this->add_control(
                'link_type',
                [
                    'label' 		=> esc_html__( 'Button Link Type', 'guto-toolkit' ),
                    'type' 			=> Controls_Manager::SELECT,
                    'label_block' 	=> true,
                    'options' => [
                        '1'  	=> esc_html__( 'Link To Page', 'guto-toolkit' ),
                        '2' 	=> esc_html__( 'External Link', 'guto-toolkit' ),
                    ],
                ]
            );

            $this->add_control(
                'link_to_page',
                [
                    'label' 		=> esc_html__( 'Button Link Page', 'guto-toolkit' ),
                    'type' 			=> Controls_Manager::SELECT,
                    'label_block' 	=> true,
                    'options' 		=> guto_toolkit_get_page_as_list(),
                    'condition' => [
                        'link_type' => '1'
                    ]
                ]
            );

            $this->add_control(
                'ex_link',
                [
                    'label'		=> esc_html__('Button External Link', 'guto-toolkit'),
                    'type'		=> Controls_Manager:: URL,
                    'show_external' => true,
                    'default' => [
                        'url' => '#',
                        'is_external' => true,
                        'nofollow' => true,
                    ],
                    'condition' => [
                        'link_type' => '2'
                    ]
                ]
            );

            $this->add_control(
                'image',
                [
                    'label' => esc_html__( 'Promotions Area Image', 'guto-toolkit' ),
                    'type'	 => Controls_Manager::MEDIA,
                ]
            );

            $this->add_control(
                'image2',
                [
                    'label' => esc_html__( 'Promotions Area Image Two', 'guto-toolkit' ),
                    'type'	 => Controls_Manager::MEDIA,
                    'condition' => [
                        'style' => '2'
                    ]
                ]
            );

        $this->end_controls_section();

        $this->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
        );

            $this->add_control(
                'title_color',
                [
                    'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .promotions-content h2, .promotions-content h3, .promotions-content h4, .promotions-content h5, .promotions-content h5, .promotions-content h6, .promotions-content h1, .promotions-text h2, .promotions-text h3, .promotions-text h4, .promotions-text h5, .promotions-text h5, .promotions-text h6, .promotions-text h1, .promotions-content-fs h2, .promotions-content-fs h3, .promotions-content-fs h4, .promotions-content-fs h5, .promotions-content-fs h5, .promotions-content-fs h6, .promotions-content-fs h1' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .promotions-content h2, .promotions-content h3, .promotions-content h4, .promotions-content h5, .promotions-content h5, .promotions-content h6, .promotions-content h1, .promotions-text h2, .promotions-text h3, .promotions-text h4, .promotions-text h5, .promotions-text h5, .promotions-text h6, .promotions-text h1, .promotions-content-fs h2, .promotions-content-fs h3, .promotions-content-fs h4, .promotions-content-fs h5, .promotions-content-fs h5, .promotions-content-fs h6, .promotions-content-fs h1',
                ]
            );

            $this->add_control(
                'sub_title_color',
                [
                    'label' => esc_html__( 'Sub Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .promotions-text h2 span, .promotions-text h3 span, .promotions-text h4 span, .promotions-text h5 span, .promotions-text h5 span, .promotions-text h6 span, .promotions-text h1, .promotions-content-fs span' => 'color: {{VALUE}}',
                    ],
                    'condition' => [
                        'style' => ['2', '3']
                    ]
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'sub_typography',
                    'label' => esc_html__( 'Sub Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .promotions-text h2 span, .promotions-text h3 span, .promotions-text h4 span, .promotions-text h5 span, .promotions-text h5 span, .promotions-text h6 span, .promotions-text h1, .promotions-content-fs span',
                    'condition' => [
                        'style' => ['2', '3']
                    ]
                ]
            );

            $this->add_control(
                'content_color',
                [
                    'label' => esc_html__( 'Content Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .promotions-content p, .promotions-text p, promotions-content-fs p' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'content_typography',
                    'label' => esc_html__( 'Content Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .promotions-content p, .promotions-text p, promotions-content-fs p',
                ]

            );

        $this->end_controls_section();
    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        // Inline Editing
        $this-> add_inline_editing_attributes('title','none');
        $this-> add_inline_editing_attributes('content','none');

        // Get Button Link
        if($settings['link_type'] == 1){
            $link = get_page_link( $settings['link_to_page'] );
        } elseif($settings['link_type'] == 2) {
            $link = $settings['ex_link']['url'];
            $target = $settings['ex_link']['is_external'] ? ' target="_blank"' : '';
		    $nofollow = $settings['ex_link']['nofollow'] ? ' rel="nofollow"' : '';
        }

        $button_text = $settings['button_text'];

        ?>

        <?php if($settings['style'] == '1'): ?>
            <div class="promotions-area pt-100">
                <div class="container">
                    <div class="promotions-content">
                        <?php if( $settings['image']['url'] != '' ): ?>
                            <img src="<?php echo esc_url($settings['image']['url']); ?>" alt="<?php echo wp_kses_post( $settings['title'] ); ?>">
                        <?php endif; ?>

                        <<?php echo esc_attr( $settings['title_tag'] ); ?> <?php echo $this-> get_render_attribute_string('title'); ?>><?php echo wp_kses_post( $settings['title'] ); ?></<?php echo esc_attr( $settings['title_tag'] ); ?>>

                        <p><?php echo wp_kses_post( $settings['content'] ); ?></p>

                        <?php if( $button_text != '' ): ?>
                            <?php if($settings['link_type'] == 1): ?>
                                <a href="<?php echo esc_url( $link ); ?>" class="default-btn"><i class="<?php echo esc_attr( $settings['button_icon'] ); ?>"></i> <?php echo esc_html( $button_text ); ?></a>
                            <?php elseif($settings['link_type'] == 2):
                                echo '<a href="' .  $link . '" class="default-btn" ' . $target . $nofollow . '> <i class="'.$settings['button_icon'].'"></i>' . $button_text . ' </a>';
                            endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if($settings['style'] == '2'): ?>
            <div class="promotions-area pt-100">
                <div class="container">
                    <div class="row justify-content-center m-0">
                        <div class="col-lg-4 col-md-6 col-sm-12 p-0">
                            <?php if( $settings['image']['url'] != '' ): ?>
                                <div class="promotions-image" style="background-image:url(<?php echo esc_url($settings['image']['url']); ?>);">
                                    <img src="<?php echo esc_url($settings['image']['url']); ?>" alt="<?php echo wp_kses_post( $settings['title'] ); ?>">
                                </div>
                            <?php endif; ?>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-12 p-0">
                            <div class="promotions-text">
                                <<?php echo esc_attr( $settings['title_tag'] ); ?> <?php echo $this-> get_render_attribute_string('title'); ?>><?php echo wp_kses_post( $settings['title'] ); ?><span><?php echo wp_kses_post( $settings['sub_title'] ); ?></span></<?php echo esc_attr( $settings['title_tag'] ); ?>>

                                <p><?php echo wp_kses_post( $settings['content'] ); ?></p>

                                <?php if( $button_text != '' ): ?>
                                    <?php if($settings['link_type'] == 1): ?>
                                        <a href="<?php echo esc_url( $link ); ?>" class="default-btn"><i class="<?php echo esc_attr( $settings['button_icon'] ); ?>"></i> <?php echo esc_html( $button_text ); ?></a>
                                    <?php elseif($settings['link_type'] == 2):
                                        echo '<a href="' .  $link . '" class="default-btn" ' . $target . $nofollow . '> <i class="'.$settings['button_icon'].'"></i>' . $button_text . ' </a>';
                                    endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-12 p-0">
                            <?php if( $settings['image2']['url'] != '' ): ?>
                                <div class="promotions-image" style="background-image:url(<?php echo esc_url($settings['image2']['url']); ?>);">
                                    <img src="<?php echo esc_url($settings['image2']['url']); ?>" alt="<?php echo wp_kses_post( $settings['title'] ); ?>">
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if($settings['style'] == '3'): ?>
            <div class="promotions-area ptb-100 bg-fafafa">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-12">
                            <div class="promotions-content-fs">
                                <<?php echo esc_attr( $settings['title_tag'] ); ?> <?php echo $this-> get_render_attribute_string('title'); ?>><?php echo wp_kses_post( $settings['title'] ); ?></<?php echo esc_attr( $settings['title_tag'] ); ?>>

                                <span><?php echo wp_kses_post( $settings['sub_title'] ); ?></span>

                                <?php if( $button_text != '' ): ?>
                                    <?php if($settings['link_type'] == 1): ?>
                                        <a href="<?php echo esc_url( $link ); ?>" class="default-btn"><i class="<?php echo esc_attr( $settings['button_icon'] ); ?>"></i> <?php echo esc_html( $button_text ); ?></a>
                                    <?php elseif($settings['link_type'] == 2):
                                        echo '<a href="' .  $link . '" class="default-btn" ' . $target . $nofollow . '> <i class="'.$settings['button_icon'].'"></i>' . $button_text . ' </a>';
                                    endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <?php if( $settings['image']['url'] != '' ): ?>
                                <img src="<?php echo esc_url($settings['image']['url']); ?>" alt="<?php echo wp_kses_post( $settings['title'] ); ?>">
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif;

	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Promotions_Area );