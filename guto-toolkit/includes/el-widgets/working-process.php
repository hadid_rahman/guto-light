<?php
/**
 * Working Process Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Working_Process extends Widget_Base {

	public function get_name() {
        return 'WorkingProcess';
    }

	public function get_title() {
        return esc_html__( 'Working Process', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-animation';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Working Process Content', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

        $this->add_control(
            'title_tag',
            [
                'label' => esc_html__( 'Title Tag', 'guto-toolkit' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'h1'         => esc_html__( 'h1', 'guto-toolkit' ),
                    'h2'         => esc_html__( 'h2', 'guto-toolkit' ),
                    'h3'         => esc_html__( 'h3', 'guto-toolkit' ),
                    'h4'         => esc_html__( 'h4', 'guto-toolkit' ),
                    'h5'         => esc_html__( 'h5', 'guto-toolkit' ),
                    'h6'         => esc_html__( 'h6', 'guto-toolkit' ),
                ],
                'default' => 'h2',
            ]
        );

        $this->add_control(
            'columns',
            [
                'label' => esc_html__( 'Choose Columns', 'guto-toolkit' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '1'   => esc_html__( '1', 'guto-toolkit' ),
                    '2'   => esc_html__( '2', 'guto-toolkit' ),
                    '3'   => esc_html__( '3', 'guto-toolkit' ),
                    '4'   => esc_html__( '4', 'guto-toolkit' ),
                ],
                'default' => '3',
            ]
        );

		$this->add_control(
            'cards',
            [
                'label' => esc_html__('Card Items', 'guto-toolkit'),
                'type' => Controls_Manager::REPEATER,
                'fields' => [
                    [
                        'name'          => 'count',
                        'label'         => esc_html__( 'Count', 'guto-toolkit' ),
                        'default'       => esc_html__('1', 'guto-toolkit'),
                        'type'          => Controls_Manager::TEXT,
                        'label_block'   => true,
                    ],
                    [
                        'name'          => 'title',
                        'label'         => esc_html__( 'Title', 'guto-toolkit' ),
                        'default'       => esc_html__('Planning & Analysis', 'guto-toolkit'),
                        'type'          => Controls_Manager::TEXT,
                        'label_block'   => true,
                    ],
                    [
                        'name'      => 'content',
                        'label'     => esc_html__('Content', 'guto-toolkit'),
                        'type'      => Controls_Manager::TEXTAREA,
                        'default'   => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore dolore magna.', 'guto-toolkit'),
                        'label_block' => true,
                    ],
                    [
                        'name' => 'icon',
                        'label' => esc_html__('Icon', 'guto-toolkit'),
                        'type' => Controls_Manager::ICON,
                        'label_block' => true,
                        'options' => guto_toolkit_icons(),
                    ],
                    [
                        'name' => 'icon_bg',
                        'label' => esc_html__('Icon Background Color', 'guto-toolkit'),
                        'type' => Controls_Manager::COLOR,
                        'label_block' => true,
                    ],
                ],
            ]
        );

    $this->end_controls_section();

    $this->start_controls_section(
        'style',
        [
            'label' => esc_html__( 'Style', 'guto-toolkit' ),
            'tab' => Controls_Manager::TAB_STYLE,
        ]
    );

        $this->add_control(
            'title_color',
            [
                'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .single-working-process-box h1, .single-working-process-box h2, .single-working-process-box h3, .single-working-process-box h4, .single-working-process-box h5, .single-working-process-box h6' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .single-working-process-box h1, .single-working-process-box h2, .single-working-process-box h3, .single-working-process-box h4, .single-working-process-box h5, .single-working-process-box h6',
            ]
        );

        $this->add_control(
            'content_color',
            [
                'label' => esc_html__( 'Content Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .single-working-process-box p' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'label' => esc_html__( 'Content Typography', 'guto-toolkit' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .single-working-process-box p',
            ]
        );

    $this->end_controls_section();

    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        $cards = $settings['cards'];

        // Card Columns
        $columns = $settings['columns'];
        if ($columns == '1') {
            $column = 'col-lg-12 col-sm-6 col-md-6';
        }elseif ($columns == '2') {
            $column = 'col-lg-6 col-sm-6 col-md-6';
        }elseif ($columns == '3') {
            $column = 'col-lg-4 col-sm-6 col-md-6';
        }elseif ($columns == '4') {
            $column = 'col-lg-3 col-sm-6 col-md-6';
        }

        ?>
        <div class="container">
            <div class="row justify-content-center">
                <?php $i = 0; foreach ($cards as $key => $item): ?>
                    <div class="<?php echo esc_attr( $column );?>">
                        <div class="single-working-process-box">
                            <span class="number"><?php echo esc_html( $item['count'] ); ?></span>
                            <?php if($item['icon'] != ''): ?>
                            <div class="icon" <?php if($item['icon_bg'] != ''): ?>style="background-color:<?php echo esc_attr($item['icon_bg']); ?>" <?php endif; ?>>
                                    <i class='<?php echo esc_attr( $item['icon'] ); ?>'></i>
                                </div>
                            <?php endif; ?>
                            <h3><?php echo esc_html( $item['title'] ); ?></h3>
                            <p><?php echo wp_kses_post( $item['content'] ); ?></p>
                        </div>
                    </div>
                <?php $i++; endforeach; ?>
            </div>
        </div>
        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Working_Process );