<?php
/**
 * Team Widget
 */
namespace Elementor;
class Guto_Team extends Widget_Base{
    public function get_name(){
        return "Guto_Team";
    }
    public function get_title(){
        return "Team";
    }
    public function get_icon(){
        return "eicon-gallery-group";
    }
    public function get_categories(){
        return ['guto-elements'];
    }
    protected function _register_controls(){

    $this->start_controls_section(
        'Guto_Team',
        [
            'label' => esc_html__( 'Guto Team', 'guto-toolkit' ),
            'tab' => Controls_Manager::TAB_CONTENT,
        ]
    );

        $this->add_control(
            'columns',
            [
                'label' => esc_html__( 'Choose Columns', 'guto-toolkit' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '1'   => esc_html__( '1', 'guto-toolkit' ),
                    '2'   => esc_html__( '2', 'guto-toolkit' ),
                    '3'   => esc_html__( '3', 'guto-toolkit' ),
                    '4'   => esc_html__( '4', 'guto-toolkit' ),
                ],
                'default' => '4',
            ]
        );

        $repeater = new Repeater();

        $repeater->add_control(
            'member_img',
            [
                'label' => esc_html__( 'Image', 'guto-toolkit' ),
                'type' => Controls_Manager::MEDIA,
                'label_block' => true,
            ]
        );
        $repeater->add_control(
            'name',
            [
                'label' => esc_html__( 'Member Name', 'guto-toolkit' ),
                'type' => Controls_Manager::TEXT,
                'default'       => esc_html__('Sarah Taylor', 'guto-toolkit'),
                'label_block'   => true,
            ]
        );
        $repeater->add_control(
            'designation',
            [
                'label' => esc_html__( 'Designation', 'guto-toolkit' ),
                'type' => Controls_Manager::TEXT,
                'default'       => esc_html__('CEO & Founder', 'guto-toolkit'),
                'label_block'   => true,
            ]
        );
        $repeater->add_control(
            'icon1',
            [
                'label' => esc_html__( 'Social Icon One', 'guto-toolkit' ),
                'type' => Controls_Manager::ICON,
                'label_block' => true,
                'options' => guto_toolkit_icons(),
            ]
        );
        $repeater->add_control(
            'url1',
            [
                'label' => esc_html__( 'Social Link One', 'guto-toolkit' ),
                'type' => Controls_Manager::URL,
            ]
        );

        $repeater->add_control(
            'icon2',
            [
                'label' => esc_html__( 'Social Icon Two', 'guto-toolkit' ),
                'type' => Controls_Manager::ICON,
                'label_block' => true,
                'options' => guto_toolkit_icons(),
            ]
        );
        $repeater->add_control(
            'url2',
            [
                'label' => esc_html__( 'Social Link Two', 'guto-toolkit' ),
                'type' => Controls_Manager::URL,
            ]
        );

        $repeater->add_control(
            'icon3',
            [
                'label' => esc_html__( 'Social Icon Three', 'guto-toolkit' ),
                'type' => Controls_Manager::ICON,
                'label_block' => true,
                'options' => guto_toolkit_icons(),
            ]
        );
        $repeater->add_control(
            'url3',
            [
                'label' => esc_html__( 'Social Link Three', 'guto-toolkit' ),
                'type' => Controls_Manager::URL,
            ]
        );

        $repeater->add_control(
            'icon4',
            [
                'label' => esc_html__( 'Social Icon Four', 'guto-toolkit' ),
                'type' => Controls_Manager::ICON,
                'label_block' => true,
                'options' => guto_toolkit_icons(),
            ]
        );
        $repeater->add_control(
            'url4',
            [
                'label' => esc_html__( 'Social Link Four', 'guto-toolkit' ),
                'type' => Controls_Manager::URL,
            ]
        );

        $repeater->add_control(
            'icon5',
            [
                'label' => esc_html__( 'Social Icon Five', 'guto-toolkit' ),
                'type' => Controls_Manager::ICON,
                'label_block' => true,
                'options' => guto_toolkit_icons(),
            ]
        );
        $repeater->add_control(
            'url5',
            [
                'label' => esc_html__( 'Social Link Five', 'guto-toolkit' ),
                'type' => Controls_Manager::URL,
            ]
        );

        $this->add_control(
            'teams',
            [
                'label' => esc_html__( 'Add Member', 'guto-toolkit' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
            ]
        );
    $this-> end_controls_section();

    // Start Style content controls
    $this-> start_controls_section(
        'style',
        [
            'label'=>esc_html__('Style', 'guto-toolkit'),
            'tab'=> Controls_Manager::TAB_STYLE,
        ]
    );

        $this->add_control(
            'name_color',
            [
                'label' => esc_html__( 'Member Name Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .single-team-box .content h3' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'name_typography',
                'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .single-team-box .content h3',
            ]
        );

        $this->add_control(
            'designation_color',
            [
                'label' => esc_html__( 'Member Designation Color', 'guto-toolkit' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .single-team-box .content span' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'designation_typography',
                'label' => esc_html__( 'Designation Typography', 'guto-toolkit' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .single-team-box .content h3',
            ]
        );

    $this-> end_controls_section();
}
    protected function render()
    {
        $settings = $this->get_settings_for_display();

        // Card Columns
        $columns = $settings['columns'];
        if ($columns == '1') {
            $column = 'col-lg-12 col-sm-6 col-md-6';
        }elseif ($columns == '2') {
            $column = 'col-lg-6 col-sm-6 col-md-6';
        }elseif ($columns == '3') {
            $column = 'col-lg-4 col-sm-6 col-md-6';
        }elseif ($columns == '4') {
            $column = 'col-lg-3 col-sm-6 col-md-6';
        }

        ?>
        <div class="container">
            <div class="row">
                <?php foreach( $settings['teams'] as $item ): ?>
                    <div class="<?php echo esc_attr( $column );?>">
                        <div class="single-team-box">
                            <div class="image">
                                <?php if( $item['member_img']['url'] != '' ): ?>
                                    <img src="<?php echo esc_url( $item['member_img']['url'] ); ?>" alt="<?php echo esc_attr( $item['name'] ); ?>">
                                <?php endif; ?>
                                <div class="social">
                                    <span><i class="bx bx-share-alt"></i></span>
                                    <ul>
                                        <?php
                                        $icon_class1 = str_replace(array('bx bxl-', '', 'fa fa-', '', 'bx bx-', ''), ' ', $item['icon1']);
                                        $icon_class2 = str_replace(array('bx bxl-', '', 'fa fa-', '', 'bx bx-', ''), ' ', $item['icon2']);
                                        $icon_class3 = str_replace(array('bx bxl-', '', 'fa fa-', '', 'bx bx-', ''), ' ', $item['icon3']);
                                        $icon_class4 = str_replace(array('bx bxl-', '', 'fa fa-', '', 'bx bx-', ''), ' ', $item['icon4']);
                                        $icon_class5 = str_replace(array('bx bxl-', '', 'fa fa-', '', 'bx bx-', ''), ' ', $item['icon5']);
                                        ?>
                                        <?php if( $item['icon1'] != '' && $item['url1']['url'] != '' ): ?>
                                            <li><a class="<?php echo esc_attr($icon_class1); ?>" href="<?php echo esc_url( $item['url1']['url'] ); ?>"><i class="<?php echo esc_attr( $item['icon1'] ); ?>"></i></a></li>
                                        <?php endif; ?>

                                        <?php if( $item['icon2'] != '' && $item['url2']['url'] != '' ): ?>
                                            <li><a class="<?php echo esc_attr($icon_class2); ?>" href="<?php echo esc_url( $item['url2']['url'] ); ?>"><i class="<?php echo esc_attr( $item['icon2'] ); ?>"></i></a></li>
                                        <?php endif; ?>

                                        <?php if( $item['icon3'] != '' && $item['url3']['url'] != '' ): ?>
                                            <li><a class="<?php echo esc_attr($icon_class3); ?>" href="<?php echo esc_url( $item['url3']['url'] ); ?>"><i class="<?php echo esc_attr( $item['icon3'] ); ?>"></i></a></li>
                                        <?php endif; ?>

                                        <?php if( $item['icon4'] != '' && $item['url4']['url'] != '' ): ?>
                                            <li><a class="<?php echo esc_attr($icon_class4); ?>" href="<?php echo esc_url( $item['url4']['url'] ); ?>"><i class="<?php echo esc_attr( $item['icon4'] ); ?>"></i></a></li>
                                        <?php endif; ?>
                                        <?php if( $item['icon5']
                                            != '' && $item['url5']['url'] != '' ): ?>
                                            <li><a class="<?php echo esc_attr($icon_class5); ?>" href="<?php echo esc_url( $item['url5']['url'] ); ?>"><i class="<?php echo esc_attr( $item['icon5'] ); ?>"></i></a></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>

                            <div class="content">
                                <h3><?php echo esc_html( $item['name'] ); ?></h3>
                                <span><?php echo esc_html( $item['designation'] ); ?></span>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php
    }

    protected function _content_template() {}
}
Plugin::instance()->widgets_manager->register_widget_type( new Guto_Team );