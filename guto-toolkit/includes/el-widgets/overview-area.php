<?php
/**
 * Overview Area Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Overview_Area extends Widget_Base {

	public function get_name() {
        return 'Overview_Area';
    }

	public function get_title() {
        return esc_html__( 'Overview Area', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-info-box';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Overview_Area',
			[
				'label' => esc_html__( 'Guto Overview Area', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );
                
            $this->add_control(
                'overview_style',
                [
                    'label' => esc_html__( 'Choose Style', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        '1'   => esc_html__( 'Style One', 'guto-toolkit' ),
                        '2'   => esc_html__( 'Style Two', 'guto-toolkit' ),
                    ],
                    'default' => '1',
                ]
            );
        
            $this->add_control(
                'title',
                [
                    'label' => esc_html__( 'Title', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__('Digital Marketing', 'guto-toolkit'),
                ]
            );

            $this->add_control(
                'title_tag',
                [
                    'label' => esc_html__( 'Title Tag', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        'h1'         => esc_html__( 'h1', 'guto-toolkit' ),
                        'h2'         => esc_html__( 'h2', 'guto-toolkit' ),
                        'h4'         => esc_html__( 'h4', 'guto-toolkit' ),
                        'h5'         => esc_html__( 'h5', 'guto-toolkit' ),
                        'h6'         => esc_html__( 'h6', 'guto-toolkit' ),
                    ],
                    'default' => 'h2',
                ]
            );

            $this->add_control(
                'content',
                [
                    'label' => esc_html__( 'Content', 'guto-toolkit' ),
                    'type' => Controls_Manager::WYSIWYG,
                    'default' => esc_html__('We believe brand interaction is key in communication. Real innovations and a positive customer experience are the heart of successful communication. No fake products and services. The customer is king, their lives and needs are the inspiration.', 'guto-toolkit'),
                ]
            );

            $this->add_control(
                'image',
                [
                    'label' => esc_html__( 'Overview Area Image', 'guto-toolkit' ),
                    'type'	 => Controls_Manager::MEDIA,
                ]
            );

        $this->end_controls_section();

        $this->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
        );

            $this->add_control(
                'title_color',
                [
                    'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .services-details-overview .services-details-desc h2, .services-details-overview .services-details-desc h4, .services-details-overview .services-details-desc h5, .services-details-overview .services-details-desc h5, .services-details-overview .services-details-desc h6, .services-details-overview .services-details-desc h1' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .services-details-overview .services-details-desc h2, .services-details-overview .services-details-desc h4, .services-details-overview .services-details-desc h5, .services-details-overview .services-details-desc h5, .services-details-overview .services-details-desc h6, .services-details-overview .services-details-desc h1',
                ]
            );

            $this->add_control(
                'content_color',
                [
                    'label' => esc_html__( 'Content Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .services-details-overview .services-details-desc p' => 'color: {{VALUE}}',
                    ],
                ]
            );
            
            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'content_typography',
                    'label' => esc_html__( 'Content Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .services-details-overview .services-details-desc p',
                ]

            );
            
        $this->end_controls_section();
    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        // Inline Editing
        $this-> add_inline_editing_attributes('title','none');
        $this-> add_inline_editing_attributes('content','none');
        ?>
            <div class="container">
                <?php if($settings['overview_style'] == '1'): ?>
                    <div class="services-details-overview">
                        <div class="services-details-desc">
                            <<?php echo esc_attr( $settings['title_tag'] ); ?> <?php echo $this-> get_render_attribute_string('title'); ?>><?php echo wp_kses_post( $settings['title'] ); ?></<?php echo esc_attr( $settings['title_tag'] ); ?>>
                            
                            <?php echo wp_kses_post( $settings['content'] ); ?>
                            
                        </div>
                        
                        <?php if( $settings['image']['url'] != '' ): ?>
                            <div class="services-details-image">
                                <img src="<?php echo esc_url($settings['image']['url']); ?>" alt="<?php esc_attr_e('Overview Image', 'guto-toolkit'); ?>">
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <?php if($settings['overview_style'] == '2'): ?>
                    <div class="services-details-overview">
                        <?php if( $settings['image']['url'] != '' ): ?>
                            <div class="services-details-image">
                                <img src="<?php echo esc_url($settings['image']['url']); ?>" alt="<?php esc_attr_e('Overview Image', 'guto-toolkit'); ?>">
                            </div>
                        <?php endif; ?>

                        <div class="services-details-desc">
                            <<?php echo esc_attr( $settings['title_tag'] ); ?> <?php echo $this-> get_render_attribute_string('title'); ?>><?php echo wp_kses_post( $settings['title'] ); ?></<?php echo esc_attr( $settings['title_tag'] ); ?>>
                            
                            <?php echo wp_kses_post( $settings['content'] ); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Overview_Area );