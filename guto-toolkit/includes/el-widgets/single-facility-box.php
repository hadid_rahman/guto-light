<?php
/**
 * Facility Area Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Facility_Area extends Widget_Base {

	public function get_name() {
        return 'Facility_Area';
    }

	public function get_title() {
        return esc_html__( 'Facility Area', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-carousel';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Facility_Area',
			[
				'label' => esc_html__( 'Guto Facility Area', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

		$this->add_control(
            'items',
            [
                'label' => esc_html__('Items', 'guto-toolkit'),
                'type' => Controls_Manager::REPEATER,
                'separator' => 'before',
                'fields' => [
					[
						'name'	=> 'title',
						'label' => esc_html__( 'Title', 'guto-toolkit' ),
                        'type'		=> Controls_Manager:: TEXT,
					],
                    [
                        'name'  => 'icon',
                        'label' => esc_html__( 'Icon', 'guto-toolkit' ),
                        'type' => Controls_Manager::ICON,
                        'label_block' => true,
                        'options' => guto_toolkit_icons(),
                    ],
                ]
            ]
        );


        $this->end_controls_section();

        $this->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
        );

            $this->add_control(
                'title_color',
                [
                    'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .single-facility-box h3' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .single-facility-box h3',
                ]
            );


        $this->end_controls_section();
    }

	protected function render() {

        $settings = $this->get_settings_for_display();
        ?>
        <div class="container">
            <div class="row">
                <?php foreach($settings['items'] as $item): ?>
                    <div class="col-lg-3 col-sm-6 col-md-6">
                        <div class="single-facility-box">
                            <div class="icon">
                                <i class="<?php echo esc_attr($item['icon']); ?>"></i>
                            </div>
                            <h3><?php echo esc_html( $item['title'] ); ?></h3>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Facility_Area );