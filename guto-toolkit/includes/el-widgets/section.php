<?php
/**
 * Section Widget
 */

namespace Elementor;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Guto_Section extends Widget_Base {

	public function get_name() {
        return 'Section';
    }

	public function get_title() {
        return esc_html__( 'Section', 'guto-toolkit' );
    }

	public function get_icon() {
        return 'eicon-handle';
    }

	public function get_categories() {
        return [ 'guto-elements' ];
    }

	protected function _register_controls() {

        $this->start_controls_section(
			'Guto_Section',
			[
				'label' => esc_html__( 'Guto Section', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
        );

            $this->add_control(
                'top_title',
                [
                    'label' => esc_html__( 'Top Title', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__('FEATURED SERVICES', 'guto-toolkit'),
                ]
            );

            $this->add_control(
                'title',
                [
                    'label' => esc_html__( 'Title', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXT,
                    'default' => esc_html__('We Provide Best Services for Our Customers Based on Needs', 'guto-toolkit'),
                ]
            );

            $this->add_control(
                'title_tag',
                [
                    'label' => esc_html__( 'Title Tag', 'guto-toolkit' ),
                    'type' => Controls_Manager::SELECT,
                    'options' => [
                        'h1'         => esc_html__( 'h1', 'guto-toolkit' ),
                        'h2'         => esc_html__( 'h2', 'guto-toolkit' ),
                        'h3'         => esc_html__( 'h3', 'guto-toolkit' ),
                        'h4'         => esc_html__( 'h4', 'guto-toolkit' ),
                        'h5'         => esc_html__( 'h5', 'guto-toolkit' ),
                        'h6'         => esc_html__( 'h6', 'guto-toolkit' ),
                    ],
                    'default' => 'h2',
                ]
            );

            $this->add_control(
                'content',
                [
                    'label' => esc_html__( 'Content', 'guto-toolkit' ),
                    'type' => Controls_Manager::TEXTAREA,
                    'default' => '',
                ]
            );

        $this->end_controls_section();

        $this->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'guto-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
        );

            $this->add_control(
                'top_title_color',
                [
                    'label' => esc_html__( 'Top Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .section-title .sub-title' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'top_title_typography',
                    'label' => esc_html__( 'Top Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .section-title .sub-title',
                ]
            );

            $this->add_control(
                'title_color',
                [
                    'label' => esc_html__( 'Title Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .section-title h2, .section-title h3, .section-title h4, .section-title h5, .section-title h5, .section-title h6, .section-title h1' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'title_typography',
                    'label' => esc_html__( 'Title Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .section-title h2, .section-title h3, .section-title h4, .section-title h5, .section-title h5, .section-title h6, .section-title h1',
                ]
            );

            $this->add_control(
                'content_color',
                [
                    'label' => esc_html__( 'Content Color', 'guto-toolkit' ),
                    'type' => Controls_Manager::COLOR,
                    'selectors' => [
                        '{{WRAPPER}} .section-title p' => 'color: {{VALUE}}',
                    ],
                ]
            );

            $this->add_group_control(
                Group_Control_Typography::get_type(),
                [
                    'name' => 'content_typography',
                    'label' => esc_html__( 'Content Typography', 'guto-toolkit' ),
                    'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                    'selector' => '{{WRAPPER}} .section-title p',
                ]
            );

        $this->end_controls_section();

    }

	protected function render() {

        $settings = $this->get_settings_for_display();

        // Inline Editing
        $this-> add_inline_editing_attributes('title','none');
        ?>
            <div class="container">
                <div class="section-title">
                
                    <?php if($settings['top_title'] != '' ): ?>
                        <span class="sub-title"><?php echo wp_kses_post( $settings['top_title'] ); ?></span>
                    <?php endif; ?>

                    <<?php echo esc_attr( $settings['title_tag'] ); ?> <?php echo $this-> get_render_attribute_string('title'); ?>><?php echo wp_kses_post( $settings['title'] ); ?></<?php echo esc_attr( $settings['title_tag'] ); ?>>

                    <?php if($settings['content'] != '' ): ?>
                        <p><?php echo wp_kses_post( $settings['content'] ) ?></p>
                    <?php endif; ?>
                </div>
            </div>
        <?php
	}

	protected function _content_template() {}

}

Plugin::instance()->widgets_manager->register_widget_type( new Guto_Section );