<?php

/**
 * Guto Toolkit Plugin Functions
 *
 * @link       https://profiles.wordpress.org/gutotheme/
 * @since      1.3.0
 *
 * @package    Guto_Toolkit
 * @subpackage Guto_Toolkit/includes
 */

/**
 * Guto Toolkit Plugin Functions
 *
 * This class responsible for defining Elementor extension functionality
 *
 * @since      1.3.0
 * @package    Guto_Toolkit
 * @subpackage Guto_Toolkit/includes
 * @author     GutoTheme
 */

/**
 * Remove empty p tag from content
 */
function guto_toolkit_remove_empty_p($content){
    $content = force_balance_tags($content);
    return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
}
add_filter('the_content', 'guto_toolkit_remove_empty_p', 20, 1);

/**
 * Check a plugin activate
 */
function guto_toolkit_plugin_active( $plugin ) {
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if ( is_plugin_active( $plugin ) ) {
		return true;
	}
	return false;
}

/**
 * Post category list
 */
function guto_toolkit_get_post_cat_list() {
	$post_category_id = get_queried_object_id();
	$args = array(
		'parent' => $post_category_id
	);

	$terms = get_terms( 'category', get_the_ID());
	$cat_options = array(esc_html__('', 'guto-toolkit') => '');

	if ($terms) {
		foreach ($terms as $term) {
			$cat_options[$term->name] = $term->name;
		}
	}
	return $cat_options;
}

/**
 * Page list
 */
function guto_toolkit_get_page_as_list() {
    $args = wp_parse_args(array(
        'post_type' => 'page',
        'numberposts' => -1,
    ));

    $posts = get_posts($args);
    $post_options = array(esc_html__('', 'guto-toolkit') => '');

    if ($posts) {
        foreach ($posts as $post) {
            $post_options[$post->post_title] = $post->ID;
        }
    }
    $flipped = array_flip($post_options);
    return $flipped;
}

/**
 * Courses category list
 */
function guto_toolkit_get_products_cat_list() {
	if ( !guto_toolkit_plugin_active( 'woocommerce/woocommerce.php' ) ) {
		return;
	}
	$products_category_id = get_queried_object_id();
	$args = array(
		'parent' => $products_category_id
	);

	$terms = get_terms( 'product_cat', get_the_ID());
	$cat_options = array(esc_html__('', 'guto-toolkit') => '');

	if ($terms) {
		foreach ($terms as $term) {
			$cat_options[$term->name] = $term->name;
		}
	}
	return $cat_options;
}

/**
 * Product list
 */
function guto_toolkit_get_product_as_list() {
	if ( !guto_toolkit_plugin_active( 'woocommerce/woocommerce.php' ) ) {
		return;
	}
    $args = wp_parse_args(array(
        'post_type' => 'product',
        'numberposts' => -1,
    ));

    $posts = get_posts($args);
    $post_options = array(esc_html__('', 'guto-toolkit') => '');

    if ($posts) {
        foreach ($posts as $post) {
            $post_options[$post->post_title] = $post->ID;
        }
    }
    $flipped = array_flip($post_options);
    return $flipped;
}

/**
 * Post list
 */
function guto_toolkit_get_post_title_array( $postType = 'post' ) {
	$args = wp_parse_args(array(
        'post_type' => $postType,
        'numberposts' => -1,
    ));

    $posts = get_posts( $args );
    $post_options = array( esc_html__('', 'guto-toolkit') => '' );

    if ($posts) {
        foreach ( $posts as $post ) {
            $post_options[$post->post_title] = $post->ID;
        }
    }
    $flipped = array_flip( $post_options);
	return $flipped;
}

/**
 * Newsletter script
 */
function guto_toolkit_add_script_to_footer(){
    if( ! is_admin() ) {
        if( function_exists('guto_option')):
            $action_url	= guto_option('newsletter_action_url');
        else:
            $action_url	= '';
        endif;
		?>

		<script>
			jQuery(document).ready(function($){
				$(document).on('click', '.plus', function(e) { // replace '.quantity' with document (without single quote)
					$input = $(this).prev('input.qty');
					var val = parseInt($input.val());
					var step = $input.attr('step');
					step = 'undefined' !== typeof(step) ? parseInt(step) : 1;
					$input.val( val + step ).change();
				});
				$(document).on('click', '.minus',  // replace '.quantity' with document (without single quote)
					function(e) {
					$input = $(this).next('input.qty');
					var val = parseInt($input.val());
					var step = $input.attr('step');
					step = 'undefined' !== typeof(step) ? parseInt(step) : 1;
					if (val > 0) {
						$input.val( val - step ).change();
					}
				});
			});

			<?php if($action_url): ?>
				;(function($){
					"use strict";
					$(document).ready(function () {
						// MAILCHIMP
						if ($(".mailchimp").length > 0) {
							$(".mailchimp").ajaxChimp({
								callback: mailchimpCallback,
								url: "<?php echo esc_js($action_url) ?>"
							});
						}
						if ($(".mailchimp_two").length > 0) {
							$(".mailchimp_two").ajaxChimp({
								callback: mailchimpCallback,
								url: "<?php echo esc_js($action_url) ?>"
							});
						}
						$(".memail").on("focus", function () {
							$(".mchimp-errmessage").fadeOut();
							$(".mchimp-sucmessage").fadeOut();
						});
						$(".memail").on("keydown", function () {
							$(".mchimp-errmessage").fadeOut();
							$(".mchimp-sucmessage").fadeOut();
						});
						$(".memail").on("click", function () {
							$(".memail").val("");
						});

						function mailchimpCallback(resp) {
							if (resp.result === "success") {
								$(".mchimp-sucmessage").html(resp.msg).fadeIn(1000);
								$(".mchimp-sucmessage").fadeOut(500);
							} else if (resp.result === "error") {
								$(".mchimp-errmessage").html(resp.msg).fadeIn(1000);
							}
						}
					});
				})(jQuery)
			<?php endif; ?>
		</script>

	<?php
	}
}
add_action( 'wp_footer', 'guto_toolkit_add_script_to_footer' );

/**
 * Get the existing menus in array format
 * @return array
 */
function guto_get_menu_array() {
    $menus = wp_get_nav_menus();
    $menu_array = [];
    foreach ( $menus as $menu ) {
        $menu_array[$menu->slug] = $menu->name;
    }
    return $menu_array;
}

/**
 * Estimated Reading Time
 */
if ( ! function_exists( 'guto_toolkit_reading_time' ) ) :
	function guto_toolkit_reading_time() {
		global $post;

		$thecontent = $post->post_content;
		$words 		= str_word_count( strip_tags( $thecontent ) );
		$m 			= floor( $words / 200 );
		$estimate 	= $m;
		$output 	= $estimate;
		return $output;
	}
endif;

/**
 * New WooCommerce Cart Style
 */
if ( ! function_exists( 'woocommerce_product_cart_style_fs' ) ) :
	function woocommerce_product_cart_style_fs() {
		global $product;

		if ( ! $product->is_purchasable() ) {
		    return;
		}

		echo wc_get_stock_html( $product );

		if ( $product->is_in_stock() ) : ?>

		    <?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

		    <form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		        <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		        <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="add-to-cart-btn"><i class="bx bx-cart"></i><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

		        <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
		    </form>

		    <?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

		<?php endif;
	}
endif;

/**
 * New WooCommerce Cart Style Two
 */
if ( ! function_exists( 'woocommerce_product_cart_style_fs_two' ) ) :
	function woocommerce_product_cart_style_fs_two() {
		global $product;

		if ( ! $product->is_purchasable() ) {
		    return;
		}

		echo wc_get_stock_html( $product );

		if ( $product->is_in_stock() ) : ?>

		    <?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

		    <form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		        <?php do_action( 'woocommerce_before_add_to_cart_button' );

		        do_action( 'woocommerce_before_add_to_cart_button' ); 

				do_action( 'woocommerce_before_add_to_cart_quantity' );

				woocommerce_quantity_input( array(
					'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
					'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
					'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				) );

				do_action( 'woocommerce_after_add_to_cart_quantity' ); ?>

		        <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="default-btn"><i class="bx bx-cart"></i><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

		        <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
		    </form>

		    <?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

		<?php endif;
	}
endif;
