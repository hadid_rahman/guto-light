<?php

/**
 * Add custom icon into Elementor ICON field
 */
function guto_toolkit_icons() {
    return [

        // Box Icons
        'bx bx-gift'                => esc_html__( 'Gift', 'guto-toolkit' ),
        'bx bx-credit-card-front'   => esc_html__( 'Credit Card Front', 'guto-toolkit' ),
        'bx bx-refresh'             => esc_html__( 'Refresh', 'guto-toolkit' ),
        'bx bxs-truck'              => esc_html__( 'Truck', 'guto-toolkit' ),
        'bx bx-time-five'           => esc_html__( 'Time Five', 'guto-toolkit' ),
        'bx bx-phone-call'          => esc_html__( 'Phone Call', 'guto-toolkit' ),
        'bx bx-map'                 => esc_html__( 'Map', 'guto-toolkit' ),
        'bx bx-globe'               => esc_html__( 'Globe', 'guto-toolkit' ),
        'bx bx-plus'                => esc_html__( 'Plus', 'guto-toolkit' ),
        'bx bx-play'                => esc_html__( 'Play', 'guto-toolkit' ),
        'bx bx-chevron-right'       => esc_html__( 'Chevron Right', 'guto-toolkit' ),
        'bx bx-cog'                 => esc_html__( 'Cog', 'guto-toolkit' ),
        'bx bx-layer'               => esc_html__( 'Layer', 'guto-toolkit' ),
        'bx bx-brain'               => esc_html__( 'Brain', 'guto-toolkit' ),
        'bx bxl-twitter'            => esc_html__( 'Twitter', 'guto-toolkit' ),
        'bx bxl-linkedin'           => esc_html__( 'Linkedin', 'guto-toolkit' ),
        'bx bxl-instagram'          => esc_html__( 'Instagram', 'guto-toolkit' ),
        'bx bxl-facebook'           => esc_html__( 'facebook', 'guto-toolkit' ),
        'bx bxl-youtube'            => esc_html__( 'YouTube', 'guto-toolkit' ),
        'bx bx-info-circle'         => esc_html__( 'Info Circle', 'guto-toolkit' ),
        'bx bx-book-open'           => esc_html__( 'Book Open', 'guto-toolkit' ),
        'bx bx-shopping-bag'        => esc_html__( 'Shopping', 'guto-toolkit' ),
        'bx bxs-badge-dollar'       => esc_html__( 'Dollar', 'guto-toolkit' ),
        'bx bx-code-alt'            => esc_html__( 'Code', 'guto-toolkit' ),
        'bx bx-flag'                => esc_html__( 'Flag', 'guto-toolkit' ),
        'bx bx-camera'              => esc_html__( 'Camera', 'guto-toolkit' ),
        'bx bxs-flag-checkered'     => esc_html__( 'Flag Checkered', 'guto-toolkit' ),
        'bx bx-health'              => esc_html__( 'Star', 'guto-toolkit' ),
        'bx bx-line-chart'          => esc_html__( 'Line Chart', 'guto-toolkit' ),
        'bx bx-book-reader'         => esc_html__( 'Book Reader', 'guto-toolkit' ),
        'bx bx-target-lock'         => esc_html__( 'Target Lock', 'guto-toolkit' ),
        'bx bxs-thermometer'        => esc_html__( 'Thermometer', 'guto-toolkit' ),
        'bx bx-shape-triangle'      => esc_html__( 'Triangle', 'guto-toolkit' ),
        'bx bx-font-family'         => esc_html__( 'Font Family', 'guto-toolkit' ),
        'bx bxs-drink'              => esc_html__( 'Drink', 'guto-toolkit' ),
        'bx bx-first-aid'           => esc_html__( 'First Aid', 'guto-toolkit' ),
        'bx bx-bar-chart-alt-2'     => esc_html__( 'Chart', 'guto-toolkit' ),
        'bx bx-briefcase-alt-2'     => esc_html__( 'Briefcase', 'guto-toolkit' ),
        'bx bx-book-reader'         => esc_html__( 'Book Reader', 'guto-toolkit' ),
        'bx bx-target-lock'         => esc_html__( 'Target Lock', 'guto-toolkit' ),
        'bx bx-user-circle'         => esc_html__( 'User', 'guto-toolkit' ),
        'bx bx-check'               => esc_html__( 'Check', 'guto-toolkit' ),
        'bx bx-paper-plane'         => esc_html__( 'Paper Plane', 'guto-toolkit' ),
        'bx bx-file'                => esc_html__( 'File', 'guto-toolkit' ),
        'bx bx-user-voice'          => esc_html__( 'User Voice', 'guto-toolkit' ),
    ];
}

function guto_toolkit_include_icons() {
    return array_keys(guto_toolkit_icons());
}