<?php

/**
 * Guto Toolkit Elementor Extension Class
 *
 * @link       https://profiles.wordpress.org/gutotheme/
 * @since      1.3.0
 *
 * @package    Guto_Toolkit
 * @subpackage Guto_Toolkit/includes
 */

/**
 * Guto Toolkit Elementor Extension Class
 *
 * This class responsible for defining Elementor extension functionality
 *
 * @since      1.3.0
 * @package    Guto_Toolkit
 * @subpackage Guto_Toolkit/includes
 * @author     GutoTheme
 */

final class Elementor_Guto_Toolkit_Extension {

	const VERSION = '1.3.0';
	const MINIMUM_ELEMENTOR_VERSION = '2.0.0';
	const MINIMUM_PHP_VERSION = '7.0';

	// Instance
    private static $_instance = null;

	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;

	}

	// Constructor
	public function __construct() {
		add_action( 'plugins_loaded', [ $this, 'init' ] );

	}

	// init
	public function init() {
        load_plugin_textdomain( 'guto-toolkit' );

		// Check for required PHP version
		if ( version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '<' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_php_version' ] );
			return;
		}

		// Add Plugin actions
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'init_widgets' ] );

        add_action('elementor/elements/categories_registered',[ $this, 'register_new_category'] );

    }

    public function register_new_category($manager){
        $manager->add_category('guto-elements',[
            'title'=>esc_html__('Guto Toolkit','guto-toolkit'),
            'icon'=> 'fa fa-image'
        ]);
    }

	public function admin_notice_minimum_elementor_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor 3: Required Elementor version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'guto-toolkit' ),
			'<strong>' . esc_html__( 'Guto Toolkit', 'guto-toolkit' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'guto-toolkit' ) . '</strong>',
			 self::MINIMUM_ELEMENTOR_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}
	public function admin_notice_minimum_php_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: PHP 3: Required PHP version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'guto-toolkit' ),
			'<strong>' . esc_html__( 'Guto Toolkit', 'guto-toolkit' ) . '</strong>',
			'<strong>' . esc_html__( 'PHP', 'guto-toolkit' ) . '</strong>',
			 self::MINIMUM_PHP_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	// Register Widgets
	public function init_widgets() {
		require_once( __DIR__ . '/el-widgets/banner-one.php' );
		require_once( __DIR__ . '/el-widgets/banner-two.php' );
		require_once( __DIR__ . '/el-widgets/banner-three.php' );
		require_once( __DIR__ . '/el-widgets/section.php' );
		require_once( __DIR__ . '/el-widgets/services.php' );
		require_once( __DIR__ . '/el-widgets/about-area.php' );
		require_once( __DIR__ . '/el-widgets/feedback.php' );
		require_once( __DIR__ . '/el-widgets/video-area.php' );
		require_once( __DIR__ . '/el-widgets/working-process.php' );
		require_once( __DIR__ . '/el-widgets/team.php' );
		require_once( __DIR__ . '/el-widgets/partner.php' );
		require_once( __DIR__ . '/el-widgets/blog-posts.php' );
		require_once( __DIR__ . '/el-widgets/funfacts.php' );
		require_once( __DIR__ . '/el-widgets/overview-area.php' );
		require_once( __DIR__ . '/el-widgets/projects.php' );
		require_once( __DIR__ . '/el-widgets/popup.php' );
		require_once( __DIR__ . '/el-widgets/project-info.php' );
		require_once( __DIR__ . '/el-widgets/pricing.php' );
		require_once( __DIR__ . '/el-widgets/faq.php' );
		require_once( __DIR__ . '/el-widgets/coming-soon.php' );
		require_once( __DIR__ . '/el-widgets/contact-area.php' );
		require_once( __DIR__ . '/el-widgets/categories-area.php' );
		require_once( __DIR__ . '/el-widgets/newsletter.php' );
		require_once( __DIR__ . '/el-widgets/instagram-area.php' );
		require_once( __DIR__ . '/el-widgets/single-facility-box.php' );
		require_once( __DIR__ . '/el-widgets/products-slider.php' );
		require_once( __DIR__ . '/el-widgets/products-grid.php' );
		require_once( __DIR__ . '/el-widgets/promotions-area.php' );
		require_once( __DIR__ . '/el-widgets/room-area.php' );
    }

}
Elementor_Guto_Toolkit_Extension::instance();