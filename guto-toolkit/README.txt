=== Guto Toolkit for Elementor Addons===
Contributors: GutoTheme
Donate link: https://profiles.wordpress.org/gutotheme/
Tags: addons,elementor, elementor addon, elementor widget, elements
Requires at least: 4.0
Tested up to: 5.7
Stable tag: 1.3.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Guto Toolkit is an Elementor addon WordPress plugin that allows you to create 25+ unique layouts on your site.

== Installation ==

= From the dashboard: =
1. Go to the WordPress Dashboard "Add New Plugin" section
2. Search For "Guto Toolkit"
3. Install, then Activate it

= From control panel: =
1. Unzip (if it is zipped) and Upload `guto-toolkit` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Screenshots ==

1. Guto Toolkit Elementor Addons into editor
2. Guto Toolkit widgets

== FAQ ==

= Can I use the plugin without Elementor Page Builder? =

No. You cannot use without Elementor since it’s an addon for Elementor.

= Does it work with any theme? =

= Absolutely! It will work with any theme where Elementor works. =

== Changelog ==

= 1.3.0 =
- Added: Banner Three Widgets
- Added: Product Grid Widgets
- Added: Room Widgets
- Updated: Product Category Widgets Version 3 
- Updated: Newsletter Widgets Version Three 
- Updated: Blog Posts Style Version 2  
- Updated: Promotion Style Version 3 

= 1.2.0 =
- Updated: CSS class name

= 1.1 =
- Fixed: Small device menu keyboard navigation issue

= 1.0 =
- Initial Release
